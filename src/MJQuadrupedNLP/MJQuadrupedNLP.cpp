#include "MJQuadrupedNLP.hpp"

#include <iostream>
#include <cmath>
#include <unistd.h>
#include <assert.h>

using namespace Ipopt;
using namespace std;

/**
 * Constructor
 *
 * Use initialiser list to call non-default constructor of the member class
 *
 * x = [ q, theta, dq, dtheta ]
 *
 * ...where q is the horizontal position
 */
MJQuadrupedNLP::MJQuadrupedNLP() :
		CollocationNLP(21), model("src/MJQuadrupedNLP/quadruped.xml")
{
	num_q = 11;
	num_states = num_q * 2;
	num_inputs = 8;

	double t_0 = 0.0;
	double t_f = 1.0;

	h = (t_f - t_0) / (N - 1);

	m_extra = 0;
	nnz_jac_g_extra = 0;

	/**
	 * Configs are not used as constraints
	 * They are still used for the initial guess
	 */
	custom_initial_guess = true;
	constraint_config_initial = true;
	constraint_config_final = true;

	L1 = L2 = 0.3;

	L_step = 0.3;

	k_half = (int) (0.5 * N);

	q_0_abs = new double[num_q];

	/*
	 // Optimisation result
	 x_0_abs[2] = -0.1601;
	 x_0_abs[3] = -0.3651;
	 x_0_abs[4] = 0.0198;
	 x_0_abs[5] = 0.1664;
	 x_0_abs[6] = 0.3235;
	 */

	for (int i = 0; i < num_q; i++)
	{
		q_0_abs[i] = 0.0;
	}
}

/**
 * Destructor
 */
MJQuadrupedNLP::~MJQuadrupedNLP()
{
	delete[] q_0_abs;
}

/**
 * Get initial configuration
 *
 * Set value to 1.0e19 to make it unbounded
 */
void MJQuadrupedNLP::get_initial_config(double* x_0, double* u_0)
{
	// Manual tuning:
	x_0[0] = 0.0;
	x_0[1] = 1.0e19;
	x_0[2] = 0.0;

	x_0[3] = x_0[5] = x_0[7] = x_0[9] = 0.3;
	x_0[4] = x_0[6] = x_0[8] = x_0[10] = -0.6;

	// Velocity
	for (int i = 0; i < num_q; i++)
	{
		x_0[i + num_q] = 0.0;

		if (i < num_inputs)
		{
			u_0[i] = 1.0e19;
		}
	}
}

/**
 * Get final configuration
 *
 * Set value to 1.0e19 to make it unbounded
 */
void MJQuadrupedNLP::get_final_config(double* x_f, double* u_f)
{
	get_initial_config(x_f, u_f);

	x_f[0] += 2.0 * L_step; // Displacement

	// Velocity
	for (int i = 0; i < num_q; i++)
	{
		x_f[i + num_q] = 0.0;
	}
}

/**
 * Get variable bounds
 *
 * Set values to 1.0e19 and -1.0e19 to make it unbounded
 */
void MJQuadrupedNLP::get_variable_bound(double* x_u, double* x_l, double* u_u,
		double* u_l)
{
	for (int i = 0; i < num_q; i++)
	{
		// velocities
		x_l[i + num_q] = -10.0;
		x_u[i + num_q] = 10.0;

		if (i < num_inputs)
		{
			u_l[i] = -50;
			u_u[i] = 50;
		}
	}

	// Positions
	//x_l[0] = -1.0e19; x_u[0] = 1.0e19;
	x_l[0] = -0.5 * L_step;
	x_u[0] = 2.5 * L_step;
	x_l[1] = (L1 + L2) * -0.3;
	x_u[1] = (L1 + L2) * 0.2;

	// Floor angle
	x_l[2] = -0.25 * M_PI;
	x_u[2] = 0.25 * M_PI;

	// Knees
	x_l[4] = x_l[6] = x_l[8] = x_l[10] = -0.8 * M_PI; // Never overstretch
	x_u[4] = x_u[6] = x_u[8] = x_u[10] = -0.01;

	// Hip angles
	x_l[3] = x_l[5] = x_l[7] = x_l[9] = -0.4 * M_PI;
	x_u[3] = x_u[5] = x_u[7] = x_u[9] = 0.4 * M_PI;
}

/**
 * Calculate dynamics
 *
 * @return void
 */
void MJQuadrupedNLP::eval_dynamics(const double* x, const double* u, double* dx)
{
	const double* qpos = &(x[0]);
	const double* qvel = &(x[num_q]);
	double qacc[num_q];
	model.get_dynamics(qpos, qvel, u, qacc);

	for (int i = 0; i < num_q; i++)
	{
		dx[i] = x[i + num_q];
		dx[i + num_q] = qacc[i];
	}
}

/**
 * Get dynamics derivative w.r.t. x
 *
 * Return vector is stacked like:
 *
 * dx/x = [
 *    dx1/x
 *    dx2/x
 *    ...
 * ]
 *
 * @return void
 */
void MJQuadrupedNLP::eval_dynamics_diff_x(const double* x, const double* u,
		double* dx_diff_x)
{
	const double* qpos = &(x[0]);
	const double* qvel = &(x[num_q]);
	double qacc_diff_qvel[num_q * num_q], qacc_diff_qpos[num_q * num_q];

	model.get_dynamics_diff_qvel(qpos, qvel, u, qacc_diff_qvel);
	model.get_dynamics_diff_qpos(qpos, qvel, u, qacc_diff_qpos);

	// Fill the four squares
	int index = 0;
	for (int col = 0; col < num_q; col++)
	{
		for (int row = 0; row < num_q; row++)
		{
			// Zero square
			dx_diff_x[row + col * 2 * num_q] = 0.0;

			// Identity square
			double eye = (row == col) ? 1.0 : 0.0;
			dx_diff_x[2 * num_q * num_q + row + col * 2 * num_q] = eye;

			// d ddq / d q
			dx_diff_x[num_q + row + col * 2 * num_q] = qacc_diff_qpos[index];

			// d ddq / d dq
			dx_diff_x[2 * num_q * num_q + num_q + row + col * 2 * num_q] =
					qacc_diff_qvel[index];

			index++;
		}
	}
}

/**
 * Get dynamics derivative w.r.t. u
 *
 * @return void
 */
void MJQuadrupedNLP::eval_dynamics_diff_u(const double* x, const double* u,
		double* dx_diff_u)
{
	const double* qpos = &(x[0]);
	const double* qvel = &(x[num_q]);
	double qacc_diff_u[num_q * num_inputs];

	model.get_dynamics_diff_u(qpos, qvel, u, qacc_diff_u);

	int index = 0;
	for (int col = 0; col < num_inputs; col++)
	{
		for (int row = 0; row < num_q; row++)
		{
			dx_diff_u[row + col * 2 * num_q] = 0.0;
			dx_diff_u[num_q + row + col * 2 * num_q] = qacc_diff_u[index];
			index++;
		}
	}
}

/**
 * Get initial guess (starting point) for NLP
 */
void MJQuadrupedNLP::get_initial_guess(double** x, double** u)
{
	// Set user defined initial and final configuration
	double x_0[num_states], u_0[num_states];
	get_initial_config(x_0, u_0);

	double x_f[num_states], u_f[num_states];
	get_final_config(x_f, u_f);

	const int num_guess = 5;
	double q_guess[num_q][num_guess];
	double xx[num_guess] = { 0.0, 0.25, 0.5, 0.75, 1.0 };

	// Initial and final config
	for (int i = 0; i < num_q; i++)
	{
		q_guess[i][0] = x_0[i] < 0.5e19 ? x_0[i] : 0.0;
		q_guess[i][4] = x_f[i] < 0.5e19 ? x_f[i] : 0.0;
		//q_guess[i][1] = q_guess[i][0] * 0.5 + q_guess[i][2] * 0.5;
	}

	q_guess[0][1] = q_guess[4][1] = 0.1;

	// Interpolate frame
	for (int i = 0; i < num_q; i++)
	{
		for (int g = 1; g < num_guess - 1; g++)
		{
			double factor = static_cast<double>(g)
					/ static_cast<double>(num_guess - 1);
			q_guess[i][g] = q_guess[i][0]
					+ factor * (q_guess[i][num_guess - 1] - q_guess[i][0]);
		}
	}

	// Lift right front leg (1)
	q_guess[3][1] = q_guess[7][1] = 0.20;
	q_guess[4][1] = q_guess[8][1] = -0.30;
	q_guess[5][1] = q_guess[9][1] = 0.50;
	q_guess[6][1] = q_guess[10][1] = -2.0;

	// Land right front leg (2)
	q_guess[3][2] = q_guess[7][2] = 0.50;
	q_guess[4][2] = q_guess[8][2] = -0.15;
	q_guess[5][2] = q_guess[9][2] = -0.50;
	q_guess[6][2] = q_guess[10][2] = -0.15;

	// Lift left front leg (3)
	q_guess[3][3] = q_guess[7][3] = 0.50;
	q_guess[4][3] = q_guess[8][3] = -2.00;
	q_guess[5][3] = q_guess[9][3] = 0.20;
	q_guess[6][3] = q_guess[10][3] = -0.30;

	for (int i = 0; i < num_q; i++)
	{
		double interp[N];
		interpolateFromPoints(xx, q_guess[i], interp, num_guess, N);

		for (int k = 0; k < N; k++)
		{
			// Position
			x_write[k][i] = interp[k];
			// Velocity
			double deriv;
			if (k == 0)
			{
				deriv = (interp[k + 1] - interp[k]) / h;
			}
			else if (k == N - 1)
			{
				deriv = (interp[k] - interp[k - 1]) / h;
			}
			else
			{
				deriv = (interp[k + 1] - interp[k - 1]) / (2.0 * h);
			}
			x_write[k][i + num_q] = deriv;
		}
	}

	// Set input
	for (int k = 0; k < N; k++)
	{
		for (int i = 0; i < num_inputs; i++)
		{
			u_write[k][i] = 0.0; // Dynamically incorrect, but should fix itself
		}
	}
}

/**
 * Run at the end of the optimisation
 */
void MJQuadrupedNLP::finalize_solution(SolverReturn status, Index n,
		const Number* z, const Number* bm_L, const Number* mb_U, Index m,
		const Number* g, const Number* lambda, Number obj_value,
		const IpoptData* ip_data, IpoptCalculatedQuantities* ip_cq)
{
	cout << endl;
	cout << "N = " << N << ";" << endl;
	cout << "h = " << h << ";" << endl;
	cout << "num_q = " << num_q << ";" << endl;
	cout << "num_dq = " << num_q << ";" << endl;

	cout << endl << "z = [";
	for (int i = 0; i < n; i++)
	{
		cout << z[i] << ",";
	}
	cout << "];" << endl << endl;

	for (int i = 0; i < m; i++)
	{
		if (abs(g[i] > 1e-6))
		{
			cout << "g[" << i << "] = " << g[i] << ", ";
		}
	}
	cout << endl;

	set_variable_pointers(z); // Update pointers

	viewSolution();
	//testSolution();
}

/**
 * Convert relative joint angles to absolute anlges
 */
void MJQuadrupedNLP::joint2abs(const double* q_joint, double* q_abs, bool state)
{
	int loops = state ? (num_q + 1) : 1;
	for (int j = 0; j < loops; j += num_q)
	{
		q_abs[0 + j] = q_joint[0 + j]; // x
		q_abs[1 + j] = q_joint[1 + j]; // y
		q_abs[2 + j] = q_joint[0 + j]; // theta
		q_abs[3 + j] = q_abs[2 + j] + q_joint[3 + j];
		q_abs[4 + j] = q_abs[3 + j] + q_joint[4 + j]; // Knee
		q_abs[5 + j] = q_abs[2 + j] + q_joint[5 + j];
		q_abs[6 + j] = q_abs[5 + j] + q_joint[6 + j]; // Knee
		q_abs[7 + j] = q_abs[2 + j] + q_joint[7 + j];
		q_abs[8 + j] = q_abs[3 + j] + q_joint[8 + j]; // Knee
		q_abs[9 + j] = q_abs[2 + j] + q_joint[9 + j];
		q_abs[10 + j] = q_abs[5 + j] + q_joint[10 + j]; // Knee
	}
}

/**
 * Convert absolute angles to joint angles
 */
void MJQuadrupedNLP::abs2joint(const double* q_abs, double* q_joint, bool state)
{
	int loops = state ? (num_q + 1) : 1;
	for (int j = 0; j < loops; j += num_q)
	{
		q_joint[0 + j] = q_abs[0 + j]; // x
		q_joint[1 + j] = q_abs[1 + j]; // y
		q_joint[2 + j] = q_abs[2 + j]; // theta
		q_joint[3 + j] = q_abs[3 + j] - q_abs[2 + j];
		q_joint[4 + j] = q_abs[4 + j] - q_abs[3 + j]; // Knee
		q_joint[5 + j] = q_abs[5 + j] - q_abs[2 + j];
		q_joint[6 + j] = q_abs[6 + j] - q_abs[5 + j]; // Knee
		q_joint[7 + j] = q_abs[7 + j] - q_abs[2 + j];
		q_joint[8 + j] = q_abs[8 + j] - q_abs[7 + j]; // Knee
		q_joint[9 + j] = q_abs[9 + j] - q_abs[2 + j];
		q_joint[10 + j] = q_abs[10 + j] - q_abs[9 + j]; // Knee
	}
}

/*
 * Use MuJoCoViewer to play the solution
 */
void MJQuadrupedNLP::viewSolution()
{
	mjModel* m = model.getModelPtr();
	mjData* d = model.getDataPtr();

	MuJoCoViewer viewer(m);

	viewer.enableFigure();

	viewer.opt.flags[mjVIS_CONTACTPOINT] = 1;
	//viewer.opt.flags[mjVIS_CONTACTFORCE] = 1;
	viewer.opt.flags[mjVIS_TRANSPARENT] = 1;
	//viewer.opt.label = mjLABEL_CONTACTFORCE;

	// Move camera
	viewer.cam.distance = 5.0;

	bool done = false;

	const double num_interp = 100.0;

	while (!done)
	{
		double tmupdate = glfwGetTime();

		mj_resetData(m, d); // Reset simulation

		// Loop over collocation points
		int k_max = 1.3 * N; // Run a little longer to pause at the end

		for (int k = 0; k < k_max; k++)
		{
			// Loop over interpolated data in real time
			for (double interp = 0.0; interp < 1.0; interp += 1.0 / num_interp)
			{
				double tmstart = glfwGetTime();

				if (k < N - 1)
				{
					for (int j = 0; j < num_q; j++) // Only take positions from state
					{
						d->qpos[j] = x[k][j] + (x[k + 1][j] - x[k][j]) * interp;
						d->qvel[j] = x[k][j + num_q]
								+ (x[k + 1][j + num_q] - x[k][j + num_q])
										* interp;
					}
					for (int j = 0; j < num_inputs; j++)
					{
						d->ctrl[j] = u[k][j] + (u[k + 1][j] - u[k][j]) * interp;
					}
				}

				mj_forward(m, d); // Necessary for proper render

				if (glfwGetTime() - tmupdate > 1.0 / 60.0) // Update when last frame is 1/60 seconds old
				{
					viewer.updateScene(d);

					// Add arrows

					for (int j = 0; j < num_inputs; j++)
					{
						double base[3];
						mju_n2d(base, &(d->xpos[(j + 2) * 3]), 3);
						double vector[3] = { 0.0, d->ctrl[j] * 0.1, 0.0 };

						viewer.drawArrow(vector, base);
					}

					float force = 0.0f;

					if (d->ncon > 0)
					{
						mjtNum contact[6];
						mj_contactForce(m, d, 0, contact);
						force = (float) mju_norm(contact, 3);
					}

					viewer.updateFigure(force);

					viewer.update(); // Render

					tmupdate = glfwGetTime();
				}

				if (viewer.windowShouldClose())
				{
					done = true;
					return; // Stop
				}

				double wait_time = 1.0 * h / num_interp;
				while (glfwGetTime() - tmstart < wait_time)
				{
					usleep(100); // Sleep to prevent a busy wait
				}
			}
		}
	}
}

