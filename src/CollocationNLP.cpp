#include "CollocationNLP.hpp"
#include <cassert>
#include <iostream>
#include <algorithm>

using namespace Ipopt;
using namespace std;

/**
 * Constructor
 *
 * @param int N Number of collocation points
 */
CollocationNLP::CollocationNLP(int N_)
{
	N = N_;
	num_states = 0;
	num_inputs = 0;
	size = num_states + num_inputs;
	h = 1.0;

	num_dynamic_bounds = 0;

	m_extra = 0;
	nnz_jac_g_extra = 0;

	custom_initial_guess = false;

	constraint_config_initial = true;
	constraint_config_final = true;

	new_z = false;

	z = nullptr;
	x = new const double*[N];
	u = new const double*[N];

	z_write = nullptr;
	x_write = new double*[N];
	u_write = new double*[N];
}

/**
 * Destructor
 */
CollocationNLP::~CollocationNLP()
{
	/**
	 * Free allocated memory
	 *
	 * We only clean the array of pointers! We do not need to clean
	 * the values themselves, since these are owned by IPOPT in the
	 * `z`
	 */
	delete[] x;
	delete[] u;
	delete[] x_write;
	delete[] u_write;
}

/**
 * Get nlp info
 *
 * The optimisation vector x (called z elsewhere) is built like:
 * z = [
 *		x_1[0]
 * 		x_2[0]
 * 		x_1[1]
 * 		x_2[1]
 * 		x_1[2]
 * 		x_2[2]
 *	...
 *		u_1[0]
 *		u_2[0]
 *		u_1[1]
 *		u_2[1]
 *		u_1[2]
 *		u_2[2]
 *	...
 * ]
 */
bool CollocationNLP::get_nlp_info(Index& n, Index& m, Index& nnz_jac_g,
		Index& nnz_h_lag, IndexStyleEnum& index_style)
{
	// Number of variables per point
	size = num_states + num_inputs;

	// Number of optimization variables
	n = size * N;

	// Number of equality constraints
	m = num_states * (N - 1); // Only the dynamics ones

	// Jacobian non-zeros
	int nnz_dynamics = 2 * size; // Depend on x[k] and x[k+1]

	nnz_jac_g = nnz_dynamics * num_states * (N - 1);

	// Add stuff for extra dynamic bounds
	if (num_dynamic_bounds > 0)
	{
		m += num_dynamic_bounds * N;
		nnz_jac_g += (num_dynamic_bounds * N) * size;
	}

	// Add stuff for extra constraints
	if (m_extra > 0)
	{
		m += m_extra;
		nnz_jac_g += nnz_jac_g_extra;
	}

	// Lagrangian hessian non-zeros
	nnz_h_lag = n * n;

	index_style = FORTRAN_STYLE; // Use 1 indexing for non-zero elements

	return true;
}

/**
 * Get bounds
 */
bool CollocationNLP::get_bounds_info(Index n, Number* z_l, Number* z_u, Index m,
		Number* g_l, Number* g_u)
{
	// Set user defined regular bounds
	double x_u[num_states], x_l[num_states], u_u[num_inputs], u_l[num_inputs];
	get_variable_bound(x_u, x_l, u_u, u_l);

	const int offset = num_states * N;

	// Regular
	for (int k = 0; k < N; k++)
	{
		for (int i = 0; i < num_states; i++)
		{
			z_u[i + k * num_states] = x_u[i];
			z_l[i + k * num_states] = x_l[i];
		}
		for (int i = 0; i < num_inputs; i++)
		{
			z_u[i + k * num_inputs + offset] = u_u[i];
			z_l[i + k * num_inputs + offset] = u_l[i];
		}
	}

	// Initial
	if (constraint_config_initial)
	{
		double x_0[num_states], u_0[num_inputs];
		get_initial_config(x_0, u_0);

		for (int i = 0; i < num_states; i++)
		{
			if (x_0[i] < 0.5e19)
			{
				z_u[i] = x_0[i];
				z_l[i] = x_0[i];
			}
		}
		for (int i = 0; i < num_inputs; i++)
		{
			if (u_0[i] < 0.5e19)
			{
				z_u[i + offset] = u_0[i];
				z_l[i + offset] = u_0[i];
			}
		}
	}

	// Final
	if (constraint_config_final)
	{
		double x_f[num_states], u_f[num_inputs];
		get_final_config(x_f, u_f);

		for (int i = 0; i < num_states; i++)
		{
			if (x_f[i] < 0.5e19)
			{
				z_u[i + (N - 1) * num_states] = x_f[i];
				z_l[i + (N - 1) * num_states] = x_f[i];
			}
		}
		for (int i = 0; i < num_inputs; i++)
		{
			if (u_f[i] < 0.5e19)
			{
				z_u[i + N * num_states + (N - 1) * num_inputs] = u_f[i];
				z_l[i + N * num_states + (N - 1) * num_inputs] = u_f[i];
			}
		}
	}

	// Constraints are bound to zero
	int i = 0;
	for (int j = 0; j < num_states * (N - 1); j++) // Dynamics constraints
	{
		g_l[i] = 0.0;
		g_u[i] = 0.0;
		i++;
	}

	// Add additional constraints
	if (num_dynamic_bounds > 0)
	{
		for (int k = 0; k < N; k++)
		{
			double g_l_extra[num_dynamic_bounds], g_u_extra[num_dynamic_bounds];
			get_dynamic_bounds(g_l_extra, g_u_extra);
			for (int j = 0; j < num_dynamic_bounds; j++)
			{
				g_l[i] = g_l_extra[j];
				g_u[i] = g_u_extra[j];
				i++;
			}
		}
	}

	// Add extra constraints

	if (m_extra > 0)
	{
		double g_l_extra[m_extra], g_u_extra[m_extra];
		get_bounds_info_extra(g_l_extra, g_u_extra);

		for (int j = 0; j < m_extra; j++)
		{
			g_l[i] = g_l_extra[j];
			g_u[i] = g_u_extra[j];
			i++;
		}
	}

	assert(i == m); // Make sure we filled in all constraints

	return true;
}

/**
 * Get initial optimisation variable values
 *
 * Note: bm = bound multipliers, what IPOPT calls 'z'.
 */
bool CollocationNLP::get_starting_point(Index n, bool init_z, Number* z,
		bool init_bm, Number* bm_L, Number* bm_U, Index m, bool init_lambda,
		Number* lambda)
{
	// Here, we assume we only have starting values for x, if you code
	// your own NLP, you can provide starting values for the others if
	// you wish.
	assert(init_z == true);
	assert(init_bm == false);
	assert(init_lambda == false);

	set_variable_pointers(z);

	// Use custom function, or continue with default
	if (custom_initial_guess)
	{
		get_initial_guess(x_write, u_write);

		return true;
	}

	// Set user defined initial and final configuration
	double x_0[num_states], u_0[num_states];
	get_initial_config(x_0, u_0);

	double x_f[num_states], u_f[num_states];
	get_final_config(x_f, u_f);

	// Make linear interpolation from start to finish for positions

	for (int i = 0; i < num_states; i++)
	{
		double step = (x_f[i] - x_0[i]) / (N - 1.0);
		for (int k = 0; k < N; k++)
		{
			if (x_f[i] > 0.5e19 || x_0[i] > 0.5e19)
			{
				x_write[k][i] = 0.0;
			}
			else
			{
				x_write[k][i] = x_0[i] + k * step;
			}
		}
	}

	for (int k = 0; k < N; k++)
	{
		for (int i = 0; i < num_inputs; i++)
		{
			// Start at zero (breaks constraints, but should be fixed automatically)
			u_write[k][i] = 0.0;
		}
	}

	return true;
}

/**
 * Get the current objective function value
 */
bool CollocationNLP::eval_f(Index n, const Number* z, bool new_z,
		Number& obj_value)
{
	set_variable_pointers(z);

	obj_value = 0.0;

	for (int k = 0; k < N - 1; k++)
	{
		for (int i = 0; i < num_inputs; i++)
		{
			obj_value += 0.5 * h
					* (u[k][i] * u[k][i] + u[k + 1][i] * u[k + 1][i]);
		}
	}

	return true;
}

/**
 * Get the current objective gradient value
 */
bool CollocationNLP::eval_grad_f(Index n, const Number* z, bool new_z,
		Number* grad_f)
{
	set_variable_pointers(z);

	int i;
	for (i = 0; i < N * num_states; i++)
	{
		grad_f[i] = 0.0;
	}

	for (int k = 0; k < N; k++)
	{
		for (int j = 0; j < num_inputs; j++)
		{
			double cof = 2.0;
			if (k == 0 || k == N - 1)
			{
				cof = 1.0;
			}

			grad_f[i] = cof * h * u[k][j];
			i++;
		}
	}

	assert(i == n); // Make sure we filled in all entries

	return true;
}

/**
 * Get the current constraint values
 *
 * Constraints are first grouped per collocation point
 *
 * 		g(x) = [
 *			x_0[0] - x_0[1] + 0.5 * h * (dx_0[0] + dx_0[1]) ;
 *			x_1[0] - x_1[1] + 0.5 * h * (dx_1[0] + dx_1[1]) ;
 *			...
 *			x_0[1] - x_0[2] + 0.5 * h * (dx_0[1] + dx_0[2]) ;
 *			x_0[1] - x_0[2] + 0.5 * h * (dx_0[1] + dx_0[2]) ;
 *			...
 * 		]
 *
 */
bool CollocationNLP::eval_g(Index n, const Number* z, bool new_z, Index m,
		Number* g)
{
	set_variable_pointers(z);

	this->new_z = new_z;

	int i = 0;

	double dx_k[num_states], dx_kp1[num_states];

	eval_dynamics(x[0], u[0], dx_kp1);

	// Dynamics integration
	for (int k = 0; k < N - 1; k++)
	{
		// ddq_k = ddq_kp1
		copy(dx_kp1, dx_kp1 + num_states, dx_k);

		eval_dynamics(x[k + 1], u[k + 1], dx_kp1);

		for (int j = 0; j < num_states; j++)
		{
			g[i++] = x[k][j] - x[k + 1][j] + 0.5 * h * (dx_k[j] + dx_kp1[j]);
		}
	}

	// Additional dynamic constraints

	if (num_dynamic_bounds > 0)
	{
		for (int k = 0; k < N; k++)
		{
			double g_extra[num_dynamic_bounds];
			eval_dynamic_bounds(x[k], u[k], g_extra);

			for (int j = 0; j < num_dynamic_bounds; j++)
			{
				g[i++] = g_extra[j];
			}
		}
	}

	// Extra constraints

	if (m_extra > 0)
	{
		double g_extra[m_extra];
		eval_g_extra(x, u, g_extra);

		for (int j = 0; j < m_extra; j++)
		{
			g[i++] = g_extra[j];
		}
	}

	assert(i == m); // Make sure we filled in all entries

	return true;
}

/**
 * Either get the constraint jacobian shape or the jacobian values
 */
bool CollocationNLP::eval_jac_g(Index n, const Number* z, bool new_z, Index m,
		Index nele_jac, Index* iRow, Index* jCol, Number* values)
{
	if (values == NULL)
	{
		// Return the structure of the jacobian of the constraints
		eval_jac_g_shape(n, m, nele_jac, iRow, jCol);
	}
	else
	{
		// Return the jacobian values
		eval_jac_g_values(n, z, new_z, m, nele_jac, values);
	}

	return true;
}

/**
 * Get the sparsity structure of the jacobian
 *
 * Jacobian is filled from left to right, then from top to bottom.
 * (i.e. row by row.)
 */
void CollocationNLP::eval_jac_g_shape(Index n, Index m, Index nele_jac,
		Index* iRow, Index* jCol)
{
	int i = 0;

	int row = 1; // 1 indexed

	// Dynamics part
	int offset_x = 1;
	int offset_u = 1 + num_states * N;

	for (int k = 0; k < N - 1; k++) // Rows
	{
		for (int j = 0; j < num_states; j++) // Rectangular blocks
		{
			for (int l = 0; l < 2 * num_states; l++)	// x_k + x_k+1 block
			{
				iRow[i] = row;
				jCol[i] = l + offset_x;
				i++;
			}
			for (int l = 0; l < 2 * num_inputs; l++) 	// u_k + u_k+1 block
			{
				iRow[i] = row;
				jCol[i] = l + offset_u;
				i++;
			}
			row++;
		}
		offset_x += num_states;
		offset_u += num_inputs;
	}

	// Part of extra dynamic constraints
	if (num_dynamic_bounds > 0)
	{
		offset_x = 1;
		offset_u = 1 + num_states * N;
		for (int k = 0; k < N; k++)
		{
			for (int j = 0; j < num_dynamic_bounds; j++)
			{
				for (int l = 0; l < num_states; l++)	// q block
				{
					iRow[i] = row;
					jCol[i] = l + offset_x;
					i++;
				}
				for (int l = 0; l < num_inputs; l++)	// u block
				{
					iRow[i] = row;
					jCol[i] = l + offset_u;
					i++;
				}

				row++;
			}
			offset_x += num_states;
			offset_u += num_inputs;
		}
	}

	// Part of the extra constraints

	if (m_extra > 0)
	{
		int iRow_extra[nnz_jac_g_extra], jCol_extra[nnz_jac_g_extra];
		eval_jac_g_extra_shape(iRow_extra, jCol_extra);

		for (int j = 0; j < nnz_jac_g_extra; j++)
		{
			iRow[i] = row + iRow_extra[j] - 1; // Row was already increased in the previous iteration
			jCol[i] = jCol_extra[j];
			i++;
		}
		row += iRow_extra[nnz_jac_g_extra - 1]; // Increase for good order
	}

	assert(i == nele_jac);
}

/**
 * Get the values of the constraints jacobian
 */
void CollocationNLP::eval_jac_g_values(Index n, const Number* z, bool new_z,
		Index m, Index nele_jac, Number* values)
{
	set_variable_pointers(z);

	this->new_z = new_z;

	int i = 0;

	// Dynamics part

	const int diff_size = num_states * num_states;
	double dx_diff_x_k[diff_size];
	double dx_diff_x_kp1[diff_size];

	const int diff_size_inputs = num_states * num_inputs;
	double dx_diff_u_k[diff_size_inputs];
	double dx_diff_u_kp1[diff_size_inputs];

	// Get differentials
	for (int k = 0; k < N - 1; k++) // Points
	{
		/*
		 * Get derivatives like
		 * [ dx1 / x1; dx2 / x1; dx3 / x1 ... ]
		 */
		eval_dynamics_diff_x(x[k], u[k], dx_diff_x_k);
		eval_dynamics_diff_x(x[k + 1], u[k + 1], dx_diff_x_kp1);

		eval_dynamics_diff_u(x[k], u[k], dx_diff_u_k);
		eval_dynamics_diff_u(x[k + 1], u[k + 1], dx_diff_u_kp1);

		for (int j = 0; j < num_states; j++) // States
		{
			for (int l = 0; l < num_states; l++) // x_k block
			{
				values[i] = 0.5 * h * dx_diff_x_k[j + l * num_states];
				if (l == j)
				{
					values[i] += 1; // The +x[k] in g(x)
				}
				i++;
			}
			for (int l = 0; l < num_states; l++) // x_k+1 block
			{
				values[i] = 0.5 * h * dx_diff_x_kp1[j + l * num_states];
				if (l == j)
				{
					values[i] += -1; // The -x[k+1] in g(x)
				}
				i++;
			}
			for (int l = 0; l < num_inputs; l++) // u_k block
			{
				values[i++] = 0.5 * h * dx_diff_u_k[j + l * num_states];
			}
			for (int l = 0; l < num_inputs; l++) // u_k+1 block
			{
				values[i++] = 0.5 * h * dx_diff_u_kp1[j + l * num_states];
			}
		}
	}

	// Part for extra dynamic bounds

	if (num_dynamic_bounds > 0)
	{
		for (int k = 0; k < N; k++)
		{
			double g_extra_diff_x[num_dynamic_bounds * num_states];
			double g_extra_diff_u[num_dynamic_bounds * num_inputs];

			eval_dynamic_bounds_gradient(x[k], u[k], g_extra_diff_x,
					g_extra_diff_u);

			for (int j = 0; j < num_dynamic_bounds; j++)
			{
				for (int l = 0; l < num_states; l++)	// q block
				{
					values[i++] = g_extra_diff_x[j + l * num_dynamic_bounds];
				}
				for (int l = 0; l < num_inputs; l++)	// u block
				{
					values[i++] = g_extra_diff_u[j + l * num_dynamic_bounds];
				}
			}
		}
	}

	// Extra constraints

	if (m_extra > 0)
	{
		double values_extra[nnz_jac_g_extra];
		eval_jac_g_extra_values(x, u, values_extra);

		for (int j = 0; j < nnz_jac_g_extra; j++)
		{
			values[i++] = values_extra[j];
		}
	}

	assert(i == nele_jac);
}

/**
 * Either get the hessian shape or hessian values
 */
bool CollocationNLP::eval_h(Index n, const Number* z, bool new_z,
		Number obj_factor, Index m, const Number* lambda, bool new_lambda,
		Index nele_hess, Index* iRow, Index* jCol, Number* values)
{
	return false; // Do not use analytical Hessian!

	if (values == NULL)
	{
		eval_h_shape(n, m, nele_hess, iRow, jCol);
	}
	else
	{
		eval_h_values(n, z, new_z, obj_factor, m, lambda, new_lambda, nele_hess,
				values);
	}

	return true;
}

/**
 * Get shape of the hessian
 */
void CollocationNLP::eval_h_shape(Index n, const Index m, Index nele_hess,
		Index* iRow, Index* jCol)
{
	int i = 0;

	for (int col = 1; col <= n; col++)
	{
		for (int row = 1; row <= n; row++)
		{
			iRow[i] = row;
			jCol[i] = col;
			i++;
		}
	}
}

/**
 * Get values of the hessian
 */
void CollocationNLP::eval_h_values(Index n, const Number* z, bool new_z,
		Number obj_factor, Index m, const Number* lambda, bool new_lambda,
		Index nele_hess, Number* values)
{

	for (int i = 0; i < nele_hess; i++)
	{
		values[i] = 0.0;
	}
}

/**
 * Set sub-array pointers in class properties
 *
 * @param const double* z
 * @return void
 */
void CollocationNLP::set_variable_pointers(const double* z)
{
	if (this->z == z)
	{
		return; // No changes
	}

	this->z = z;

	// Create pointers to the first element of each vector per collocation point
	for (int k = 0; k < N; k++)
	{
		x[k] = &(z[k * num_states]); // x[i][k] = z[ k * num_states + i ]
		u[k] = &(z[N * num_states + k * num_inputs]); // u[i][k] = z[ N * num_states + i ]
	}
}

/**
 * Set writable sub-array pointers in class properties
 *
 * @param double* z
 * @return void
 */
void CollocationNLP::set_variable_pointers(double* z_write)
{
	if (this->z_write == z_write)
	{
		return; // No changes
	}

	this->z_write = z_write;

	// Create pointers to the first element of each vector per collocation point
	for (int k = 0; k < N; k++)
	{
		x_write[k] = &(z_write[k * num_states]); // x[k][0] = z[ k * num_states ]
		u_write[k] = &(z_write[N * num_states + k * num_inputs]); // u[k][0] = z[ N * num_states + k * num_inputs ]
	}
}

/**
 * Get bounds for additional constraint function g_extra(x)
 *
 * Zero by default, does not need to be overriden
 */
void CollocationNLP::get_dynamic_bounds(double* g_extra_l, double* g_extra_u)
{
	for (int i = 0; i < num_dynamic_bounds; i++)
	{
		g_extra_l[i] = 0.0;
		g_extra_u[i] = 0.0;
	}
}

/**
 * Get additional constraint function g(x)
 *
 * Default empty definition
 */
void CollocationNLP::eval_dynamic_bounds(const double* x, const double* u,
		double* g_extra)
{
	assert(num_dynamic_bounds == 0); // Function must be overridden if extra dynamic bounds are used
}

/**
 * Get gradient of additional constraint function g(x)
 *
 * Default empty definition
 */
void CollocationNLP::eval_dynamic_bounds_gradient(const double* x,
		const double* u, double* g_extra_diff_x, double* g_extra_diff_u)
{
	assert(num_dynamic_bounds == 0); // Function must be overridden if extra dynamic bounds are used
}

/**
 * Evaluate extra constraints
 *
 * Default empty definition
 */
void CollocationNLP::eval_g_extra(const double** x, const double** u,
		double* g_extra)
{
	assert(m_extra == 0); // Function must be overridden if extra constraints are used
}

/**
 * Get shape of extra constraints jacobian
 *
 * Default empty definition
 */
void CollocationNLP::eval_jac_g_extra_shape(int* iRow, int* jCol)
{
	assert(m_extra == 0); // Function must be overridden if extra constraints are used
}

/**
 * Get values of extra constraints jacobian
 *
 * Default empty definition
 */
void CollocationNLP::eval_jac_g_extra_values(const double** x, const double** u,
		double* values)
{
	assert(m_extra == 0); // Function must be overridden if extra constraints are used
}

/**
 * Set manual initial guess
 *
 * Overriding this function is not essential, by default the initial and final
 * configurations are linearly interpolated
 */
void CollocationNLP::get_initial_guess(double** x, double** u)
{
	assert(custom_initial_guess == false);
}

/**
 * Get bounds for extra constraints
 *
 * Zeros by default. This function does not need to be overridden.
 */
void CollocationNLP::get_bounds_info_extra(double* g_l_extra, double* g_u_extra)
{
	for (int i = 0; i < m_extra; i++)
	{
		g_l_extra[i] = 0.0;
		g_u_extra[i] = 0.0;
	}
}

/**
 * Interpolate input force/torque based on time
 */
void CollocationNLP::interpolateTime(double time, double* u_t, double* x_t)
{
	int k_left = (int) (time / h);
	int k_right = k_left + 1;

	double factor = (time - k_left * h) / h; // [0, 1]

	if (u_t)
	{
		for (int i = 0; i < num_inputs; i++)
		{
			if (k_left < 0 || k_right > N - 1)
			{
				u_t[i] = 0.0;
			}
			else
			{
				// Do linear interpolation
				u_t[i] = u[k_left][i] + factor * (u[k_right][i] - u[k_left][i]);
			}
		}
	}

	if (x_t)
	{
		for (int i = 0; i < num_states; i++)
		{
			if (k_left < 0 || k_right > N - 1)
			{
				x_t[i] = x[N - 1][i]; // Repeat last state
			}
			else
			{
				// Do linear interpolation
				x_t[i] = x[k_left][i] + factor * (x[k_right][i] - x[k_left][i]);
			}
		}
	}
}

/**
 * Interpolate between given points, for collocation points
 *
 * @param double[] x-axis (few points) (range from 0 to 1)
 * @param double[] y-axis (few points)
 * @param output[] interpolated result (many points)
 * @param int Length of the given points
 * @param int Length of the interpolated result
 */
void CollocationNLP::interpolateFromPoints(const double* xx, const double* yy,
		double* output, int size, int steps)
{
	int point_left = 0;
	int point_right = 1;

	for (int k = 0; k < steps; k++)
	{
		// Make sure to include first and last point
		double portion = (double) k / (double) (steps - 1);

		while (portion > xx[point_right])
		{
			point_left++;
			point_right++;
		}

		double factor = (portion - xx[point_left])
				/ (xx[point_right] - xx[point_left]);

		// Do linear interpolation
		output[k] = yy[point_left]
				+ factor * (yy[point_right] - yy[point_left]);
	}
}

