#include "MJAcrobotNLP.hpp"

#include <iostream>
#include <cmath>
#include <unistd.h>

using namespace Ipopt;
using namespace std;

/**
 * Constructor
 *
 * Use initialiser list to call non-default constructor of the member class
 *
 * x = [ theta1, theta2, dtheta1, dtheta2 ]
 *
 * ...where q is the horizontal position
 */
MJAcrobotNLP::MJAcrobotNLP() :
		CollocationNLP(15), model(
				"/home/robert/Documents/Master Assignment/MuJoCo/Models/acrobot.xml")
{
	num_q = 2;
	num_dq = 2;

	num_states = num_q + num_dq;
	num_inputs = 1;

	double t_0 = 0.0;
	double t_f = 0.6;

	h = (t_f - t_0) / (N - 1);

	custom_initial_guess = true;
}

/**
 * Destructor
 */
MJAcrobotNLP::~MJAcrobotNLP()
{

}

/**
 * Get initial configuration
 *
 * Set value to 1.0e19 to make it unbounded
 */
void MJAcrobotNLP::get_initial_config(double* x_0, double* u_0)
{
	// Position

	x_0[0] = 2.55;
	x_0[1] = -0.95; // Relative to other joint

	// Velocity
	x_0[2] = 0.0;
	x_0[3] = 0.0;

	u_0[0] = 1.0e19;
}

/**
 * Get final configuration
 *
 * Set value to 1.0e19 to make it unbounded
 */
void MJAcrobotNLP::get_final_config(double* x_f, double* u_f)
{
	// Positions
	x_f[0] = 0.0;
	x_f[1] = 2.0 * M_PI;
	// Velocities
	x_f[2] = 0.0;
	x_f[3] = 0.0;

	u_f[0] = 0.0;
}

/**
 * Get variable bounds
 *
 * Set values to 1.0e19 and -1.0e19 to make it unbounded
 */
void MJAcrobotNLP::get_variable_bound(double* x_u, double* x_l, double* u_u,
		double* u_l)
{
	/*
	 x_l[0] = -1.0e19;
	 x_u[0] = 1.0e19;

	 x_l[1] = -1.0e19;
	 x_u[1] = 1.0e19;
	 x_l[2] = -1.0e19;
	 x_u[2] = 1.0e19;

	 x_l[3] = -1.0e19;
	 x_u[3] = 1.0e19;
	 */

	x_l[0] = -3.0 * M_PI;
	x_u[0] = 3.0 * M_PI;

	x_l[1] = -3.0 * M_PI;
	x_u[1] = 3.0 * M_PI;

	double vel = 50;

	x_l[2] = -vel;
	x_u[2] = vel;

	x_l[3] = -vel;
	x_u[3] = vel;

	u_l[0] = -300.0;
	u_u[0] = -u_l[0];
}

/**
 * Calculate dynamics
 *
 * @return void
 */
void MJAcrobotNLP::eval_dynamics(const double* x, const double* u, double* dx)
{
	const double* qpos = &(x[0]);
	const double* qvel = &(x[2]);
	double qacc[2];
	model.get_dynamics(qpos, qvel, u, qacc);
	dx[0] = x[2];
	dx[1] = x[3];
	dx[2] = qacc[0];
	dx[3] = qacc[1];
}

/**
 * Get dynamics derivative w.r.t. x
 *
 * Return vector is stacked like:
 *
 * dx/x = [
 *    dx1/x
 *    dx2/x
 *    ...
 * ]
 *
 * @return void
 */
void MJAcrobotNLP::eval_dynamics_diff_x(const double* x, const double* u,
		double* dx_diff_x)
{
	//model.get_dynamics_diff_dq(q, dq, u, ddq_diff_dq);

	const double* qpos = &(x[0]);
	const double* qvel = &(x[2]);
	double qacc_diff_qvel[4], qacc_diff_qpos[4];

	model.get_dynamics_diff_qvel(qpos, qvel, u, qacc_diff_qvel);
	model.get_dynamics_diff_qpos(qpos, qvel, u, qacc_diff_qpos);

	dx_diff_x[0] = 0.0;
	dx_diff_x[1] = 0.0;
	dx_diff_x[2] = qacc_diff_qpos[0];
	dx_diff_x[3] = qacc_diff_qpos[1];

	dx_diff_x[4] = 0.0;
	dx_diff_x[5] = 0.0;
	dx_diff_x[6] = qacc_diff_qpos[2];
	dx_diff_x[7] = qacc_diff_qpos[3];

	dx_diff_x[8] = 1.0;
	dx_diff_x[9] = 0.0;
	dx_diff_x[10] = qacc_diff_qvel[0];
	dx_diff_x[11] = qacc_diff_qvel[1];

	dx_diff_x[12] = 0.0;
	dx_diff_x[13] = 1.0;
	dx_diff_x[14] = qacc_diff_qvel[2];
	dx_diff_x[15] = qacc_diff_qvel[3];
}

/**
 * Get dynamics derivative w.r.t. u
 *
 * @return void
 */
void MJAcrobotNLP::eval_dynamics_diff_u(const double* x, const double* u,
		double* dx_diff_u)
{
	//model.get_dynamics_diff_u(q, dq, u, ddq_diff_u);

	const double* qpos = &(x[0]);
	const double* qvel = &(x[2]);
	double qacc_diff_u[2];

	model.get_dynamics_diff_u(qpos, qvel, u, qacc_diff_u);

	dx_diff_u[0] = 0.0;
	dx_diff_u[1] = 0.0;
	dx_diff_u[2] = qacc_diff_u[0];
	dx_diff_u[3] = qacc_diff_u[1];
}

/**
 * Get initial guess (starting point) for NLP
 */
void MJAcrobotNLP::get_initial_guess(double** x, double** u)
{
	// Set user defined initial and final configuration
	double x_0[num_states], u_0[num_states];
	get_initial_config(x_0, u_0);

	double x_f[num_states], u_f[num_states];
	get_final_config(x_f, u_f);

	const int num_guess = 3;
	double q_guess[num_q][num_guess];
	double xx[num_guess] =
	{ 0.0, 0.3, 1.0 };

	// Initial and final config
	for (int i = 0; i < num_q; i++)
	{
		q_guess[i][0] = x_0[i] < 0.5e19 ? x_0[i] : 0.0;
		q_guess[i][2] = x_f[i] < 0.5e19 ? x_f[i] : 0.0;
	}

	// Program a wiggly bounce at the floor
	q_guess[0][1] = 1.64;
	q_guess[1][1] = 0.6;

	for (int i = 0; i < num_q; i++)
	{
		double interp[N];
		interpolateFromPoints(xx, q_guess[i], interp, num_guess, N);

		for (int k = 0; k < N; k++)
		{
			// Position
			x_write[k][i] = interp[k];
			// Velocity
			double deriv;
			if (k == 0)
			{
				deriv = (interp[k + 1] - interp[k]) / h;
			}
			else if (k == N - 1)
			{
				deriv = (interp[k] - interp[k - 1]) / h;
			}
			else
			{
				deriv = (interp[k + 1] - interp[k - 1]) / (2.0 * h);
			}
			x_write[k][i + num_q] = deriv;
		}
	}

	// Set input
	for (int k = 0; k < N; k++)
	{
		for (int i = 0; i < num_inputs; i++)
		{
			u_write[k][i] = 0.0; // Dynamically incorrect, but should fix itself
		}
	}

}

/**
 * Run at the end of the optimisation
 */
void MJAcrobotNLP::finalize_solution(SolverReturn status, Index n,
		const Number* z, const Number* bm_L, const Number* mb_U, Index m,
		const Number* g, const Number* lambda, Number obj_value,
		const IpoptData* ip_data, IpoptCalculatedQuantities* ip_cq)
{
	cout << endl;
	cout << "N = " << N << ";" << endl;
	cout << "h = " << h << ";" << endl;
	cout << "num_q = " << 2 << ";" << endl;
	cout << "num_dq = " << 2 << ";" << endl;

	cout << endl << "z = [";
	for (int i = 0; i < n; i++)
	{
		cout << z[i] << ",";
	}
	cout << "];" << endl << endl;

	set_variable_pointers(z); // Update pointers

	viewSolution();
	//testSolution();
}

/*
 * Use MuJoCoViewer to play the solution
 */
void MJAcrobotNLP::viewSolution()
{
	mjModel* m = model.getModelPtr();
	mjData* d = model.getDataPtr();
	double force;

	MuJoCoViewer viewer(m);

	viewer.opt.flags[mjVIS_CONTACTPOINT] = 1;
	//viewer.opt.flags[mjVIS_CONTACTFORCE] = 1;
	viewer.opt.flags[mjVIS_TRANSPARENT] = 1;
	//viewer.opt.label = mjLABEL_CONTACTFORCE;

	bool done = false;

	const double num_interp = 100.0;

	while (!done)
	{
		double tmupdate = glfwGetTime();

		mj_resetData(m, d); // Reset simulation

		for (int j = 0; j < 2; j++) // Set initial position and velocity
		{
			d->qpos[j] = x[0][j];
			d->qvel[j] = x[0][j + 2];
		}

		// Loop over collocation points
		int k_max = 1.3 * N; // Run a little longer to pause at the end

		for (int k = 0; k < k_max; k++)
		{
			// Loop over interpolated data in real time
			for (double interp = 0.0; interp < 1.0; interp += 1.0 / num_interp)
			{
				double tmstart = glfwGetTime();

				if (k < N - 1)
				{
					for (int j = 0; j < 2; j++) // Only take positions from state
					{
						d->qpos[j] = x[k][j] + (x[k + 1][j] - x[k][j]) * interp;
					}
					force = u[k][0] + (u[k + 1][0] - u[k][0]) * interp;
				}

				mj_forward(m, d); // Necessary for proper render

				if (glfwGetTime() - tmupdate > 1.0 / 60.0) // Update when last frame is 1/60 seconds old
				{
					viewer.updateScene(d);

					// Add arrow
					double base[3] =
					{ 0.0, 0.2, 0.0 };
					double vector[3] =
					{ 0.0, force * 0.005, 0.0 };

					viewer.drawArrow(vector, base);

					viewer.update(); // Update at 60 fps

					tmupdate = glfwGetTime();
				}

				if (viewer.windowShouldClose())
				{
					done = true;
					return; // Stop
				}

				double wait_time = h / num_interp;
				while (glfwGetTime() - tmstart < wait_time)
				{
					usleep(100); // Sleep for 0.1 millisecond
				}
			}
		}
	}
}

/**
 * Run feed-forward control from solutin
 */
void MJAcrobotNLP::testSolution()
{
	mjModel* m = model.getModelPtr();
	mjData* d = model.getDataPtr();

	MuJoCoViewer viewer(m);

	bool done = false;

	// Repeat simulation
	while (!done)
	{
		mj_resetData(m, d); // Reset simulation

		for (int j = 0; j < 2; j++) // Set initial position and velocity
		{
			d->qpos[j] = x[0][j];
			d->qvel[j] = x[0][j + 2];
		}

		double simstart = glfwGetTime();

		double force[num_inputs];
		double x_target[num_states];
		double feedback;

		// Run for a little longer than the optimisation time span
		while (d->time < 2.0f * h * N)
		{
			// Record cpu time at start of iteration
			double tmstart = glfwGetTime();

			if (viewer.windowShouldClose())
			{
				done = true;
				return; // Stop
			}

			viewer.updateScene(d);

			// Add arrow
			double base[3] =
			{ d->qpos[0], -0.2, 0.0 };
			double vector[3] =
			{ d->ctrl[0] * 0.005, 0.0, 0.0 };

			viewer.drawArrow(vector, base);

			viewer.update(); // Render

			// Bring simulation up-to-date
			while (d->time < (tmstart - simstart))
			{
				interpolateTime(d->time, force, x_target);

				double error = x_target[1] - d->qpos[1];
				double K = -10.0f;
				feedback = error * K;
				d->ctrl[0] = force[0] + feedback;

				mj_step(m, d);
			}

			// Wait until a total of 1 frame time has passed
			while (glfwGetTime() - tmstart < 1.0 / 60.0)
			{
				usleep(100); // Sleep for 0.1 millisecond to prevent a busy wait
			}
		}
	}
}
