#ifndef MYNLP_
#define MYNLP_

#include "../CollocationNLP.hpp"
#include "../MuJoCoModel/MuJoCoModel.hpp"
#include "../MuJoCoViewer/MuJoCoViewer.hpp"

using namespace Ipopt;

class MJAcrobotNLP: public CollocationNLP
{
public:
	MJAcrobotNLP();
	virtual ~MJAcrobotNLP();

	/** Get forward dynamics */
	virtual void eval_dynamics(const double* x, const double* u, double* dx);

	/** Get (\partial dx / \partial x) */
	virtual void eval_dynamics_diff_x(const double* x, const double* u,
			double* dx_diff_x);

	/** Get (\partial dx / \partial u) */
	virtual void eval_dynamics_diff_u(const double* x, const double* u,
			double* dx_diff_u);

	/** Get initial configuration */
	virtual void get_initial_config(double* x_0, double* u_0);

	/** Get final configuration */
	virtual void get_final_config(double* x_f, double* u_f);

	/** Get variable bounds */
	virtual void get_variable_bound(double* x_u, double* x_l, double* u_u,
			double* u_l);

	/** Get initial guess */
	void get_initial_guess(double** x, double** u);

	/** This method is called when the algorithm is complete so the TNLP can store/write the solution */
	virtual void finalize_solution(SolverReturn status, Index n,
			const Number* z, const Number* bm_L, const Number* bm_U, Index m,
			const Number* g, const Number* lambda, Number obj_value,
			const IpoptData* ip_data, IpoptCalculatedQuantities* ip_cq);

private:
	MJAcrobotNLP(const MJAcrobotNLP&);
	MJAcrobotNLP& operator=(const MJAcrobotNLP&);

	int num_q;
	int num_dq;

	void viewSolution();
	void testSolution();

	/** Use MoJoCo as a back-end */
	MuJoCoModel model;

	/** Use viewer to show results */
	//MuJoCoViewer viewer;
};

#endif
