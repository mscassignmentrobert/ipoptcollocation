#include "mujoco/mujoco.h"
#include "mujoco/glfw3.h"

#include "MuJoCoViewer.hpp"

#include <iostream>
#include <string.h>

/**
 * Constructor
 *
 * @param const MuJoCoModel* Model to be linked to
 */
MuJoCoViewer::MuJoCoViewer(const mjModel* m)
{
	if (m)
	{
		initialize(m);
	}
	else
	{
		this->m = nullptr;
	}
	figure_enabled = false;
	figure_num_points = 200;
}

/**
 * Destructor
 */
MuJoCoViewer::~MuJoCoViewer()
{
	glfwDestroyWindow(window);

	mjv_freeScene(&scn);
	mjr_freeContext(&con);

	// Terminate GLFW (crashes with Linux NVidia drivers)
#if defined(__APPLE__) || defined(_WIN32)
    glfwTerminate();
#endif
}

/**
 * Initialise viewer with model
 */
void MuJoCoViewer::initialize(const mjModel* m)
{
	if (!glfwInit())
	{
		std::cout << "Failed to initialise GLFW" << std::endl;
	}

	// Create window, make OpenGL context current, request v-sync
	window = glfwCreateWindow(1200, 900, "MuJoCo Viewer", NULL, NULL);

	glfwMakeContextCurrent(window);
	glfwSwapInterval(1);

	// Initialise visualisation data structures
	mjv_defaultCamera(&cam);
	mjv_defaultOption(&opt);
	mjv_defaultScene(&scn);
	mjr_defaultContext(&con);

	// Place pointer to this object inside the window reference
	glfwSetWindowUserPointer(window, this);

	// Install GLFW mouse and keyboard callbacks
	glfwSetKeyCallback(window, cb_keyboard);
	glfwSetCursorPosCallback(window, cb_mouse_move);
	glfwSetMouseButtonCallback(window, cb_mouse_button);
	glfwSetScrollCallback(window, cb_scroll);

	// Initialise controls
	button_left = button_middle = button_right = false;
	mouse_x = mouse_y = 0.0;

	this->m = m;

	// Create scene and context
	mjv_makeScene(m, &scn, 1000);
	mjr_makeContext(m, &con, mjFONTSCALE_150);
}

/**
 * Return true when window was closed
 */
bool MuJoCoViewer::windowShouldClose()
{
	return glfwWindowShouldClose(window);
}

/**
 * Update mjScene
 *
 * Useful to call manually if you want to add abstract things
 * to the scene before rendering
 */
void MuJoCoViewer::updateScene(mjData* d)
{
	mjv_updateScene(m, d, &opt, NULL, &cam, mjCAT_ALL, &scn);
}

/**
 * Render new scene and poll for glwfw events
 *
 * Leave d empty to not update the scene
 *
 * @param mjData* d = nullptr
 */
void MuJoCoViewer::update(mjData* d)
{
	// Get framebuffer viewport
	mjrRect viewport =
	{ 0, 0, 0, 0 };
	glfwGetFramebufferSize(window, &viewport.width, &viewport.height);

	// Update scene and render
	if (d)
	{
		updateScene(d);
	}
	mjr_render(viewport, &scn, &con);

	// Render figure
	if (figure_enabled)
	{
		mjrRect viewport =
		{ 0, 0, 300, 300 };
		mjr_figure(viewport, &figure, &con);
	}

	// swap OpenGL buffers (blocking call due to v-sync)
	glfwSwapBuffers(window);

	// Process events and run callbacks
	glfwPollEvents();
}

/**
 * Callback for keyboard event
 */
void MuJoCoViewer::cb_keyboard(GLFWwindow* window, int key, int scancode,
		int act, int mods)
{
	// Do nothing
	//auto my_this = MuJoCoViewer::getViewerPtr(window);
}

/**
 * Callback for mouse buttons
 */
void MuJoCoViewer::cb_mouse_button(GLFWwindow* window, int button, int act,
		int mods)
{
	auto viewer = MuJoCoViewer::getViewerPtr(window);

	// Update button state
	viewer->button_left = (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT)
			== GLFW_PRESS);
	viewer->button_middle = (glfwGetMouseButton(window,
	GLFW_MOUSE_BUTTON_MIDDLE) == GLFW_PRESS);
	viewer->button_right = (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT)
			== GLFW_PRESS);

	// Update mouse position
	glfwGetCursorPos(window, &(viewer->mouse_x), &(viewer->mouse_y));
}

/**
 * Callback for mouse move
 */
void MuJoCoViewer::cb_mouse_move(GLFWwindow* window, double xpos, double ypos)
{
	auto viewer = MuJoCoViewer::getViewerPtr(window);

	// No buttons down: nothing to do
	if (!viewer->button_left && !viewer->button_middle && !viewer->button_right)
	{
		return;
	}

	// Compute mouse displacement, save
	double dx = xpos - viewer->mouse_x;
	double dy = ypos - viewer->mouse_y;
	viewer->mouse_x = xpos;
	viewer->mouse_y = ypos;

	// Get current window size
	int width, height;
	glfwGetWindowSize(window, &width, &height);

	// Get shift key state
	bool mod_shift = (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS
			|| glfwGetKey(window, GLFW_KEY_RIGHT_SHIFT) == GLFW_PRESS);

	// determine action based on mouse button
	mjtMouse action;
	if (viewer->button_right)
	{
		action = mod_shift ? mjMOUSE_MOVE_H : mjMOUSE_MOVE_V;
	}
	else if (viewer->button_left)
	{
		action = mod_shift ? mjMOUSE_ROTATE_H : mjMOUSE_ROTATE_V;
	}
	else
	{
		action = mjMOUSE_ZOOM;
	}

	// Move camera
	mjv_moveCamera(viewer->m, action, dx / height, dy / height, &(viewer->scn),
			&(viewer->cam));
}

/**
 * Callback for scrolling
 */
void MuJoCoViewer::cb_scroll(GLFWwindow* window, double xoffset, double yoffset)
{
	auto viewer = MuJoCoViewer::getViewerPtr(window);

	// Emulate vertical mouse motion = 5% of window height
	mjv_moveCamera(viewer->m, mjMOUSE_ZOOM, 0, 0.05 * yoffset, &(viewer->scn),
			&(viewer->cam));
}

/**
 * Get MuJoCoViewer pointer from window object
 */
MuJoCoViewer* MuJoCoViewer::getViewerPtr(GLFWwindow* window)
{
	return static_cast<MuJoCoViewer*>(glfwGetWindowUserPointer(window));
}

/**
 * Create arrow with size of given vector, starting from a base
 *
 * Arrow will only show up if added after scene update!
 *
 * @param const double[3] vector
 * @param const double[3] base
 */
void MuJoCoViewer::drawArrow(const double vector[3], const double base[3])
{
	// Check if there is space left
	if (scn.ngeom >= scn.maxgeom)
	{
		return;
	}

	mjvGeom* arrow = scn.geoms + scn.ngeom;

	// Create geom
	arrow->type = mjGEOM_ARROW;
	arrow->dataid = -1;
	arrow->objtype = mjOBJ_SITE;
	arrow->objid = -1;
	arrow->category = mjCAT_DECOR;
	arrow->texid = -1;
	arrow->texuniform = 0;
	arrow->texrepeat[0] = 1;
	arrow->texrepeat[1] = 1;
	arrow->emission = 0;
	arrow->specular = 0.5;
	arrow->shininess = 0.5;
	arrow->reflectance = 0;
	arrow->label[0] = 0;
	arrow->rgba[0] = 1.0f;
	arrow->rgba[1] = 0.1f;
	arrow->rgba[2] = 0.1f;
	arrow->rgba[3] = 1.0f;

	arrow->pos[0] = (float) base[0];
	arrow->pos[1] = (float) base[1];
	arrow->pos[2] = (float) base[2];

	float magnitude = (float) mju_norm3(vector);
	arrow->size[0] = 0.02f;
	arrow->size[1] = arrow->size[0];
	arrow->size[2] = magnitude;

	mjtNum quat[4], mat[9];
	mju_quatZ2Vec(quat, vector);
	mju_quat2Mat(mat, quat);
	mju_n2f(arrow->mat, mat, 9);

	// Register new geom
	scn.ngeom++;
}

/**
 * Add MuJoCo figure
 */
void MuJoCoViewer::enableFigure()
{
	mjv_defaultFigure(&figure);
	figure.figurergba[3] = 0.5f;

	// set flags
	figure.flg_extend = 1;
	figure.flg_barplot = 0;
	figure.flg_symmetric = 1;

	// title
	strcpy(figure.title, "Figure");

	// y-tick nubmer format
	strcpy(figure.yformat, "%.0f");

	// grid size
	figure.gridsize[0] = 2;
	figure.gridsize[1] = 3;

	// minimum range
	figure.range[0][0] = 0;
	figure.range[0][1] = 0;
	figure.range[1][0] = -1;
	figure.range[1][1] = 1;

	figure_enabled = true;

	figure.linepnt[0] = figure_num_points; // Points in line

	for (int i = 0; i < figure_num_points; i++)
	{
		figure.linedata[0][2*i + 0] = 0.01f * i;
		figure.linedata[0][2*i + 1] = 0.0f;
	}
}

/**
 * Set new value in figure
 */
void MuJoCoViewer::updateFigure(float val)
{
	/*for (int i = 0; i < figure_num_points; i++)
	{
		figure.linedata[0][2*i + 0] = (float)i;
		figure.linedata[0][2*i + 1] = 5.0f + i;
	}*/

	// Scooch data
	for (int i = 0; i < figure_num_points - 1; i++)
	{
		// Scooch two places because of x and y data
		//figure.linedata[0][2 * i + 0] = figure.linedata[0][2 * i + 2];
		figure.linedata[0][2 * i + 1] = figure.linedata[0][2 * i + 3];
	}

	//figure.linedata[0][2 * figure_num_points - 2] = time;
	figure.linedata[0][2 * figure_num_points - 1] = val;
}

