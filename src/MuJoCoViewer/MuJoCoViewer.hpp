#ifndef MUJOCOVIEWER_H_
#define MUJOCOVIEWER_H_

#include "mujoco/mujoco.h"
#include "mujoco/glfw3.h"

// Use forward declaration so we can return it from itself
class MuJoCoViewer;

class MuJoCoViewer
{
public:
	MuJoCoViewer(const mjModel* m = nullptr);
	virtual ~MuJoCoViewer();

	/** Return true when window was closed */
	bool windowShouldClose();

	/** Regular window updates to keep it alive */
	void update(mjData* d = nullptr);

	/** Manually update scene */
	void updateScene(mjData* d);

	/** Create window */
	void initialize(const mjModel* m);

	/** Callback functions for GLFW events */
	static void cb_keyboard(GLFWwindow* window, int key, int scancode, int act,
			int mods);
	static void cb_mouse_button(GLFWwindow* window, int button, int act,
			int mods);
	static void cb_mouse_move(GLFWwindow* window, double xpos, double ypos);
	static void cb_scroll(GLFWwindow* window, double xoffset, double yoffset);

	/** Get original object reference which was tucked inside the window */
	static MuJoCoViewer* getViewerPtr(GLFWwindow* window);

	/** Add an arrow to the current scene */
	void drawArrow(const double vector[3], const double base[3]);

	/** Create the figure */
	void enableFigure();

	/** Set new value in figure */
	void updateFigure(float val);

	/** Mouse buttons */
	bool button_left, button_middle, button_right;
	/** Last known mouse position */
	double mouse_x, mouse_y;

	/** Pointer to MJ model object */
	const mjModel* m;

	/** MJ camera object */
	mjvCamera cam;
	/** MJ visualisations options */
	mjvOption opt;
	/** MJ scene */
	mjvScene scn;
	/** MJ GPU context */
	mjrContext con;

private:
	GLFWwindow* window;

	mjvFigure figure;
	bool figure_enabled;
	float figure_val;
	int figure_num_points;

};

#endif /* MUJOCOVIEWER_H_ */
