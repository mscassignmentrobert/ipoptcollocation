clear;
clc;

p = parameters();

L_step = 0.4;
h_hips = (p.L(1) + p.L(2)) * 0.95;

sol_1 = inverse_kinematics_2link(-L_step / 2, h_hips, p.L(1), p.L(2));

sol_2 = inverse_kinematics_2link(L_step / 2, h_hips, p.L(1), p.L(2));

q = zeros(5,1);
q(1) = sol_1(1,2);
q(2) = sol_1(2,2);

q(4) = sol_2(1,1);
q(5) = sol_2(2,1);

plot_biped(q, p);