clear;
clc;
close all;

syms L1 L2 q1 q2 x y real;

x_eq = -L1 * sin(q1) - L2 * sin(q2);
y_eq =  L1 * cos(q1) + L2 * cos(q2);

sol = solve([x_eq; y_eq] == [x; y], [q1; q2], 'ReturnConditions', false);

q_sol1 = [sol.q1(1); sol.q2(1)];
q_sol2 = [sol.q1(2); sol.q2(2)];

matlabFunction([q_sol1, q_sol2], 'File', 'inverse_kinematics_2link', 'vars', {x, y, L1, L2});