#ifndef COLLOCATIONNLP_HPP_
#define COLLOCATIONNLP_HPP_

#include <coin/IpTNLP.hpp>

using namespace Ipopt;

/**
 * Template for collocation NLP
 *
 * Trapezoid integration is used between points.
 *
 * Extend this class and define the dynamics function, dynamics partial derivatives
 * and some settings. This helper will build the actual constraints and optimization
 * function to solve the problem.
 *
 * Differentials of the dynamics function are needed to build the gradients and Jacobians
 * required for efficient optimization.
 *
 * About naming:
 * IPOPT uses the symbol `x` for the optimisation vector. We use the symbol `z` instead,
 * as is conventional in math. We use `x` for the state, i.e. a combination of postions
 * and velocities.
 * IPOPT uses the symbol `z` for the bounds multipliers. Instead we renamed those to
 * `bm`.
 */
class CollocationNLP: public TNLP
{
public:

	/** Default constructor */
	CollocationNLP(int N);

	/** Default destructor */
	virtual ~CollocationNLP();

	/** @name Essential functions (dynamics, starting point, etc.) */
	//@{
	/** Get forward dynamics */
	virtual void eval_dynamics(const double* x, const double* u,
			double* dx) = 0;

	/** Get (\partial dx / \partial x) */
	virtual void eval_dynamics_diff_x(const double* x, const double* u,
			double* dx_diff_x) = 0;

	/** Get (\partial dx / \partial u) */
	virtual void eval_dynamics_diff_u(const double* x, const double* u,
			double* dx_diff_u) = 0;

	/** Get initial configuration */
	virtual void get_initial_config(double* x_0, double* u_0) = 0;

	/** Get final configuration */
	virtual void get_final_config(double* x_f, double* u_f) = 0;

	/** Get variable bounds */
	virtual void get_variable_bound(double* x_u, double* x_l, double* u_u,
			double* u_l) = 0;

	//@}

	/** @name Extra functions: dynamic bounds per collocation point */
	//@{
	/** Get bounds for additional constraint function */
	virtual void get_dynamic_bounds(double* g_extra_l, double* g_extra_u);

	/** Get additional constraint function */
	virtual void eval_dynamic_bounds(const double* x, const double* u,
			double* g_extra);

	/** Get gradient of additional constraint function */
	virtual void eval_dynamic_bounds_gradient(const double* x, const double* u,
			double* g_extra_diff_x, double* g_extra_diff_u);
	//@}

	/** @name Extra functions: Completely open extra contraints for entire vector `z` */
	//@{
	/** Get initial guess */
	virtual void get_initial_guess(double** x, double** u);

	/** Evaluate extra constraints */
	virtual void eval_g_extra(const double** x, const double** u,
			double* g_extra);

	/** Get bounds for extra constraints */
	virtual void get_bounds_info_extra(double* g_l_extra, double* g_u_extra);

	/** Get shape of extra constraints jacobian */
	virtual void eval_jac_g_extra_shape(int* iRow, int* jCol);

	/** Get values of extra constraints jacobian */
	virtual void eval_jac_g_extra_values(const double** x, const double** u,
			double* values);

	//@}

	/** @name Overloaded from TNLP */
	//@{
	/** Method to return some info about the nlp */
	virtual bool get_nlp_info(Index& n, Index& m, Index& nnz_jac_g,
			Index& nnz_h_lag, IndexStyleEnum& index_style);

	/** Method to return the bounds for my problem */
	virtual bool get_bounds_info(Index n, Number* z_l, Number* z_u, Index m,
			Number* g_l, Number* g_u);

	/** Method to return the starting point for the algorithm */
	virtual bool get_starting_point(Index n, bool init_z, Number* z,
			bool init_bm, Number* bm_L, Number* bm_U, Index m, bool init_lambda,
			Number* lambda);

	/** Method to return the objective value */
	virtual bool eval_f(Index n, const Number* z, bool new_z,
			Number& obj_value);

	/** Method to return the gradient of the objective */
	virtual bool eval_grad_f(Index n, const Number* z, bool new_z,
			Number* grad_f);

	/** Method to return the constraint residuals */
	virtual bool eval_g(Index n, const Number* z, bool new_z, Index m,
			Number* g);

	/** Method to return:
	 *   1) The structure of the jacobian (if "values" is NULL)
	 *   2) The values of the jacobian (if "values" is not NULL)
	 */
	virtual bool eval_jac_g(Index n, const Number* z, bool new_z, Index m,
			Index nele_jac, Index* iRow, Index* jCol, Number* values);

	/** Method to return:
	 *   1) The structure of the hessian of the lagrangian (if "values" is NULL)
	 *   2) The values of the hessian of the lagrangian (if "values" is not NULL)
	 */
	virtual bool eval_h(Index n, const Number* z, bool new_z, Number obj_factor,
			Index m, const Number* lambda, bool new_lambda, Index nele_hess,
			Index* iRow, Index* jCol, Number* values);

	//@}

	/** Get input and trajectory interpolated by time */
	void interpolateTime(double time, double* u_t, double* x_t = nullptr);

	/** Interpolate output between points */
	void interpolateFromPoints(const double* xx, const double* yy, double* output, int size, int steps);

	/** Number of states */
	int num_states;
	/** Number of inputs (forces and torques) */
	int num_inputs;
	/** Number of variables per collocation point */
	int size;
	/** Number of collocation points */
	int N;
	/** Time step, t_{k+1} - t_k*/
	double h;

	/** Number of dynamic bounds per collocation point */
	int num_dynamic_bounds;

	/** Amount of user-specified constraints */
	int m_extra;
	/** Amount of non-zeroes in user-specified constraints jacobian */
	int nnz_jac_g_extra;

	/** Set to true to provide a custom intial guess function */
	bool custom_initial_guess;

	/** Set initial configuration as constraint (default: true) */
	bool constraint_config_initial;
	/** Set final configuration as constraint (default: true) */
	bool constraint_config_final;

	/** When false this z has been processed before */
	bool new_z;

protected:
	/** Set sub-array pointers based on complete optimization set z */
	void set_variable_pointers(const double* z);
	void set_variable_pointers(double* z_write);

	/** Pointers to (parts of) the current optimization variables */
	const double* z;
	const double** x;
	const double** u;

	/** Writable pointers to (parts of) the current optimization variable */
	double* z_write;
	double** x_write;
	double** u_write;

private:

	/**@name Methods to block default compiler methods.
	 * The compiler automatically generates the following three methods.
	 *  Since the default compiler implementation is generally not what
	 *  you want (for all but the most simple classes), we usually
	 *  put the declarations of these methods in the private section
	 *  and never implement them. This prevents the compiler from
	 *  implementing an incorrect "default" behavior without us
	 *  knowing. (See Scott Meyers book, "Effective C++")
	 *
	 */
	//@{
	//  MyNLP();
	CollocationNLP(const CollocationNLP&);
	CollocationNLP& operator=(const CollocationNLP&);
	//@}

	/** Method to return the structure of the jacobian */
	void eval_jac_g_shape(Index n, const Index m, Index nele_jac, Index* iRow,
			Index* jCol);

	/** Method to return the values of the jacobian */
	void eval_jac_g_values(Index n, const Number* z, bool new_z, Index m,
			Index nele_jac, Number* values);

	/** Method to return the structure of the hessian */
	void eval_h_shape(Index n, const Index m, Index nele_hess, Index* iRow,
			Index* jCol);

	/** Method to return the values of the hessian */
	void eval_h_values(Index n, const Number* z, bool new_z, Number obj_factor,
			Index m, const Number* lambda, bool new_lambda, Index nele_hess,
			Number* values);
};

#endif /* COLLOCATIONNLP_HPP_ */
