#include "MJPendulum3DNLP.hpp"
#include "../DoubleMatrix/DoubleMatrix.hpp"

#include <iostream>
#include <cmath>
#include <unistd.h>
#include <assert.h>

using namespace Ipopt;
using namespace std;

/**
 * Constructor
 *
 * Use initialiser list to call non-default constructor of the member class
 *
 * x = [ q, theta, dq, dtheta ]
 *
 * ...where q is the horizontal position
 */
MJPendulum3DNLP::MJPendulum3DNLP() :
		CollocationNLP(3), model(
				"/home/robert/Documents/Master Assignment/MuJoCo/Models/pendulum_3d.xml")
{
	num_q = 6;
	num_dq = 5;

	num_states = num_q + num_dq;
	num_inputs = 2;

	size = num_states + num_inputs;

	double t_0 = 0.0;
	double t_f = 1.0;

	h = (t_f - t_0) / (N - 1);

	num_dynamic_bounds = 0;

	/**
	 * Quaternion unit length
	 */
#if CONST_3D_QUATERNION
	num_dynamic_bounds += 1;
#endif

	m_extra = 0;
	nnz_jac_g_extra = 0;

	/**
	 * Configs are not used as constraints
	 * They are still used for the initial guess
	 */
	custom_initial_guess = true;
	constraint_config_initial = true;
	constraint_config_final = true;

	L1 = L2 = 0.5;
}

/**
 * Destructor
 */
MJPendulum3DNLP::~MJPendulum3DNLP()
{

}

/**
 * Get initial configuration
 *
 * Set value to 1.0e19 to make it unbounded
 */
void MJPendulum3DNLP::get_initial_config(double* x_0, double* u_0)
{
	mjtNum axis[3] =
	{ 1.0, 0.0, 0.0 };
	mju_axisAngle2Quat(&x_0[0], axis, 0.05 * M_PI);

	int i;
	for (i = 4; i < num_q; i++)
	{
		x_0[i] = 0.0;
	}

	// Velocity
	for (; i < num_states; i++)
	{
		//x_0[i] = 1.0e19;
		x_0[i] = 0.0;
	}

	// Input
	for (int j = 0; j < num_inputs; j++)
	{
		u_0[j] = 1.0e19;
		//u_0[i] = 0.0;
	}
}

/**
 * Get final configuration
 *
 * Set value to 1.0e19 to make it unbounded
 */
void MJPendulum3DNLP::get_final_config(double* x_f, double* u_f)
{
	mjtNum axis[3] =
	{ 1.0, 0.0, 0.0 };
	mju_axisAngle2Quat(&x_f[0], axis, 0.0 * M_PI);

	int i;
	for (i = 4; i < num_q; i++)
	{
		x_f[i] = 0.0;
	}

	// Velocity
	for (; i < num_states; i++)
	{
		//x_f[i] = 1.0e19;
		x_f[i] = 0.0;
	}

	// Input
	for (int j = 0; j < num_inputs; j++)
	{
		u_f[j] = 1.0e19;
		//u_f[j] = 0.0;
	}
}

/**
 * Get variable bounds
 *
 * Set values to 1.0e19 and -1.0e19 to make it unbounded
 */
void MJPendulum3DNLP::get_variable_bound(double* x_u, double* x_l, double* u_u,
		double* u_l)
{
	// Quaternion
	for (int i = 0; i < 4; i++)
	{
		x_l[i] = -1.0;
		x_u[i] = 1.0;
	}
	// Rotations
	for (int i = 4; i < num_q; i++)
	{
		x_l[i] = -1.0e19;
		x_u[i] = 1.0e19;
	}
	// Velocities
	for (int i = num_q; i < num_q + num_dq; i++)
	{
		x_l[i] = -10.0;
		x_u[i] = 10.0;
	}

	for (int i = 0; i < num_inputs; i++)
	{
		u_l[i] = -200;
		u_u[i] = 200;
	}
}

/**
 * Calculate dynamics
 *
 * @return void
 */
void MJPendulum3DNLP::eval_dynamics(const double* x, const double* u,
		double* dx)
{
	const double* qpos = &x[0];
	const double* qvel = &x[num_q];
	double qacc[num_dq];
	model.get_dynamics(qpos, qvel, u, qacc);

	// Convert qvel to qpos_dot
	qvel_to_qpos_dot(qpos, qvel, &dx[0]);

	// Set acceleration
	for (int i = 0; i < num_dq; i++)
	{
		dx[i + num_q] = qacc[i];
	}
}

/**
 * Get dynamics derivative w.r.t. x
 *
 * Return vector is stacked like:
 *
 * dx/x = [
 *    dx1/x1
 *    dx2/x1
 *    ...
 * ]
 *
 * @return void
 */
void MJPendulum3DNLP::eval_dynamics_diff_x(const double* x, const double* u,
		double* dx_diff_x)
{
	const double* qpos = &x[0];
	const double* qvel = &x[num_dq];
	double qacc_diff_qvel[num_dq * num_dq], qacc_diff_qpos[num_dq * num_q];

	model.get_dynamics_diff_qvel(qpos, qvel, u, qacc_diff_qvel);
	model.get_dynamics_diff_qpos(qpos, qvel, u, qacc_diff_qpos);

	// diff q_pos_dot / q_pos
	for (int col = 0; col < num_q; col++)
	{
		for (int row = 0; row < num_q; row++)
		{
			dx_diff_x[row + col * num_states] = 0.0;
		}
	}
	// diff q_acc / q_pos
	int index = 0;
	for (int col = 0; col < num_q; col++)
	{
		for (int row = 0; row < num_dq; row++)
		{
			dx_diff_x[num_q + row + col * num_states] = qacc_diff_qpos[index++];
		}
	}
	// diff q_pos_dot / qvel
	double qmat[16];
	quaternion_to_matrix(&qpos[0], qmat);
	for (int col = 0; col < num_dq; col++)
	{
		for (int row = 0; row < num_q; row++)
		{
			double val;
			if (row < 4 && col < 3)
			{
				val = 0.5 * qmat[row + 4 + col * 4];
			}
			else
			{
				val = ((row - 1) == col) ? 1.0 : 0.0;
			}
			dx_diff_x[row + (num_q + col) * num_states] = val;
		}
	}
	// Diff q_acc / q_vel
	index = 0;
	for (int col = 0; col < num_q; col++)
	{
		for (int row = 0; row < num_dq; row++)
		{
			dx_diff_x[num_q + row + (num_q + col) * num_states] =
					qacc_diff_qvel[index++];
		}
	}
}

/**
 * Get dynamics derivative w.r.t. u
 *
 * @return void
 */
void MJPendulum3DNLP::eval_dynamics_diff_u(const double* x, const double* u,
		double* dx_diff_u)
{
	const double* qpos = &x[0];
	const double* qvel = &x[num_q];
	double qacc_diff_u[num_dq * num_inputs];

	model.get_dynamics_diff_u(qpos, qvel, u, qacc_diff_u);

	for (int col = 0; col < num_inputs; col++)
	{
		for (int row = 0; row < num_q; row++)
		{
			dx_diff_u[row + col * num_states] = 0.0;
		}
	}
	int index = 0;
	for (int col = 0; col < num_inputs; col++)
	{
		for (int row = 0; row < num_dq; row++)
		{
			dx_diff_u[num_q + row + col * num_states] = qacc_diff_u[index];
			index++;
		}
	}
}

/**
 * Convert qvel into qpos derivative
 *
 * This conversion takes quaternions into account
 */
void MJPendulum3DNLP::qvel_to_qpos_dot(const double* qpos, const double* qvel,
		double* qpos_dot)
{
	// Convert the first four elements making up a quaternion
	mju_derivQuat(&qpos_dot[0], &qpos[0], &qvel[0]);

	// Simply copy the other ones
	for (int i = 4; i < num_q; i++)
	{
		qpos_dot[i] = qvel[i - 1];
	}
}

/**
 * Convert quaternion to 4x4 matrix form (not the rotation matrix!)
 */
void MJPendulum3DNLP::quaternion_to_matrix(const double* quat, double* qmat)
{
	qmat[0] = quat[0]; // Column 1
	qmat[1] = quat[1];
	qmat[2] = quat[2];
	qmat[3] = quat[3];

	qmat[4] = -quat[1]; // Column 2
	qmat[5] = quat[0];
	qmat[6] = -quat[3];
	qmat[7] = quat[2];

	qmat[8] = -quat[2]; // Column 3
	qmat[9] = quat[3];
	qmat[10] = quat[0];
	qmat[11] = -quat[1];

	qmat[12] = -quat[3]; // Column 4
	qmat[13] = -quat[2];
	qmat[14] = quat[1];
	qmat[15] = quat[0];
}

/**
 * Get initial guess (starting point) for NLP
 */
void MJPendulum3DNLP::get_initial_guess(double** x_write, double** u_write)
{
	// Set user defined initial and final configuration
	double x_0[num_states], u_0[num_states];
	get_initial_config(x_0, u_0);

	double x_f[num_states], u_f[num_states];
	get_final_config(x_f, u_f);

	const int num_guess = 2;
	double q_guess[num_q][num_guess];
	double xx[num_guess] =
	{ 0.0, 1.0 };

	for (int i = 0; i < num_q; i++)
	{
		q_guess[i][0] = x_0[i];
		q_guess[i][1] = x_f[i];
	}

	for (int i = 0; i < num_q; i++)
	{
		double interp[N];
		interpolateFromPoints(xx, q_guess[i], interp, num_guess, N);

		for (int k = 0; k < N; k++)
		{
			// Position
			x_write[k][i] = interp[k];
			// Velocity
			if (i < 3)
			{
				x_write[k][num_q + i] = 0.0;
			}
			if (i > 3)
			{
				double deriv;
				if (k == 0)
				{
					deriv = (interp[k + 1] - interp[k]) / h;
				}
				else if (k == N - 1)
				{
					deriv = (interp[k] - interp[k - 1]) / h;
				}
				else
				{
					deriv = (interp[k + 1] - interp[k - 1]) / (2.0 * h);
				}
				x_write[k][i - 1 + num_q] = deriv;
			}
		}
	}

	// Re-normalise quaternions
	for (int k = 0; k < N; k++)
	{
		mju_normalize4(&x_write[k][0]);
	}


	// Get quaternion derivative
	for (int k = 0; k < N; k++)
	{
		double dt;
		double* qa;
		double* qb;
		if (k == 0)
		{
			dt = h;
			qa = &x_write[k + 1][0];
			qb = &x_write[k][0];
		}
		else if (k == N - 1)
		{
			dt = h;
			qa = &x_write[k][0];
			qb = &x_write[k - 1][0];
		}
		else
		{
			dt = 2 * h;
			qa = &x_write[k + 1][0];
			qb = &x_write[k - 1][0];
		}

		mju_subQuat(&x_write[k][num_q], qa, qb);

		for (int i = 0; i < 3; i++)
		{
			// Scale by dt
			x_write[k][num_q + i] *= 1.0 / dt;
		}
	}


	// Set input
	for (int k = 0; k < N; k++)
	{
		for (int i = 0; i < num_inputs; i++)
		{
			u_write[k][i] = 0.0; // Dynamically incorrect, but should fix itself
		}
	}
}

/**
 * Run at the end of the optimisation
 */
void MJPendulum3DNLP::finalize_solution(SolverReturn status, Index n,
		const Number* z, const Number* bm_L, const Number* mb_U, Index m,
		const Number* g, const Number* lambda, Number obj_value,
		const IpoptData* ip_data, IpoptCalculatedQuantities* ip_cq)
{
	cout << endl;
	cout << "num_q = " << num_q << ";" << endl;
	cout << "num_dq = " << num_dq << ";" << endl;
	cout << "N = " << N << ";" << endl;
	cout << "h = " << h << ";" << endl;

	cout << endl << "z = [";
	for (int i = 0; i < n; i++)
	{
		cout << z[i] << ",";
	}
	cout << "];" << endl << endl;

	for (int i = 0; i < m; i++)
	{
		if (abs(g[i] > 1e-6))
		{
			cout << "g[" << i << "] = " << g[i] << ", ";
		}
	}
	cout << endl;

	set_variable_pointers(z); // Update pointers

	viewSolution();
	//testSolution();
}

/*
 * Use MuJoCoViewer to play the solution
 */
void MJPendulum3DNLP::viewSolution()
{
	mjModel* m = model.getModelPtr();
	mjData* d = model.getDataPtr();

	MuJoCoViewer viewer(m);

	viewer.enableFigure();

	viewer.opt.flags[mjVIS_CONTACTPOINT] = 1;
	//viewer.opt.flags[mjVIS_CONTACTFORCE] = 1;
	viewer.opt.flags[mjVIS_TRANSPARENT] = 1;
	//viewer.opt.label = mjLABEL_CONTACTFORCE;

	// Move camera
	viewer.cam.distance = 5.0;

	bool done = false;

	const double num_interp = 100.0;

	while (!done)
	{
		double tmupdate = glfwGetTime();

		mj_resetData(m, d); // Reset simulation

		// Loop over collocation points
		int k_max = 1.3 * N; // Run a little longer to pause at the end

		for (int k = 0; k < k_max; k++)
		{
			// Loop over interpolated data in real time
			for (double interp = 0.0; interp < 1.0; interp += 1.0 / num_interp)
			{
				double tmstart = glfwGetTime();

				if (k < N - 1)
				{
					for (int j = 0; j < num_q; j++) // Only take positions from state
					{
						d->qpos[j] = x[k][j] + (x[k + 1][j] - x[k][j]) * interp;
						d->qvel[j] = x[k][j + num_q]
								+ (x[k + 1][j + num_q] - x[k][j + num_q])
										* interp;
					}
					for (int j = 0; j < num_inputs; j++)
					{
						d->ctrl[j] = u[k][j] + (u[k + 1][j] - u[k][j]) * interp;
					}

					//Re-normalise quaternion
					mju_normalize4(&d->qpos[0]);
				}

				mj_forward(m, d); // Necessary for proper render

				if (glfwGetTime() - tmupdate > 1.0 / 60.0) // Update when last frame is 1/60 seconds old
				{
					viewer.updateScene(d);

					// Add arrows

					/*for (int j = 0; j < num_inputs; j++)
					 {
					 double base[3];
					 mju_n2d(base, &(d->xpos[(j + 2) * 3]), 3);
					 double vector[3] =
					 { 0.0, d->ctrl[j] * 0.1, 0.0 };

					 viewer.drawArrow(vector, base);
					 }*/

					float force = 0.0f;

					if (d->ncon > 0)
					{
						mjtNum contact[6];
						mj_contactForce(m, d, 0, contact);
						force = (float) mju_norm(contact, 3);
					}

					viewer.updateFigure(force);

					viewer.update(); // Render

					tmupdate = glfwGetTime();
				}

				if (viewer.windowShouldClose())
				{
					done = true;
					return; // Stop
				}

				double wait_time = 1.0 * h / num_interp;
				while (glfwGetTime() - tmstart < wait_time)
				{
					usleep(100); // Sleep to prevent a busy wait
				}
			}
		}
	}
}

/**
 * Evaluate extra constraints
 */
void MJPendulum3DNLP::eval_g_extra(const double** x, const double** u,
		double* g_extra)
{
	int i = 0;

	assert(i == m_extra);
}

/**
 * Get shape of extra constraints jacobian
 */
void MJPendulum3DNLP::eval_jac_g_extra_shape(int* iRow, int* jCol)
{
	int i = 0;
	int row = 1;

	assert(i == nnz_jac_g_extra);
}

/**
 * Get values of extra constraints jacobian
 */
void MJPendulum3DNLP::eval_jac_g_extra_values(const double** x,
		const double** u, double* values)
{

	int i = 0;

	assert(i == nnz_jac_g_extra);
}

/**
 * Get bounds for extra constraints
 */
void MJPendulum3DNLP::get_bounds_info_extra(double* g_l_extra,
		double* g_u_extra)
{
	int i = 0;

	assert(i == m_extra);
}

/**
 * Evaluate additional dynamics bounds
 */
void MJPendulum3DNLP::eval_dynamic_bounds(const double* x, const double* u,
		double* g_extra)
{
	int i = 0;

	// Quaternion sum
#if CONST_3D_QUATERNION
	double norm = 0;
	for (int i = 0; i < 4; i++)
	{
		norm += x[i] * x[i];
	}
	g_extra[i++] = norm - 1.0; // Should be zero
#endif

	assert(i == num_dynamic_bounds);
}

/**
 * Get gradient of additional dynamic bounds
 */
void MJPendulum3DNLP::eval_dynamic_bounds_gradient(const double* x,
		const double* u, double* g_extra_diff_x, double* g_extra_diff_u)
{
	int row = 0;

	// Quaternion sum
#if CONST_3D_QUATERNION
	for (int i = 0; i < num_states; i++)
	{
		if (i < 4)
		{
			g_extra_diff_x[row + i] = 2.0 * x[i];
		}
		else
		{
			g_extra_diff_x[row + i] = 0.0;
		}
	}
	for (int i = 0; i < num_inputs; i++)
	{
		g_extra_diff_u[row + i] = 0.0;
	}
	row++;
#endif

	assert(row == num_dynamic_bounds);
}
