#include <iostream>
#include <memory>
#include <unistd.h>
#include <math.h>

#include <coin/IpIpoptApplication.hpp>
#include <coin/IpSolveStatistics.hpp>

#include "MuJoCoModel/MuJoCoModel.hpp"
#include "MJQuadrupedNLP/MJQuadrupedNLP.hpp"

using namespace std;
using namespace Ipopt;

int main()
{
	// Create problem instance
	SmartPtr<MJQuadrupedNLP> nlp = new MJQuadrupedNLP();

	cout << "Problem created" << endl << endl;

	// Create Ipopt factory and verify
	SmartPtr<IpoptApplication> app = IpoptApplicationFactory();

	// Initialise the IpoptApplication and process the options
	ApplicationReturnStatus status = app->Initialize();
	if (status != Solve_Succeeded)
	{
		cout << endl << endl << "*** Error during Ipopt initialisation!"
				<< endl;
		return (int) status;
	}

	//app->RethrowNonIpoptException(true);

	app->Options()->SetIntegerValue("max_iter", 200);
	app->Options()->SetStringValue("hessian_approximation", "limited-memory"); // Use numeric hessian!
	app->Options()->SetIntegerValue("print_frequency_iter", 5); // Use numeric hessian!


	//app->Options()->SetStringValue("derivative_test", "first-order"); // Perform derivative check
	//app->Options()->SetNumericValue("derivative_test_perturbation", 1e-6);
	//app->Options()->SetNumericValue("point_perturbation_radius", 0.0); // Make sure the exact starting point is used
	//app->Options()->SetStringValue("derivative_test_print_all", "yes");


	cout << "Starting optimisation...";

	status = app->OptimizeTNLP(nlp);

	cout << "Finished optimisation" << endl;

	if (status == Solve_Succeeded || status == Solved_To_Acceptable_Level)
	{
		cout << "*** Iterations needed: " << app->Statistics()->IterationCount()
				<< endl;
		cout << "*** Wall-time taken: "
				<< app->Statistics()->TotalWallclockTime() << " s" << endl;
	}

	return (int) status;
}

