#ifndef MJFiveLinkBiped3DHingedNLP_
#define MJFiveLinkBiped3DHingedNLP_

#include "../CollocationNLP.hpp"
#include "../MuJoCoModel/MuJoCoModel.hpp"
#include "../MuJoCoViewer/MuJoCoViewer.hpp"

// Control constraints
#define CONST_3D_SYMMETRY 0
#define CONST_3D_STEP 0
#define CONST_3D_FOOTHOLD 0
#define CONST_3D_FOOTLIFT 0

using namespace Ipopt;

class MJFiveLinkBiped3DHingedNLP: public CollocationNLP
{
public:
	MJFiveLinkBiped3DHingedNLP();
	virtual ~MJFiveLinkBiped3DHingedNLP();

	/** Get forward dynamics */
	virtual void eval_dynamics(const double* x, const double* u, double* dx);

	/** Get (\partial dx / \partial x) */
	virtual void eval_dynamics_diff_x(const double* x, const double* u,
			double* dx_diff_x);

	/** Get (\partial dx / \partial u) */
	virtual void eval_dynamics_diff_u(const double* x, const double* u,
			double* dx_diff_u);

	/** Get initial configuration */
	virtual void get_initial_config(double* x_0, double* u_0);

	/** Get final configuration */
	virtual void get_final_config(double* x_f, double* u_f);

	/** Get variable bounds */
	virtual void get_variable_bound(double* x_u, double* x_l, double* u_u,
			double* u_l);

	/** Get initial guess */
	virtual void get_initial_guess(double** x, double** u);

	/** This method is called when the algorithm is complete so the TNLP can store/write the solution */
	virtual void finalize_solution(SolverReturn status, Index n,
			const Number* z, const Number* bm_L, const Number* bm_U, Index m,
			const Number* g, const Number* lambda, Number obj_value,
			const IpoptData* ip_data, IpoptCalculatedQuantities* ip_cq);

	/** Evaluate extra constraints */
	virtual void eval_g_extra(const double** x, const double** u,
			double* g_extra);

	/** Get shape of extra constraints jacobian */
	virtual void eval_jac_g_extra_shape(int* iRow, int* jCol);

	/** Get values of extra constraints jacobian */
	virtual void eval_jac_g_extra_values(const double** x, const double** u,
			double* values);

	/** Get bounds for extra constraints */
	virtual void get_bounds_info_extra(double* g_l_extra, double* g_u_extra);

	/** Get additional constraint function */
	virtual void eval_dynamic_bounds(const double* x, const double* u,
			double* g_extra);

	/** Get gradient of additional constraint function */
	virtual void eval_dynamic_bounds_gradient(const double* x, const double* u,
			double* g_extra_diff_x, double* g_extra_diff_u);

private:
	MJFiveLinkBiped3DHingedNLP(const MJFiveLinkBiped3DHingedNLP&);
	MJFiveLinkBiped3DHingedNLP& operator=(const MJFiveLinkBiped3DHingedNLP&);

	void viewSolution();
	void testSolution();

	void makeArrow(mjvGeom* arrow, const double vector[3],
			const double base[3]);

	/** Convert qvel to qpos derivative  */
	void qvel_to_qpos_dot(const double* qpos, const double* qvel, double* qpos_dot);

	/** Convert relative joint angles to absolute angles */
	void joint2abs(const double* q_joint, double* q_abs, bool state = false);
	void abs2joint(const double* q_abs, double* q_joint, bool state = false);

	/** Number of positions */
	int num_q;
	/** Number of velocities */
	int num_dq;

	/** Body lengths */
	double L1, L2, L3, L4, L5;

	/** Single step length */
	double L_step;

	/** Half-point in window */
	int k_half;

	/** Initial configuration (absolute angles) */
	double* q_0_abs;

	/** Use MoJoCo as a back-end */
	MuJoCoModel model;

	/** Use viewer to show results */
	//MuJoCoViewer viewer;
};

#endif
