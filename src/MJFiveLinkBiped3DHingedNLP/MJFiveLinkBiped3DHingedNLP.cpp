#include "MJFiveLinkBiped3DHingedNLP.hpp"

#include <iostream>
#include <cmath>
#include <unistd.h>
#include <assert.h>

using namespace Ipopt;
using namespace std;

/**
 * Constructor
 *
 * Use initialiser list to call non-default constructor of the member class
 *
 * x = [ q, theta, dq, dtheta ]
 *
 * ...where q is the horizontal position
 */
MJFiveLinkBiped3DHingedNLP::MJFiveLinkBiped3DHingedNLP() :
		CollocationNLP(15), model(
				"/home/robert/Documents/Master Assignment/MuJoCo/Models/five_link_biped_3d_hinged.xml")
{
	num_q = 8;
	num_dq = 8;

	num_states = num_q + num_dq;
	num_inputs = 6;

	size = num_states + num_inputs;

	double t_0 = 0.0;
	double t_f = 0.8;

	h = (t_f - t_0) / (N - 1);

	m_extra = 0;
	nnz_jac_g_extra = 0;
	/**
	 * Constraint: Symmetry
	 * Compare all first and last states (ignore inputs)
	 * Comparison is in absolute angles, so the gradient becomes pretty awful
	 */
#if CONST_3D_SYMMETRY
	m_extra += num_states;
	nnz_jac_g_extra += 52;
#endif

	/**
	 * Constraint: Step
	 * Make sure hips move forward
	 */
#if CONST_3D_STEP
	m_extra += 1;
	nnz_jac_g_extra += 4;
#endif

	/**
	 * Constraint: foothold
	 * Make sure the swing foot starts on the ground
	 */
#if CONST_3D_FOOTHOLD
	m_extra += 1;
	nnz_jac_g_extra += 4;
#endif

	/**
	 * Constraint: foot lift
	 * Make sure the swing foot is of the ground in mid-step
	 */
#if CONST_3D_FOOTLIFT
	m_extra += 1;
	nnz_jac_g_extra += 4;
#endif

	/**
	 * Configs are not used as constraints
	 * They are still used for the initial guess
	 */
	custom_initial_guess = true;
	constraint_config_initial = true;
	constraint_config_final = true;

	L1 = L5 = 0.4; // Include radius for distance to the ground
	L2 = L4 = 0.4;
	L3 = 0.625;

	L_step = 0.4;

	k_half = (int) (0.5 * N);

	q_0_abs = new double[num_q];

	for (int i = 0; i < num_q; i++)
	{
		q_0_abs[i] = 0.0;
	}
}

/**
 * Destructor
 */
MJFiveLinkBiped3DHingedNLP::~MJFiveLinkBiped3DHingedNLP()
{
	delete[] q_0_abs;
}

/**
 * Get initial configuration
 *
 * Set value to 1.0e19 to make it unbounded
 */
void MJFiveLinkBiped3DHingedNLP::get_initial_config(double* x_0, double* u_0)
{
	/*
	 srand(1234); // Fix seed

	 for (int i = 0; i < num_states; i++)
	 {
	 double r = 1.0 - 2.0 * (double) rand() / RAND_MAX;
	 x_0[i] = r * 0.2 * M_PI;
	 }
	 */

	// Turn absolute angles into relative joint angles
	//abs2joint(q_0_abs, x_0);
	// Position
	/*int i = 0;
	 for (; i < num_q; i++)
	 {
	 //x_0[i] = 1.0e19;
	 x_0[i] = 0.0;
	 }*/
	// Manually tuned config:
	x_0[0] = -0.125664;
	x_0[1] = 0.031416;
	x_0[2] = -0.251328;
	x_0[3] = 0.502656;
	x_0[4] = -0.031416;
	x_0[5] = 0.062832;
	x_0[6] = -0.031416;
	x_0[7] = 0.251328 + 0.1;

	//x_0[5] = 0.5 * M_PI;

	// Velocity
	for (int i = num_q; i < num_states; i++)
	{
		x_0[i] = 1.0e19;
		//x_0[i] = 0.0;
	}

	// Input
	for (int j = 0; j < num_inputs; j++)
	{
		u_0[j] = 1.0e19;
		//u_0[i] = 0.0;
	}
}

/**
 * Get final configuration
 *
 * Set value to 1.0e19 to make it unbounded
 */
void MJFiveLinkBiped3DHingedNLP::get_final_config(double* x_f, double* u_f)
{
	// Position
	/*
	 for (int i = 0; i < num_q; i++)
	 {
	 x_f[i] = 0.0;
	 }
	 x_f[7] = 0.5 * M_PI;
	 */

	// Turn absolute angles into relative joint angles (and flip order)
	/*x_f[0] = q_0_abs[4];
	 x_f[1] = q_0_abs[3] - q_0_abs[4];
	 x_f[2] = q_0_abs[2] - q_0_abs[3];
	 x_f[3] = q_0_abs[1] - q_0_abs[3];
	 x_f[4] = q_0_abs[0] - q_0_abs[1];*/

	// Manually tuned config
	x_f[0] = 0.31416;
	x_f[1] = 0.062832;
	x_f[2] = -0.15708;
	x_f[3] = 0.062832;
	x_f[4] = -0.062832;
	x_f[5] = -0.565488;
	x_f[6] = -0.062832;
	x_f[7] = 0.251328;

	// Velocity
	for (int i = num_q; i < num_states; i++)
	{
		//x_f[i] = 1.0e19;
		x_f[i] = 0.0;
	}

	// Input
	for (int j = 0; j < num_inputs; j++)
	{
		u_f[j] = 1.0e19;
		//u_f[j] = 0.0;
	}
}

/**
 * Get variable bounds
 *
 * Set values to 1.0e19 and -1.0e19 to make it unbounded
 */
void MJFiveLinkBiped3DHingedNLP::get_variable_bound(double* x_u, double* x_l,
		double* u_u, double* u_l)
{
	// States
	for (int i = 0; i < num_states; i++)
	{
		x_l[i] = -1.0e19;
		x_u[i] = 1.0e19;
	}

	// Floor angles
	x_l[0] = -0.5 * M_PI;
	x_u[0] = 0.5 * M_PI;
	x_l[1] = -0.5 * M_PI;
	x_u[1] = 0.5 * M_PI;

	// Knees
	x_l[2] = -0.3 * M_PI;
	x_u[2] = -0.01; // Never overstretch
	x_l[7] = 0.01; // Never overstretch
	x_u[7] = 0.3 * M_PI;

	// Hip angles
	double lim_y = 0.2 * M_PI;
	double lim_x = 0.05 * M_PI;
	x_l[3] = -lim_y; // hinge-y
	x_u[3] = lim_y;
	x_l[4] = -lim_x; // hinge-x
	x_u[4] = lim_x;
	x_l[5] = -lim_y; // hinge-y
	x_u[5] = lim_y;
	x_l[6] = -lim_x; // hinge-x
	x_u[6] = lim_x;

	// Angular velocities
	for (int i = num_q; i < num_states; i++)
	{
		x_l[i] = -5.0;
		x_u[i] = 5.0;
	}

	// Inputs
	for (int i = 0; i < num_inputs; i++)
	{
		u_l[i] = -300;
		u_u[i] = 300;
	}

	return;
}

/**
 * Calculate dynamics
 *
 * @return void
 */
void MJFiveLinkBiped3DHingedNLP::eval_dynamics(const double* x, const double* u,
		double* dx)
{
	const double* qpos = &x[0];
	const double* qvel = &x[num_q];
	double qacc[num_dq];
	model.get_dynamics(qpos, qvel, u, qacc);

	// Convert qvel to qpos_dot
	qvel_to_qpos_dot(qpos, qvel, &dx[0]);

	// Set acceleration
	for (int i = 0; i < num_dq; i++)
	{
		dx[i + num_q] = qacc[i];
	}
}

/**
 * Get dynamics derivative w.r.t. x
 *
 * Return vector is stacked like:
 *
 * dx/x = [
 *    dx1/x1
 *    dx2/x1
 *    ...
 * ]
 *
 * @return void
 */
void MJFiveLinkBiped3DHingedNLP::eval_dynamics_diff_x(const double* x,
		const double* u, double* dx_diff_x)
{
	const double* qpos = &x[0];
	const double* qvel = &x[num_dq];
	double qacc_diff_qvel[num_dq * num_dq], qacc_diff_qpos[num_dq * num_q];

	model.get_dynamics_diff_qvel(qpos, qvel, u, qacc_diff_qvel);
	model.get_dynamics_diff_qpos(qpos, qvel, u, qacc_diff_qpos);

	// diff q_pos_dot / q_pos
	for (int col = 0; col < num_q; col++)
	{
		for (int row = 0; row < num_q; row++)
		{
			dx_diff_x[row + col * num_states] = 0.0;
		}
	}
	// diff q_acc / q_pos
	int index = 0;
	for (int col = 0; col < num_q; col++)
	{
		for (int row = 0; row < num_dq; row++)
		{
			dx_diff_x[num_q + row + col * num_states] = qacc_diff_qpos[index++];
		}
	}
	// diff q_pos_dot / q_vel
	for (int col = 0; col < num_dq; col++)
	{
		for (int row = 0; row < num_q; row++)
		{
			double eye = (row == col) ? 1.0 : 0.0;
			dx_diff_x[row + (num_q + col) * num_states] = eye;

		}
	}
	// diff q_acc / q_vel
	index = 0;
	for (int col = 0; col < num_q; col++)
	{
		for (int row = 0; row < num_dq; row++)
		{
			dx_diff_x[num_q + row + (num_q + col) * num_states] =
					qacc_diff_qvel[index++];
		}
	}
}

/**
 * Get dynamics derivative w.r.t. u
 *
 * @return void
 */
void MJFiveLinkBiped3DHingedNLP::eval_dynamics_diff_u(const double* x,
		const double* u, double* dx_diff_u)
{
	const double* qpos = &x[0];
	const double* qvel = &x[num_q];
	double qacc_diff_u[num_dq * num_inputs];

	model.get_dynamics_diff_u(qpos, qvel, u, qacc_diff_u);

	for (int col = 0; col < num_inputs; col++)
	{
		for (int row = 0; row < num_q; row++)
		{
			dx_diff_u[row + col * num_states] = 0.0;
		}
	}
	int index = 0;
	for (int col = 0; col < num_inputs; col++)
	{
		for (int row = 0; row < num_dq; row++)
		{
			dx_diff_u[num_q + row + col * num_states] = qacc_diff_u[index];
			index++;
		}
	}
}

/**
 * Convert qvel into qpos derivative
 *
 * This conversion takes quaternions into account
 */
void MJFiveLinkBiped3DHingedNLP::qvel_to_qpos_dot(const double* qpos,
		const double* qvel, double* qpos_dot)
{
	// Simply copy
	for (int i = 0; i < num_q; i++)
	{
		qpos_dot[i] = qvel[i];
	}
}

/**
 * Get initial guess (starting point) for NLP
 */
void MJFiveLinkBiped3DHingedNLP::get_initial_guess(double** x, double** u)
{
// Set user defined initial and final configuration
	double x_0[num_states], u_0[num_states];
	get_initial_config(x_0, u_0);

	double x_f[num_states], u_f[num_states];
	get_final_config(x_f, u_f);

	const int num_guess = 4;
	double q_guess[num_q][num_guess];
	double xx[num_guess] =
	{ 0.0, 0.1, 0.6, 1.0 };

	for (int i = 0; i < num_q; i++)
	{
		int frame = 0;
		q_guess[i][frame++] = x_0[i];
		q_guess[i][frame++] = x_0[i];
		q_guess[i][frame++] = x_0[i] * 0.5 + x_f[i] * 0.5;
		q_guess[i][frame++] = x_f[i];
	}

	// Initial frame
	int frame = 0;
	frame++;

	// Push off a little bit
	q_guess[7][frame] -= 0.25 * M_PI;
	q_guess[0][frame] += 0.01 * M_PI;
	frame++;

	// Lift leg a little bit
	q_guess[6][frame] -= 0.25 * M_PI;
	q_guess[7][frame] += 0.25 * M_PI;
	frame++;

	// Final frame
	frame++;

	for (int i = 0; i < num_q; i++)
	{
		double interp[N];
		interpolateFromPoints(xx, q_guess[i], interp, num_guess, N);

		for (int k = 0; k < N; k++)
		{
			// Position
			x_write[k][i] = interp[k];
			// Velocity
			double deriv;
			if (k == 0)
			{
				deriv = (interp[k + 1] - interp[k]) / h;
			}
			else if (k == N - 1)
			{
				deriv = (interp[k] - interp[k - 1]) / h;
			}
			else
			{
				deriv = (interp[k + 1] - interp[k - 1]) / (2.0 * h);
			}
			x_write[k][i + num_q] = deriv;
		}
	}

	// Set input
	for (int k = 0; k < N; k++)
	{
		for (int i = 0; i < num_inputs; i++)
		{
			u_write[k][i] = 0.0; // Dynamically incorrect, but should fix itself
		}
	}
}

/**
 * Run at the end of the optimisation
 */
void MJFiveLinkBiped3DHingedNLP::finalize_solution(SolverReturn status, Index n,
		const Number* z, const Number* bm_L, const Number* mb_U, Index m,
		const Number* g, const Number* lambda, Number obj_value,
		const IpoptData* ip_data, IpoptCalculatedQuantities* ip_cq)
{
	cout << endl;
	cout << "N = " << N << ";" << endl;
	cout << "h = " << h << ";" << endl;
	cout << "num_q = " << num_q << ";" << endl;
	cout << "num_dq = " << num_dq << ";" << endl;

	cout << endl << "z = [";
	for (int i = 0; i < n; i++)
	{
		cout << z[i] << ",";
	}
	cout << "];" << endl << endl;

	for (int i = 0; i < m; i++)
	{
		if (abs(g[i] > 1e-6))
		{
			cout << "g[" << i << "] = " << g[i] << ", ";
		}
	}
	cout << endl;

	set_variable_pointers(z); // Update pointers

	viewSolution();
	//testSolution();
}

/**
 * Convert relative joint angles to absolute anlges
 */
void MJFiveLinkBiped3DHingedNLP::joint2abs(const double* q_joint, double* q_abs,
		bool state)
{
	int loops = state ? (num_q + 1) : 1;
	for (int j = 0; j < loops; j += num_q)
	{
		q_abs[0 + j] = q_joint[0 + j];
		q_abs[1 + j] = q_abs[0 + j] + q_joint[1 + j];
		q_abs[2 + j] = q_abs[1 + j] + q_joint[2 + j];
		q_abs[3 + j] = q_abs[1 + j] + q_joint[3 + j];
		q_abs[4 + j] = q_abs[3 + j] + q_joint[4 + j];
	}
}

/**
 * Convert absolute angles to joint angles
 */
void MJFiveLinkBiped3DHingedNLP::abs2joint(const double* q_abs, double* q_joint,
		bool state)
{
	int loops = state ? (num_q + 1) : 1;
	for (int j = 0; j < loops; j += num_q)
	{
		q_joint[0 + j] = q_abs[0 + j];
		q_joint[1 + j] = q_abs[1 + j] - q_abs[0 + j];
		q_joint[2 + j] = q_abs[2 + j] - q_abs[1 + j];
		q_joint[3 + j] = q_abs[3 + j] - q_abs[1 + j];
		q_joint[4 + j] = q_abs[4 + j] - q_abs[3 + j];
	}
}

/*
 * Use MuJoCoViewer to play the solution
 */
void MJFiveLinkBiped3DHingedNLP::viewSolution()
{
	mjModel* m = model.getModelPtr();
	mjData* d = model.getDataPtr();

	MuJoCoViewer viewer(m);

	viewer.enableFigure();

	viewer.opt.flags[mjVIS_CONTACTPOINT] = 1;
	//viewer.opt.flags[mjVIS_CONTACTFORCE] = 1;
	//viewer.opt.flags[mjVIS_TRANSPARENT] = 1;
	//viewer.opt.label = mjLABEL_CONTACTFORCE;

	// Move camera
	viewer.cam.distance = 5.0;

	bool done = false;

	const double num_interp = 100.0;

	while (!done)
	{
		double tmupdate = glfwGetTime();

		mj_resetData(m, d); // Reset simulation

		// Loop over collocation points
		int k_max = 1.3 * N; // Run a little longer to pause at the end

		for (int k = 0; k < k_max; k++)
		{
			// Loop over interpolated data in real time
			for (double interp = 0.0; interp < 1.0; interp += 1.0 / num_interp)
			{
				double tmstart = glfwGetTime();

				if (k < N - 1)
				{
					for (int j = 0; j < num_q; j++) // Only take positions from state
					{
						d->qpos[j] = x[k][j] + (x[k + 1][j] - x[k][j]) * interp;
						d->qvel[j] = x[k][j + num_q]
								+ (x[k + 1][j + num_q] - x[k][j + num_q])
										* interp;
					}
					for (int j = 0; j < num_inputs; j++)
					{
						d->ctrl[j] = u[k][j] + (u[k + 1][j] - u[k][j]) * interp;
					}
				}

				mj_forward(m, d); // Necessary for proper render

				if (glfwGetTime() - tmupdate > 1.0 / 60.0) // Update when last frame is 1/60 seconds old
				{
					viewer.updateScene(d);

					// Add arrows

					/*for (int j = 0; j < num_inputs; j++)
					 {
					 double base[3];
					 mju_n2d(base, &(d->xpos[(j + 2) * 3]), 3);
					 double vector[3] =
					 { 0.0, d->ctrl[j] * 0.1, 0.0 };

					 viewer.drawArrow(vector, base);
					 }*/

					float force = 0.0f;

					if (d->ncon > 0)
					{
						mjtNum contact[6];
						mj_contactForce(m, d, 0, contact);
						force = (float) mju_norm(contact, 3);
					}

					viewer.updateFigure(force);

					viewer.update(); // Render

					tmupdate = glfwGetTime();
				}

				if (viewer.windowShouldClose())
				{
					done = true;
					return; // Stop
				}

				double wait_time = 1.0 * h / num_interp;
				while (glfwGetTime() - tmstart < wait_time)
				{
					usleep(100); // Sleep to prevent a busy wait
				}
			}
		}
	}
}

/**
 * Evaluate extra constraints
 */
void MJFiveLinkBiped3DHingedNLP::eval_g_extra(const double** x,
		const double** u, double* g_extra)
{
	int i = 0;

// Symmetry constraints
#if CONST_3D_SYMMETRY
	double x_0_abs[num_states], x_f_abs[num_states];
	joint2abs(x[0], x_0_abs, true);
	joint2abs(x[N - 1], x_f_abs, true);

	for (int j = 0; j <= num_q; j += num_q)
	{
		for (int m = 0; m < num_q; m++)
		{
			g_extra[i++] = x_0_abs[j + m] - x_f_abs[j + 4 - m];
			// e.g. x_1[0] = x_5[end]
		}
	}
#endif

// Step constraint
#if CONST_3D_STEP
	double x_body_1 = L1 * sin(x[0][0]) + L2 * sin(x[0][0] + x[0][1]);
	double x_body_N = L1 * sin(x[N - 1][0])
			+ L2 * sin(x[N - 1][0] + x[N - 1][1]);
	g_extra[i++] = x_body_N - x_body_1 - L_step; // Should be zero
#endif

// Foothold constraint
#if CONST_3D_FOOTHOLD
	double q_0[num_q];
	joint2abs(x[0], q_0);
	g_extra[i++] = L1 * cos(q_0[0]) + L2 * cos(q_0[1]) - L4 * cos(q_0[3])
			- L5 * cos(q_0[4]);
#endif

// Foot lift constraint
#if CONST_3D_FOOTLIFT
	double q_k[num_q];
	joint2abs(x[k_half], q_k);
	g_extra[i++] = L1 * cos(q_k[0]) + L2 * cos(q_k[1]) - L4 * cos(q_k[3])
			- L5 * cos(q_k[4]);

#endif

	assert(i == m_extra);
}

/**
 * Get shape of extra constraints jacobian
 */
void MJFiveLinkBiped3DHingedNLP::eval_jac_g_extra_shape(int* iRow, int* jCol)
{
	int i = 0;
	int row = 1;

// Symmetry constraints
#if CONST_3D_SYMMETRY
	for (int j = 0; j <= num_q; j += num_q) // For q and dq
	{
		iRow[i] = row;
		jCol[i++] = 1 + j;				// x_1[0]
		iRow[i] = row;
		jCol[i++] = num_states * (N - 1) + 1 + j; // x_1[end]
		iRow[i] = row;
		jCol[i++] = num_states * (N - 1) + 2 + j; // x_2[end]
		iRow[i] = row;
		jCol[i++] = num_states * (N - 1) + 4 + j; // x_4[end]
		iRow[i] = row;
		jCol[i++] = num_states * (N - 1) + 5 + j; // x_5[end]

		row++;

		iRow[i] = row;
		jCol[i++] = 1 + j;				// x_1[0]
		iRow[i] = row;
		jCol[i++] = 2 + j;				// x_2[0]
		iRow[i] = row;
		jCol[i++] = num_states * (N - 1) + 1 + j; // x_1[end]
		iRow[i] = row;
		jCol[i++] = num_states * (N - 1) + 2 + j; // x_2[end]
		iRow[i] = row;
		jCol[i++] = num_states * (N - 1) + 4 + j; // x_4[end]

		row++;

		iRow[i] = row;
		jCol[i++] = 1 + j;				// x_1[0]
		iRow[i] = row;
		jCol[i++] = 2 + j;				// x_2[0]
		iRow[i] = row;
		jCol[i++] = 3 + j;				// x_3[0]
		iRow[i] = row;
		jCol[i++] = num_states * (N - 1) + 1 + j; // x_1[end]
		iRow[i] = row;
		jCol[i++] = num_states * (N - 1) + 2 + j; // x_2[end]
		iRow[i] = row;
		jCol[i++] = num_states * (N - 1) + 3 + j; // x_3[end]

		row++;

		iRow[i] = row;
		jCol[i++] = 1 + j;				// x_1[0]
		iRow[i] = row;
		jCol[i++] = 2 + j;				// x_2[0]
		iRow[i] = row;
		jCol[i++] = 4 + j;				// x_4[0]
		iRow[i] = row;
		jCol[i++] = num_states * (N - 1) + 1 + j; // x_1[end]
		iRow[i] = row;
		jCol[i++] = num_states * (N - 1) + 2 + j; // x_2[end]

		row++;

		iRow[i] = row;
		jCol[i++] = 1 + j;				// x_1[0]
		iRow[i] = row;
		jCol[i++] = 2 + j;				// x_2[0]
		iRow[i] = row;
		jCol[i++] = 4 + j;				// x_4[0]
		iRow[i] = row;
		jCol[i++] = 5 + j;				// x_4[0]
		iRow[i] = row;
		jCol[i++] = num_states * (N - 1) + 1 + j; // x_1[end]

		row++;
	}
#endif

// Step constraint
#if CONST_3D_STEP
	iRow[i] = row;
	jCol[i++] = 1; // x_1[1];
	iRow[i] = row;
	jCol[i++] = 2; // x_2[1];
	iRow[i] = row;
	jCol[i++] = num_states * (N - 1) + 1; // x_1[N];
	iRow[i] = row;
	jCol[i++] = num_states * (N - 1) + 2; // x_2[N];
	row++;
#endif

// Foothold constraint
#if CONST_3D_FOOTHOLD
	iRow[i] = row;
	jCol[i++] = 1; // x_1[1];
	iRow[i] = row;
	jCol[i++] = 2; // x_2[1];
	iRow[i] = row;
	jCol[i++] = 4; // x_4[1];
	iRow[i] = row;
	jCol[i++] = 5; // x_5[1];
	row++;
#endif

// Foot lift constraint
#if CONST_3D_FOOTLIFT
	iRow[i] = row;
	jCol[i++] = num_states * k_half + 1; // x_1[1];
	iRow[i] = row;
	jCol[i++] = num_states * k_half + 2; // x_2[1];
	iRow[i] = row;
	jCol[i++] = num_states * k_half + 4; // x_4[1];
	iRow[i] = row;
	jCol[i++] = num_states * k_half + 5; // x_5[1];
	row++;
#endif

	assert(i == nnz_jac_g_extra);
}

/**
 * Get values of extra constraints jacobian
 */
void MJFiveLinkBiped3DHingedNLP::eval_jac_g_extra_values(const double** x,
		const double** u, double* values)
{

	int i = 0;

// Symmetry constraint
#if CONST_3D_SYMMETRY
	for (int j = 0; j < 2; j++) // For q and dq
	{
		values[i++] = 1;
		values[i++] = -1;
		values[i++] = -1;
		values[i++] = -1;
		values[i++] = -1;

		values[i++] = 1;
		values[i++] = 1;
		values[i++] = -1;
		values[i++] = -1;
		values[i++] = -1;

		values[i++] = 1;
		values[i++] = 1;
		values[i++] = 1;
		values[i++] = -1;
		values[i++] = -1;
		values[i++] = -1;

		values[i++] = 1;
		values[i++] = 1;
		values[i++] = 1;
		values[i++] = -1;
		values[i++] = -1;

		values[i++] = 1;
		values[i++] = 1;
		values[i++] = 1;
		values[i++] = 1;
		values[i++] = -1;
	}
#endif

// Step constraint
#if CONST_3D_STEP
	values[i++] = -L1 * cos(x[0][0]) - L2 * cos(x[0][0] + x[0][1]);
	values[i++] = -L2 * cos(x[0][0] + x[0][1]);
	values[i++] = L1 * cos(x[N - 1][0]) + L2 * cos(x[N - 1][0] + x[N - 1][1]);
	values[i++] = L2 * cos(x[N - 1][0] + x[N - 1][1]);
#endif

// Foothold constraint
#if CONST_3D_FOOTHOLD
	values[i++] = -L1 * sin(x[0][0]) - L2 * sin(x[0][0] + x[0][1])
			+ L4 * sin(x[0][0] + x[0][1] + x[0][3])
			+ L5 * sin(x[0][0] + x[0][1] + x[0][3] + x[0][4]);
	values[i++] = -L2 * sin(x[0][0] + x[0][1])
			+ L4 * sin(x[0][0] + x[0][1] + x[0][3])
			+ L5 * sin(x[0][0] + x[0][1] + x[0][3] + x[0][4]);
	values[i++] = L4 * sin(x[0][0] + x[0][1] + x[0][3])
			+ L5 * sin(x[0][0] + x[0][1] + x[0][3] + x[0][4]);
	values[i++] = L5 * sin(x[0][0] + x[0][1] + x[0][3] + x[0][4]);
#endif

// Foot lift constraint
#if CONST_3D_FOOTLIFT
	int k = k_half;
	values[i++] = -L1 * sin(x[k][0]) - L2 * sin(x[k][0] + x[k][1])
			+ L4 * sin(x[k][0] + x[k][1] + x[k][3])
			+ L5 * sin(x[k][0] + x[k][1] + x[k][3] + x[k][4]);
	values[i++] = -L2 * sin(x[k][0] + x[k][1])
			+ L4 * sin(x[k][0] + x[k][1] + x[k][3])
			+ L5 * sin(x[k][0] + x[k][1] + x[k][3] + x[k][4]);
	values[i++] = L4 * sin(x[k][0] + x[k][1] + x[k][3])
			+ L5 * sin(x[k][0] + x[k][1] + x[k][3] + x[k][4]);
	values[i++] = L5 * sin(x[k][0] + x[k][1] + x[k][3] + x[k][4]);
#endif

	assert(i == nnz_jac_g_extra);
}

/**
 * Get bounds for extra constraints
 */
void MJFiveLinkBiped3DHingedNLP::get_bounds_info_extra(double* g_l_extra,
		double* g_u_extra)
{
	int i = 0;

// Symmetry
#if CONST_3D_SYMMETRY
	for (int j = 0; j < num_states; j++)
	{
		g_l_extra[i] = 0.0;
		g_u_extra[i++] = 0.0;
	}
#endif

// Step
#if CONST_3D_STEP
	g_l_extra[i] = 0.0;
	g_u_extra[i++] = 0.0;
#endif

// Foothold
#if CONST_3D_FOOTHOLD
	g_l_extra[i] = -0.05;
	g_u_extra[i++] = 0.00;
#endif

// Foot lift
#if CONST_3D_FOOTLIFT
	g_l_extra[i] = 0.15 * L1;
//g_l_extra[i] = 0.0 * L1;
	g_u_extra[i++] = L1;
#endif

	assert(i == m_extra);
}

/**
 * Evaluate additional dynamics bounds
 */
void MJFiveLinkBiped3DHingedNLP::eval_dynamic_bounds(const double* x,
		const double* u, double* g_extra)
{
	int i = 0;

	assert(i == num_dynamic_bounds);
}

/**
 * Get gradient of additional dynamic bounds
 */
void MJFiveLinkBiped3DHingedNLP::eval_dynamic_bounds_gradient(const double* x,
		const double* u, double* g_extra_diff_x, double* g_extra_diff_u)
{
	int row = 0;

	assert(row == num_dynamic_bounds);
}
