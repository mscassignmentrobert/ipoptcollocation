#include "MJCartPendulumNLP.hpp"

#include <iostream>
#include <cmath>
#include <unistd.h>

using namespace Ipopt;
using namespace std;

/**
 * Constructor
 *
 * Use initialiser list to call non-default constructor of the member class
 *
 * x = [ q, theta, dq, dtheta ]
 *
 * ...where q is the horizontal position
 */
MJCartPendulumNLP::MJCartPendulumNLP() :
		CollocationNLP(15), model(
				"/home/robert/Documents/Master Assignment/MuJoCo/Models/cartpole.xml")
{
	num_q = 2;
	num_dq = 2;

	num_states = num_q + num_dq;
	num_inputs = 1;

	double t_0 = 0.0;
	double t_f = 1.5;

	h = (t_f - t_0) / (N - 1);

	custom_initial_guess = true;
}

/**
 * Destructor
 */
MJCartPendulumNLP::~MJCartPendulumNLP()
{

}

/**
 * Get initial configuration
 *
 * Set value to 1.0e19 to make it unbounded
 */
void MJCartPendulumNLP::get_initial_config(double* x_0, double* u_0)
{
	// Position
	x_0[0] = 0.0;
	x_0[1] = 0.000001;
	//x_0[1] = 0.25 * M_PI;
	// Velocity
	x_0[2] = 0.0;
	x_0[3] = 0.0;

	u_0[0] = 1.0e19;
}

/**
 * Get final configuration
 *
 * Set value to 1.0e19 to make it unbounded
 */
void MJCartPendulumNLP::get_final_config(double* x_f, double* u_f)
{
	// Positions
	x_f[0] = 1.0e19; // Unconstraint
	x_f[1] = M_PI;
	// Velocities
	x_f[2] = 0.0;
	x_f[3] = 0.0;

	//u_f[0] = 1.0e19;
	u_f[0] = 0.0; // Stable result?
}

/**
 * Get variable bounds
 *
 * Set values to 1.0e19 and -1.0e19 to make it unbounded
 */
void MJCartPendulumNLP::get_variable_bound(double* x_u, double* x_l,
		double* u_u, double* u_l)
{
	x_l[0] = -1.0;
	x_u[0] = 1.0;

	x_l[1] = -1.0e19;
	x_u[1] = 1.0e19;

	x_l[2] = -1.0e19;
	x_u[2] = 1.0e19;

	x_l[3] = -1.0e19;
	x_u[3] = 1.0e19;

	u_l[0] = -1.0e19;
	u_u[0] = 1.0e19;
}

/**
 * Calculate dynamics
 *
 * @return void
 */
void MJCartPendulumNLP::eval_dynamics(const double* x, const double* u,
		double* dx)
{

	const double* qpos = &x[0];
	const double* qvel = &x[num_q];
	double qacc[num_dq];
	model.get_dynamics(qpos, qvel, u, qacc);

	// Set velocity
	for (int i = 0; i < num_q; i++)
	{
		dx[i] = qvel[i];
	}

	// Set acceleration
	for (int i = 0; i < num_dq; i++)
	{
		dx[i + num_q] = qacc[i];
	}
}

/**
 * Get dynamics derivative w.r.t. x
 *
 * Return vector is stacked like:
 *
 * dx/x = [
 *    dx1/x
 *    dx2/x
 *    ...
 * ]
 *
 * @return void
 */
void MJCartPendulumNLP::eval_dynamics_diff_x(const double* x, const double* u,
		double* dx_diff_x)
{
	const double* qpos = &x[0];
	const double* qvel = &x[num_dq];
	double qacc_diff_qvel[num_dq * num_dq], qacc_diff_qpos[num_dq * num_q];

	model.get_dynamics_diff_qvel(qpos, qvel, u, qacc_diff_qvel);
	model.get_dynamics_diff_qpos(qpos, qvel, u, qacc_diff_qpos);

	//dx_diff_x[0] = 0.0;
	//dx_diff_x[1] = 0.0;
	//dx_diff_x[2] = qacc_diff_qpos[0];
	//dx_diff_x[3] = qacc_diff_qpos[1];

	//dx_diff_x[4] = 0.0;
	//dx_diff_x[5] = 0.0;
	//dx_diff_x[6] = qacc_diff_qpos[2];
	//dx_diff_x[7] = qacc_diff_qpos[3];

	//dx_diff_x[8] = 1.0;
	//dx_diff_x[9] = 0.0;
	//dx_diff_x[10] = qacc_diff_qvel[0];
	//dx_diff_x[11] = qacc_diff_qvel[1];

	//dx_diff_x[12] = 0.0;
	//dx_diff_x[13] = 1.0;
	//dx_diff_x[14] = qacc_diff_qvel[2];
	//dx_diff_x[15] = qacc_diff_qvel[3];

	// diff q_pos_dot / q_pos
	for (int col = 0; col < num_q; col++)
	{
		for (int row = 0; row < num_q; row++)
		{
			dx_diff_x[row + col * num_states] = 0.0;
		}
	}
	// diff q_pos_dot / qvel
	for (int col = 0; col < num_dq; col++)
	{
		for (int row = 0; row < num_q; row++)
		{
			double eye = (row == col) ? 1.0 : 0.0;
			dx_diff_x[row + (num_q + col) * num_states] = eye;
		}
	}
	// diff q_acc / q_pos
	int index = 0;
	for (int col = 0; col < num_q; col++)
	{
		for (int row = 0; row < num_dq; row++)
		{
			dx_diff_x[num_q + row + col * num_states] = qacc_diff_qpos[index++];
		}
	}
	// Diff q_acc / q_vel
	index = 0;
	for (int col = 0; col < num_q; col++)
	{
		for (int row = 0; row < num_dq; row++)
		{
			dx_diff_x[num_q + row + (num_q + col) * num_states] =
					qacc_diff_qvel[index++];
		}
	}

}

/**
 * Get dynamics derivative w.r.t. u
 *
 * @return void
 */
void MJCartPendulumNLP::eval_dynamics_diff_u(const double* x, const double* u,
		double* dx_diff_u)
{

	const double* qpos = &x[0];
	const double* qvel = &x[num_q];
	double qacc_diff_u[num_dq * num_inputs];

	model.get_dynamics_diff_u(qpos, qvel, u, qacc_diff_u);

	for (int col = 0; col < num_inputs; col++)
	{
		for (int row = 0; row < num_q; row++)
		{
			dx_diff_u[row + col * num_states] = 0.0;
		}
	}
	int index = 0;
	for (int col = 0; col < num_inputs; col++)
	{
		for (int row = 0; row < num_dq; row++)
		{
			dx_diff_u[num_q + row + col * num_states] = qacc_diff_u[index];
			index++;
		}
	}
}

/**
 * Get initial guess (starting point) for NLP
 */
void MJCartPendulumNLP::get_initial_guess(double** x, double** u)
{
	const int num_guess = 3;
	double q_guess[num_q][num_guess];
	double xx[num_guess] =
	{ 0.0, 0.5, 1.0 };

	q_guess[0][0] = 0.0;
	q_guess[1][0] = 0.0;

	q_guess[0][1] = -0.4;
	q_guess[1][1] = -0.2 * M_PI;

	q_guess[0][2] = 0.0;
	q_guess[1][2] = M_PI;

	for (int i = 0; i < num_q; i++)
	{
		double interp[N];
		interpolateFromPoints(xx, q_guess[i], interp, num_guess, N);

		for (int k = 0; k < N; k++)
		{
			// Position
			x_write[k][i] = interp[k];
			// Velocity
			double deriv;
			if (k == 0)
			{
				deriv = (interp[k + 1] - interp[k]) / h;
			}
			else if (k == N - 1)
			{
				deriv = (interp[k] - interp[k - 1]) / h;
			}
			else
			{
				deriv = (interp[k + 1] - interp[k - 1]) / (2.0 * h);
			}
			x_write[k][i + num_q] = deriv;
		}
	}

	// Set input
	for (int k = 0; k < N; k++)
	{
		for (int i = 0; i < num_inputs; i++)
		{
			u_write[k][i] = 0.0; // Dynamically incorrect, but should fix itself
		}
	}
}

/**
 * Run at the end of the optimisation
 */
void MJCartPendulumNLP::finalize_solution(SolverReturn status, Index n,
		const Number* z, const Number* bm_L, const Number* mb_U, Index m,
		const Number* g, const Number* lambda, Number obj_value,
		const IpoptData* ip_data, IpoptCalculatedQuantities* ip_cq)
{
	cout << endl;
	cout << "N = " << N << ";" << endl;
	cout << "h = " << h << ";" << endl;

	cout << endl << "z = [";
	for (int i = 0; i < n; i++)
	{
		cout << z[i] << ",";
	}
	cout << "];" << endl << endl;

	for (int i = 0; i < m; i++)
	{
		if (abs(g[i] > 1e-6))
		{
			cout << "g[" << i << "] = " << g[i] << ", ";
		}
	}
	cout << endl;

	set_variable_pointers(z); // Update pointers

	viewSolution();
	//testSolution();
}

/*
 * Use MuJoCoViewer to play the solution
 */
void MJCartPendulumNLP::viewSolution()
{
	mjModel* m = model.getModelPtr();
	mjData* d = model.getDataPtr();
	double force;

	MuJoCoViewer viewer(m);

	bool done = false;

	const double num_interp = 100.0;

	while (!done)
	{
		double tmupdate = glfwGetTime();

		mj_resetData(m, d); // Reset simulation

		for (int j = 0; j < 2; j++) // Set initial position and velocity
		{
			d->qpos[j] = x[0][j];
			d->qvel[j] = x[0][j + 2];
		}

		// Loop over collocation points
		int k_max = 1.3 * N; // Run a little longer to pause at the end

		for (int k = 0; k < k_max; k++)
		{
			// Loop over interpolated data in real time
			for (double interp = 0.0; interp < 1.0; interp += 1.0 / num_interp)
			{
				double tmstart = glfwGetTime();

				if (k < N - 1)
				{
					for (int j = 0; j < 2; j++) // Only take positions from state
					{
						d->qpos[j] = x[k][j] + (x[k + 1][j] - x[k][j]) * interp;
					}
					force = u[k][0] + (u[k + 1][0] - u[k][0]) * interp;
				}

				mj_forward(m, d); // Necessary for proper render

				if (glfwGetTime() - tmupdate > 1.0 / 60.0) // Update when last frame is 1/60 seconds old
				{
					viewer.updateScene(d);

					// Add arrow

					double base[3] =
					{ d->qpos[0], -0.2, 0.0 };
					double vector[3] =
					{ force * 0.005, 0.0, 0.0 };

					viewer.drawArrow(vector, base);

					viewer.update(); // Render

					tmupdate = glfwGetTime();
				}

				if (viewer.windowShouldClose())
				{
					done = true;
					return; // Stop
				}

				double wait_time = h / num_interp;
				while (glfwGetTime() - tmstart < wait_time)
				{
					usleep(100); // Sleep for 0.1 millisecond
				}
			}
		}
	}
}

/**
 * Run feed-forward control from solutin
 */
void MJCartPendulumNLP::testSolution()
{
	mjModel* m = model.getModelPtr();
	mjData* d = model.getDataPtr();

	MuJoCoViewer viewer(m);

	bool done = false;

	// Repeat simulation
	while (!done)
	{
		mj_resetData(m, d); // Reset simulation

		for (int j = 0; j < 2; j++) // Set initial position and velocity
		{
			d->qpos[j] = x[0][j];
			d->qvel[j] = x[0][j + 2];
		}

		double simstart = glfwGetTime();

		double force[num_inputs];
		double x_target[num_states];

		// Run for a little longer than the optimisation time span
		while (d->time < 2.0 * h * N)
		{
			// Record cpu time at start of iteration
			double tmstart = glfwGetTime();

			if (viewer.windowShouldClose())
			{
				done = true;
				return; // Stop
			}

			viewer.updateScene(d);

			// Add arrow

			double base[3] =
			{ d->qpos[0], -0.2, 0.0 };
			double vector[3] =
			{ d->ctrl[0] * 0.005, 0.0, 0.0 };

			viewer.drawArrow(vector, base);

			viewer.update(); // Render

			// Bring simulation up-to-date
			if (d->time < h * N) // Update untill end of sequence
			{
				while (d->time < (tmstart - simstart))
				{
					interpolateTime(d->time, force, x_target);

					d->ctrl[0] = force[0];

					mj_step(m, d);
				}
			}

			// Wait until a total of 1 frame time has passed
			while (glfwGetTime() - tmstart < 1.0 / 60.0)
			{
				usleep(100); // Sleep for 0.1 millisecond to prevent a busy wait
			}
		}
	}
}

