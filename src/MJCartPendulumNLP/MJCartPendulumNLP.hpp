#ifndef MJCartPendulumNLP_
#define MJCartPendulumNLP_

#include "../CollocationNLP.hpp"
#include "../MuJoCoModel/MuJoCoModel.hpp"
#include "../MuJoCoViewer/MuJoCoViewer.hpp"

using namespace Ipopt;

class MJCartPendulumNLP: public CollocationNLP
{
public:
	MJCartPendulumNLP();
	virtual ~MJCartPendulumNLP();

	/** Get forward dynamics */
	virtual void eval_dynamics(const double* x, const double* u, double* dx);

	/** Get (\partial dx / \partial x) */
	virtual void eval_dynamics_diff_x(const double* x, const double* u,
			double* dx_diff_x);

	/** Get (\partial dx / \partial u) */
	virtual void eval_dynamics_diff_u(const double* x, const double* u,
			double* dx_diff_u);

	/** Get initial configuration */
	virtual void get_initial_config(double* x_0, double* u_0);

	/** Get final configuration */
	virtual void get_final_config(double* x_f, double* u_f);

	/** Get variable bounds */
	virtual void get_variable_bound(double* x_u, double* x_l, double* u_u,
			double* u_l);

	/** Get initial guess */
	virtual void get_initial_guess(double** x, double** u);

	/** This method is called when the algorithm is complete so the TNLP can store/write the solution */
	virtual void finalize_solution(SolverReturn status, Index n,
			const Number* z, const Number* bm_L, const Number* bm_U, Index m,
			const Number* g, const Number* lambda, Number obj_value,
			const IpoptData* ip_data, IpoptCalculatedQuantities* ip_cq);

private:
	MJCartPendulumNLP(const MJCartPendulumNLP&);
	MJCartPendulumNLP& operator=(const MJCartPendulumNLP&);

	void viewSolution();
	void testSolution();

	void makeArrow(mjvGeom* arrow, const double vector[3],
			const double base[3]);

	/** Use MoJoCo as a back-end */
	MuJoCoModel model;

	/** Number of positions */
	int num_q;
	/** Number of velocities */
	int num_dq;

	/** Use viewer to show results */
	//MuJoCoViewer viewer;
};

#endif
