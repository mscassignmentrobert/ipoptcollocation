function plot_biped(q, p, t)
%PLOT_BIPED Plot a single frame of the biped walker

if nargin < 3
    t = 0;
end

dim = (p.L(1) + p.L(2) + p.L(3)) * 1;

x = zeros(5,1);
y = zeros(5,1);

x(1) = -p.L(1) * sin(q(1));
y(1) =  p.L(1) * cos(q(1));

x(2) = x(1) - p.L(2) * sin(q(2));
y(2) = y(1) + p.L(2) * cos(q(2));

x(3) = x(2) - p.L(3) * sin(q(3));
y(3) = y(2) + p.L(3) * cos(q(3));

x(4) = x(2) + p.L(4) * sin(q(4));
y(4) = y(2) - p.L(4) * cos(q(4));

x(5) = x(4) + p.L(5) * sin(q(5));
y(5) = y(4) - p.L(5) * cos(q(5));

plot([0 x(1) x(2)], [0 y(1) y(2)], 'r-*');
hold on;
plot([x(2) x(4) x(5)], [y(2) y(4) y(5)], 'g-*');
plot([x(2) x(3)], [y(2) y(3)], 'b-o');
hold off;

xlim(dim*[-1, 1]);
ylim(dim*[-1, 1] + 0.5);

text(-0.9*dim, -0.1*dim, 0, sprintf('t = %.2fs', t));

end
