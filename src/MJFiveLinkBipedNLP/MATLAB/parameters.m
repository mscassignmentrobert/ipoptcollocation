function set = parameters()
%SETTINGS Get parameters
%   @return struct: set

set = struct;

%% Model

set.L = zeros(1,5);
set.m = zeros(1,5);
set.I = zeros(1,5);

% Values from Kelly:
set.L(1) = 0.4;
set.L(5) = set.L(1);
set.L(2) = 0.4;
set.L(4) = set.L(2);
set.L(3) = 0.625;

end
