#include "MuJoCoModel.hpp"

#include "mujoco/mujoco.h"

#include <iostream>

#define PRINT_MODEL_INFO 1

/**
 * Constructor
 */
MuJoCoModel::MuJoCoModel(const char* model_file)
{
	mj_activate("/usr/local/lib/mujoco/mjkey.txt");

	char load_error[256];
	m = mj_loadXML(model_file, nullptr, load_error, 256);

	if (!m)
	{
		std::cout << "Failed to load model `" << model_file << "`" << std::endl;
		std::cout << "Error: " << load_error << std::endl;
	}

	d = mj_makeData(m); // Initialise data pointer

	// Allocate memory for assisting variables
	qacc_center = mj_stackAlloc(d, m->nv);
	qacc_warmstart = mj_stackAlloc(d, m->nv);

	eps = 1e-6; // Good value

#if PRINT_MODEL_INFO
	std::cout << "model->nq = " << m->nq << " (size qpos)" << std::endl;
	std::cout << "model->nv = " << m->nv << " (size qvel)" << std::endl;
	std::cout << "model->nu = " << m->nu << " (size ctrl)" << std::endl;
	std::cout << "model->njnt = " << m->njnt << " (#joints)" << std::endl;
#endif

	quat_id = -1;

	// Search for quaternions
	for (int i = 0; i < m->njnt; i++)
	{
		if (m->jnt_type[i] == mjJNT_FREE || m->jnt_type[i] == mjJNT_BALL)
		{
			quat_id = *(m->jnt_qposadr);
			break;
		}
	}
}

/**
 * Destructor
 */
MuJoCoModel::~MuJoCoModel()
{
	// Shut down
	mj_deleteData(d);
	mj_deleteModel(m);
	mj_deactivate();
}

/**
 * Get forward dynamics
 */
void MuJoCoModel::get_dynamics(const double* qpos, const double* qvel,
		const double* u, double* qacc)
{
	reset_data(qpos, qvel, u);

	// Perform dynamics (without integrating)
	mj_forward(m, d);

	// Copy acceleration to output
	mju_copy(qacc, d->qacc, m->nv);

	return;
}

/**
 * Get derivative of acceleration w.r.t. position
 *
 * Passed data represents centre point
 */
void MuJoCoModel::get_dynamics_diff_qpos(const double* qpos, const double* qvel,
		const double* u, double* qacc_diff_qpos)
{
	finite_difference(qpos, qvel, u, d->qpos, m->nq, mjSTAGE_NONE,
			DIFF_TYPE_POS, qacc_diff_qpos);
}

/**
 * Get derivative of acceleration w.r.t. velocity
 *
 * Passed data represents centre point
 */
void MuJoCoModel::get_dynamics_diff_qvel(const double* qpos, const double* qvel,
		const double* u, double* qacc_diff_qvel)
{
	finite_difference(qpos, qvel, u, d->qvel, m->nv, mjSTAGE_POS, DIFF_TYPE_VEL,
			qacc_diff_qvel);
}

/**
 * Get derivative of acceleration w.r.t. input force
 *
 * Passed data represents centre point
 */
void MuJoCoModel::get_dynamics_diff_u(const double* qpos, const double* qvel,
		const double* u, double* qacc_diff_u)
{
	finite_difference(qpos, qvel, u, d->ctrl, m->nu, mjSTAGE_VEL, DIFF_TYPE_IN,
			qacc_diff_u);
}

/**
 * Take finite difference derivative of qacc to some target
 *
 * Use the skipstage argument to skip a stage to speed up computation.
 *
 * Result is stacked like:
 *
 * qacc_diff = [
 * 		d qacc1 / d x1
 * 		d qacc2 / d x1
 * 		d qacc3 / d x1
 * 		...
 * 		d qacc1 / d x2
 * 		d qacc2 / d x2
 * 		d qacc3 / d x2
 * 		...
 * 	]
 */
void MuJoCoModel::finite_difference(const double* qpos, const double* qvel,
		const double* u, double* target, int var_size, _mjtStage skipstage,
		DIFF_TYPE type, double* qacc_diff)
{
	reset_data(qpos, qvel, u);

	// Run full computation at centre
	mj_forward(m, d);

	// Copy the regular (centre) output and put them aside
	mju_copy(qacc_center, d->qacc, m->nv);
	mju_copy(qacc_warmstart, d->qacc_warmstart, m->nv);

	mjtNum original[var_size];
	mju_copy(original, target, var_size);

	// Loop over variables to be varied
	for (int i_var = 0; i_var < var_size; i_var++)
	{
		//double original = target[i_var];
		//double original_quat[4];

		bool is_quat = false;

		// Check of part of quaternion
		/*if (quat_id >= 0 && type == DIFF_TYPE_POS && i_var >= quat_id
				&& i_var <= quat_id + 3)
		{
			is_quat = true;

			//mju_copy(original_quat, &target[quat_id], 4);
		}*/

		target[i_var] += eps; // Perturb current variable

		if (is_quat)
		{
			// Re-normalize after perturbation
			mju_normalize4(&target[quat_id]);
		}

		// Place default warmstart to speed up this computation iteration
		mju_copy(d->qacc_warmstart, qacc_warmstart, m->nv);

		mj_forward(m, d);
		/*
		// Perform forward dynamics
		if (skipstage == 0)
		{
			mj_forward(m, d);
		}
		else
		{
			mj_forwardSkip(m, d, skipstage, 1); // Skip stage to speed up computation
		}
		*/

		mju_copy(target, original, var_size);

		/*
		target[i_var] = original; // Undo perturbation

		if (is_quat)
		{
			// Reset entire quaternion
			//std::copy(original_quat, original_quat + 4, &target[quat_id]);
			mju_copy(&target[quat_id], original_quat, 4);
		}
		*/

		// Loop through columns of qacc
		for (int c = 0; c < m->nv; c++)
		{
			// Take finite difference
			double derivative = (d->qacc[c] - qacc_center[c]) / eps;

			// Place inside derivative vector / matrix
			qacc_diff[i_var * m->nv + c] = derivative;
		}
	}
}

/**
 * Set current state of MJ data object
 */
void MuJoCoModel::reset_data(const double* qpos, const double* qvel,
		const double* u)
{
	// Copy state and input to data object
	d->time = 0.0;
	mju_copy(d->qpos, qpos, m->nq);
	mju_copy(d->qvel, qvel, m->nv);
	mju_copy(d->ctrl, u, m->nu);
}

/**
 * Return pointer to owned mjModel object
 */
mjModel* MuJoCoModel::getModelPtr() const
{
	return this->m;
}

/**
 * Return pointer to owned mjData object
 */
mjData* MuJoCoModel::getDataPtr() const
{
	return this->d;
}
