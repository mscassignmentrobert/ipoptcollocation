#ifndef MYOMP_HPP_
#define MYOMP_HPP_


// Enable compilation with and without OpenMP support
#if defined(_OPENMP)
    #include <omp.h>
#else
	// omp timer replacement
    #include <chrono>
    double omp_get_wtime(void)
    {
        static std::chrono::system_clock::time_point _start = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed = std::chrono::system_clock::now() - _start;
        return elapsed.count();
    }

    // omp functions used below
    void omp_set_dynamic(int) {}
    void omp_set_num_threads(int) {}
    int omp_get_num_procs(void) {return 1;}
#endif


#endif /* MYOMP_HPP_ */
