/*
 * MuJoCoModel.hpp
 *
 * Interfacing class between MuJoCo and the IpoptCollocation class
 *
 * This class uses a MuJoCo model to compute dynamics and dynamics
 * gradients of a system.
 */

#ifndef MUJOCOMODEL_HPP_
#define MUJOCOMODEL_HPP_

#include "mujoco/mujoco.h"

class MuJoCoModel
{
public:
	enum DIFF_TYPE {DIFF_TYPE_POS, DIFF_TYPE_VEL, DIFF_TYPE_IN};

	MuJoCoModel(const char*);
	virtual ~MuJoCoModel();

	/** Get forward dynamics */
	void get_dynamics(const double* qpos, const double* qvel, const double* u,
			double* qacc);

	/** Get derivative of acceleration to q */
	void get_dynamics_diff_qpos(const double* qpos, const double* qvel, const double* u,
			double* qacc_diff_qpos);

	/** Get derivative of acceleration to qvel */
	void get_dynamics_diff_qvel(const double* qpos, const double* qvel,
			const double* u, double* qacc_diff_qvel);

	/** Get derivative of acceleration to u */
	void get_dynamics_diff_u(const double* qpos, const double* qvel, const double* u,
			double* qacc_diff_u);

	/** Reset MJ data struct to given states */
	void reset_data(const double* qpos, const double* qvel, const double* u);

	/** Get pointer to MJ model object */
	mjModel* getModelPtr() const;

	/** get pointer to MJ data object */
	mjData* getDataPtr() const;

private:
	/** MuJoCo model information (loaded once) */
	mjModel* m;
	/** MuJoCo simulation data (represents a single timestep) */
	mjData* d;

	/** Take finite difference derivative of qacc to some target */
	void finite_difference(const double* qpos, const double* qvel, const double* u,
			double* target, int var_size, _mjtStage skipstage,
			DIFF_TYPE type, double* qacc_diff);

	/** Centre output, class property to only allocate once */
	double* qacc_center;
	/** Simulation warmstart, class property to only allocate once */
	double* qacc_warmstart;

	/** Finite-difference epsilon */
	double eps;

	/** Quaternion address */
	int quat_id;
};

#endif /* MUJOCOMODEL_HPP_ */
