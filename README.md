# IPOPT Collocation

**NOTE:** This repository has been surpassed by Gambol: https://bitbucket.org/mscassignmentrobert/gambol  
Consider the [release/cartpole](https://bitbucket.org/mscassignmentrobert/gambol/branch/release/cartpole) branch for an example of general direct collocation optimisation, without a focus on locomotion.

An extension of the standard IPOPT NLP class to allow easy optmisation of dynamics collocation problems.

See below for instructions on how to intall and use this class.  
See the 'About' sections for details on how the class works.

## Install

First of all, make sure you have IPOPT working on your system and you can compile and run the example problems.
Instructions on how to install IPOPT: https://coin-or.github.io/Ipopt/INSTALL.html .

For now just make sure the `CollocationNLP.cpp/hpp` files are included and compiled properly.

## How to use

### Basic

IPOPT solves NLP objects. This tool contains a large basis that defines a collocation problem for a general situtation.
Extend the CollocationNLP and define the template (purely virtual) functions. These are used to define general dynamics functions like `dx(x, u)` and the derivatives.
You define these functions to take arguments of single vectors `x` and `u`, instead of specific elements from optimisation variable `z`, saving you a lot of effort.
When running the NLP, the collocation base class will generate the constraints to connect the positions, velocities and accelerations of each collocation point.

This is enough to solve a basic problem. You can implement more functions to gain more control. See below.

### Dynamic bounds

You need to specifiy bounds for each optimisation variable. Additionally you can specify dynamic bounds which are repeated for each collocation point. Think of e.g. end-effector positions or knee angles.
These functions have `x` etc. as functions, not entire vector z.  
You need to set `num_dynamic_bounds` to a non-zero value.

### Extra constraints

If you have extra constraints you want to apply, you can specify those. Unlike other functions, these are a function of entire vector x.
For convience it has been split up to collocation vectors `x` and `u`. Remember the entire vector z when building the extra jacobian.  
You need to set the `m_extra` and `nnz_jac_g_extra` properties.

### Initial and final configuration

By default the variables are bound at the initial and final configuration specified by you. If you set the `constraint_config_initial` and/or `constraint_config_final` flags to `false`, these configuration will not be constraint.  
Regardless, the initial and final configurations will provide the basis for which the starting point of `z` is interpolated!

## About

The `IpoptCollocation` class builds the optimisation (primal) variables like so:

```
z = [
	x_1[0]
	x_2[0]
	x_1[1]
	x_2[1]
	x_1[2]
	x_2[2]
	...
	u_1[0]
	u_2[0]
	u_1[1]
	u_2[1]
	u_1[2]
	u_2[2]
	...
]
```
Where `x_1[0]` is state x_1 at collocation point 0, etc.

With this notation `z` is a stack of vectors per collocation point, i.e. build by the vectors \vec{q[k]}, \vec{dq[k]} and \vec{u[k]}.
Through this notation the entire state can be easily extracted for each collocation point.

To make access to the variables easier, the properties `x` and `u` are made. These are two-dimensional arrays of appropiate lengths that point to elements of vector x, e.g.:

```
this->q[k][i]
```
...Points to q_i[k] (i.e. state i, collocation point k).

The constraints vector `g(x)` looks like:

```
 	g(x) = [
			x_0[0] - x_0[1] + 0.5 * h * (dx_0[0] + dx_0[1]) ;
			x_1[0] - x_1[1] + 0.5 * h * (dx_1[0] + dx_1[1]) ;
			...
			x_0[1] - x_0[2] + 0.5 * h * (dx_0[1] + dx_0[2]) ;
			x_0[1] - x_0[2] + 0.5 * h * (dx_0[1] + dx_0[2]) ;
			...
 	]
```

Where the grouping is in the order of:

 1. Velocity, acceleration
 2. Collocation point
 3. State

Because of the shape of g(x), the jacobian of g(x) has the following sparsity structure:
 
 ![](MATLAB/spy_jacobian.png)
 
 See the MATLAB script for an example problem used to generate this plot and as comparision for the C++ output.
 
 The acceleration part of the jacobian is more dense, because the accelerations ddq_i are differentiable to all states and state derivatives in that same point.
 
 The hessian has this spartsity structure:
 
 ![](MATLAB/spy_hessian.png)
 
 *The hessian has not yet been implented*! Instead, a nummerical approach is used. This seems to work fine, though an analytical hessian might still be faster.
 
 ### State vs. coordinates
 
 This program started with handling coordinates and their derivatives independently, i.e. `q`, `dq` and `ddq`. However, this makes working with rotations in 3D almost impossible. So the platform was generalized to a state vector, i.e. `x` and `dx`. How these are made up is then up to the user.
 