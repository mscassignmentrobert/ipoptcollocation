clear;
clc;

%% Settings
N = 5;
num_states = 4;
num_inputs = 3;
hk = sym('hk');

%% Variables
x = sym('x%d_%d', [num_states, N]);
u = sym('u%d_%d', [num_inputs, N]);

z = [...
    reshape(x, numel(x), 1);
    reshape(u, numel(u), 1);
    ];

n = length(z);

dx = sym('dx%d_%d', [num_states, N]);

for k = 1:N
    args = '(';
    for i = 1:num_states
        
        if i > 1
            args = [args, ','];
        end
        
        args = [args, char(x(i,k))];
    end
    for i = 1:num_inputs
        
        args = [args, ',', char(u(i,k))];
    end
    args = [args, ')'];
    
    for i = 1:num_states
        dx(i,k) = str2sym([sprintf('dx%d_%d', i, k), args]);
    end
end

%% Objective

f = 0;
for k = 1:N-1
    for i = 1:num_inputs
        f = f + 0.5 * hk * (u(i, k)^2 + u(i, k+1)^2);
    end
end

f_gradient = gradient(f, z);

%% Constraints

g = [];

% Dynamics
for k = 1:N-1
    
    for i = 1:num_states
        g = [g;
            x(i, k) - x(i, k+1) + 0.5*hk*(dx(i,k) + dx(i,k+1));
            ];
    end
    
end

m = length(g);

g_jacobian = jacobian(g, z);

figure;
spy(g_jacobian);
title('jacobian');

% return;

%% Hessian

syms sigma_f;
lambda = sym('lambda_%d', [m, 1]);

f_jacobian = jacobian(f_gradient, z);

hessian = sigma_f * f_jacobian;

for i = 1:m
    
    hessian = hessian + lambda(i) * jacobian(gradient(g(i), z), z);
    
end

figure;
spy(hessian);
title('hessian');
