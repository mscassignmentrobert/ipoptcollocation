#include "ExampleProblem.hpp"
#include <iostream>

using namespace Ipopt;
using namespace std;

/**
 * Constructor
 */
ExampleProblem::ExampleProblem()
{
	num_states = 1;
	num_inputs = 1;
	N = 3;
	h = 0.1;

	create_indices();
}

/**
 * Destructor
 */
ExampleProblem::~ExampleProblem()
{

}

/**
 * Calculate dynamics
 *
 * @return void
 */
void ExampleProblem::eval_dynamics(const double *q, const double *dq,
		const double *u, double *ddq)
{
	for (int i = 0; i < num_states; i++)
	{
		ddq[i] = 1.0 + 1.01 * i;
	}
}

/**
 * Get dynamics derivative w.r.t. dq
 *
 * Return vector is stacked like:
 *
 * ddq/dq = [
 *    ddq1/dq
 *    ddq2/dq
 *    ...
 * ]
 *
 * @return void
 */
void ExampleProblem::eval_dynamics_diff_dq(const double *q, const double *dq,
		const double *u, double *ddq_diff_dq)
{
	for (int i = 0; i < num_states * num_states; i++)
	{
		ddq_diff_dq[i] = 2.0 + 1.01 * i;
	}
}

/**
 * Get dynamics derivative w.r.t. q
 *
 * @return void
 */
void ExampleProblem::eval_dynamics_diff_q(const double *q, const double *dq,
		const double *u, double *ddq_diff_q)
{
	for (int i = 0; i < num_states * num_states; i++)
	{
		ddq_diff_q[i] = 3.0 + 1.01 * i;
	}
}

/**
 * Get dynamics derivative w.r.t. u
 *
 * @return void
 */
void ExampleProblem::eval_dynamics_diff_u(const double *q, const double *dq,
		const double *u, double *ddq_diff_u)
{
	for (int i = 0; i < num_states * num_inputs; i++)
	{
		ddq_diff_u[i] = 4.0 + 1.01 * i;
	}
}


/** Get initial configuration */
void ExampleProblem::get_initial_config(double* q_0, double* dq_0, double* u_0)
{
	for (int i = 0; i < num_states; i++)
	{
		q_0[i] = 0.0;
		dq_0[i] = 0.0;
	}
	for (int i = 0; i < num_inputs; i++)
	{
		u_0[i] = 0.0;
	}
}

/** Get final configuration */
void ExampleProblem::get_final_config(double* q_f, double* dq_f, double* u_f)
{
	for (int i = 0; i < num_states; i++)
	{
		q_f[i] = 0.0;
		dq_f[i] = 0.0;
	}
	for (int i = 0; i < num_inputs; i++)
	{
		u_f[i] = 0.0;
	}
}

/** Get variable bounds */
void ExampleProblem::get_variable_bound(double* q_u, double* q_l, double* dq_u,
		double* dq_l, double* u_u, double* u_l)
{
	for (int i = 0; i < num_states; i++)
	{
		q_l[i] = -1.0e19;
		q_u[i] = 1.0e19;
		dq_l[i] = -1.0e19;
		dq_u[i] = 1.0e19;
	}
	for (int i = 0; i < num_inputs; i++)
	{
		u_l[i] = -1.0e19;
		u_u[i] = 1.0e19;
	}
}

/** Finalise solution */
void ExampleProblem::finalize_solution(SolverReturn status, Index n,
		const Number* x, const Number* z_L, const Number* z_U, Index m,
		const Number* g, const Number* lambda, Number obj_value,
		const IpoptData* ip_data, IpoptCalculatedQuantities* ip_cq)
{

}

