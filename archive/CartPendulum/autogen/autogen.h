/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * autogen.h
 *
 * Code generation for function 'autogen'
 *
 */

#ifndef AUTOGEN_H
#define AUTOGEN_H

/* Include files */
#include <cstddef>
#include <cstdlib>
#include "rtwtypes.h"
#include "autogen_types.h"

/* Function Declarations */
extern void autogen_dynamics(const double in1[2], const double in2[2], double F1,
  double ddq[2]);
extern void autogen_dynamics_diff_dq(const double in1[2], const double in2[2],
  double F1, double ddq_diff_dq_flat[4]);
extern void autogen_dynamics_diff_q(const double in1[2], const double in2[2],
  double F1, double ddq_diff_q_flat[4]);
extern void autogen_dynamics_diff_u(const double in1[2], const double in2[2],
  double F1, double ddq_diff_u_flat[2]);
extern void autogen_initialize();
extern void autogen_terminate();

#endif

/* End of code generation (autogen.h) */
