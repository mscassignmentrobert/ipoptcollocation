/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * autogen_types.h
 *
 * Code generation for function 'autogen_types'
 *
 */

#ifndef AUTOGEN_TYPES_H
#define AUTOGEN_TYPES_H

/* Include files */
#include "rtwtypes.h"
#endif

/* End of code generation (autogen_types.h) */
