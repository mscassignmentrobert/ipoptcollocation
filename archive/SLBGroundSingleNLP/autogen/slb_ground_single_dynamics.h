/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * slb_ground_single_dynamics.h
 *
 * Code generation for function 'slb_ground_single_dynamics'
 *
 */

#ifndef SLB_GROUND_SINGLE_DYNAMICS_H
#define SLB_GROUND_SINGLE_DYNAMICS_H

/* Include files */
#include <cstddef>
#include <cstdlib>
#include "rtwtypes.h"
#include "slb_ground_single_dynamics_types.h"

/* Function Declarations */
extern void dynamics_F(const double in1[7], const double in2[7], const double
  in3[6], double F[7]);
extern void dynamics_F_diff_dq(const double in1[7], const double in2[7], const
  double in3[6], double F_diff_dq[49]);
extern void dynamics_F_diff_u(const double in1[7], const double in2[7], const
  double in3[6], double F_diff_u[42]);
extern void dynamics_M(const double in1[7], double M[49]);
extern void dynamics_MF_diff_q(const double in1[7], const double in2[7], const
  double in3[7], const double in4[6], double MF_diff_q[49]);
extern void slb_ground_single_dynamics_initialize();
extern void slb_ground_single_dynamics_terminate();

#endif

/* End of code generation (slb_ground_single_dynamics.h) */
