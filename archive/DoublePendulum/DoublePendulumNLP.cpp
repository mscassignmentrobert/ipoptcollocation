#include "DoublePendulumNLP.hpp"
#include "autogen/autogen_dynamics.h"
#include <iostream>
#include <cmath>

using namespace Ipopt;
using namespace std;

/**
 * Constructor
 */
DoublePendulumNLP::DoublePendulumNLP()
{
	num_states = 2;
	num_inputs = 1;

	double t_0 = 0.0;
	double t_f = 4.0;

	N = 50;
	h = (t_f - t_0) / (N - 1);

	create_indices();
}

/**
 * Destructor
 */
DoublePendulumNLP::~DoublePendulumNLP()
{

}

/**
 * Get initial configuration
 *
 * Set value to 1.0e19 to make it unbounded
 */
void DoublePendulumNLP::get_initial_config(double* q_0, double* dq_0, double* u_0)
{
	q_0[0] = 0.0;
	q_0[1] = 0.0;
	dq_0[0] = 0.0;
	dq_0[1] = 0.0;

	u_0[0] = 1.0e19;

	return;
}

/**
 * Get final configuration
 *
 * Set value to 1.0e19 to make it unbounded
 */
void DoublePendulumNLP::get_final_config(double* q_f, double* dq_f, double* u_f)
{
	q_f[0] = M_PI;
	q_f[1] = M_PI;
	dq_f[0] = 0.0;
	dq_f[1] = 0.0;

	u_f[0] = 1.0e19;

	return;
}

/**
 * Get variable bounds
 *
 * Set values to 1.0e19 and -1.0e19 to make it unbounded
 */
void DoublePendulumNLP::get_variable_bound(double* q_u, double* q_l, double* dq_u,
			double* dq_l, double* u_u, double* u_l)
{
	q_l[0] = -2.0 * M_PI;
	q_u[0] = 2.0 * M_PI;

	q_l[1] = -2.0 * M_PI;
	q_u[1] = 2.0 * M_PI;

	dq_l[0] = -100.0;
	dq_u[0] = 100.0;

	dq_l[1] = -100.0;
	dq_u[1] = 100.0;

	u_l[0] = -100.0;
	u_u[0] = 100.0;
}

/**
 * Calculate dynamics
 *
 * @return void
 */
void DoublePendulumNLP::eval_dynamics(const double* q, const double* dq,
		const double* u, double* ddq)
{
	autogen_dynamics(q, dq, u[0], ddq);
}

/**
 * Get dynamics derivative w.r.t. dq
 *
 * Return vector is stacked like:
 *
 * ddq/dq = [
 *    ddq1/dq
 *    ddq2/dq
 *    ...
 * ]
 *
 * @return void
 */
void DoublePendulumNLP::eval_dynamics_diff_dq(const double* q, const double* dq,
		const double* u, double* ddq_diff_dq)
{
	autogen_dynamics_diff_dq(q, dq, u[0], ddq_diff_dq);
}

/**
 * Get dynamics derivative w.r.t. q
 *
 * @return void
 */
void DoublePendulumNLP::eval_dynamics_diff_q(const double* q, const double* dq,
		const double* u, double* ddq_diff_q)
{
	autogen_dynamics_diff_q(q, dq, u[0], ddq_diff_q);
}

/**
 * Get dynamics derivative w.r.t. u
 *
 * @return void
 */
void DoublePendulumNLP::eval_dynamics_diff_u(const double* q, const double* dq,
		const double* u, double* ddq_diff_u)
{
	autogen_dynamics_diff_u(q, dq, u[0], ddq_diff_u);
}

/**
 * Run at the end of the optimisation
 */
void DoublePendulumNLP::finalize_solution(SolverReturn status, Index n,
		const Number* x, const Number* z_L, const Number* z_U, Index m,
		const Number* g, const Number* lambda, Number obj_value,
		const IpoptData* ip_data, IpoptCalculatedQuantities* ip_cq)
{
	cout << endl;
	cout << "N = " << N << ";" << endl;
	cout << "h = " << h << ";" << endl;

	cout << endl << "x = [";
	for (int i = 0; i < n; i++)
	{
		cout << x[i] << ",";
	}
	cout << "];" << endl << endl;
}
