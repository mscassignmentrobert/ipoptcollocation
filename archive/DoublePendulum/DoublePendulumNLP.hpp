#ifndef DoublePendulumNLP_HPP_
#define DoublePendulumNLP_HPP_

#include "../CollocationNLP.hpp"

using namespace Ipopt;

class DoublePendulumNLP: public CollocationNLP
{
public:
	DoublePendulumNLP();
	virtual ~DoublePendulumNLP();

	/** Get forward dynamics */
	virtual void eval_dynamics(const double* q, const double* dq,
			const double* u, double* ddq);

	/** Get (\partial ddq / \partial dq) */
	virtual void eval_dynamics_diff_dq(const double* q, const double* dq,
			const double* u, double* ddq_diff_dq);

	/** Get (\partial ddq / \partial q) */
	virtual void eval_dynamics_diff_q(const double* q, const double* dq,
			const double* u, double* ddq_diff_q);

	/** Get (\partial ddq / \partial u) */
	virtual void eval_dynamics_diff_u(const double* q, const double* dq,
			const double* u, double* ddq_diff_u);

	/** Get initial configuration */
	virtual void get_initial_config(double *q_0, double *dq_0, double *u_0);

	/** Get final configuration */
	virtual void get_final_config(double *q_f, double *dq_f, double *u_f);

	/** Get variable bounds */
	virtual void get_variable_bound(double* q_u, double* q_l, double* dq_u,
			double* dq_l, double* u_u, double* u_l);

	/** This method is called when the algorithm is complete so the TNLP can store/write the solution */
	virtual void finalize_solution(SolverReturn status, Index n,
			const Number* x, const Number* z_L, const Number* z_U, Index m,
			const Number* g, const Number* lambda, Number obj_value,
			const IpoptData* ip_data, IpoptCalculatedQuantities* ip_cq);

private:
	DoublePendulumNLP(const DoublePendulumNLP&);
	DoublePendulumNLP& operator=(const DoublePendulumNLP&);
};

#endif /* DoublePendulumNLP_HPP_ */
