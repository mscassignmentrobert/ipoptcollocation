#ifndef FiveLinkBipedNLP_HPP_
#define FiveLinkBipedNLP_HPP_

#include "../alglib/ap.h"

#include "../CollocationNLP.hpp"

using namespace Ipopt;

class FLBGroundSingleNLP: public CollocationNLP
{
public:
	FLBGroundSingleNLP();
	virtual ~FLBGroundSingleNLP();

	/** Get forward dynamics */
	virtual void eval_dynamics(const double* q, const double* dq,
			const double* u, double* ddq);

	/** Get (\partial ddq / \partial dq) */
	virtual void eval_dynamics_diff_dq(const double* q, const double* dq,
			const double* u, double* ddq_diff_dq);

	/** Get (\partial ddq / \partial q) */
	virtual void eval_dynamics_diff_q(const double* q, const double* dq,
			const double* u, double* ddq_diff_q);

	/** Get (\partial ddq / \partial u) */
	virtual void eval_dynamics_diff_u(const double* q, const double* dq,
			const double* u, double* ddq_diff_u);

	/** Get initial configuration */
	virtual void get_initial_config(double* q_0, double* dq_0, double* u_0);

	/** Get final configuration */
	virtual void get_final_config(double* q_f, double* dq_f, double* u_f);

	/** Get variable bounds */
	virtual void get_variable_bound(double* q_u, double* q_l, double* dq_u,
			double* dq_l, double* u_u, double* u_l);

	/** Get bounds for additional constraint function */
	virtual void get_dynamic_bounds(double* g_extra_l, double* g_extra_u);

	/** Get additional constraint function */
	virtual void eval_dynamic_bounds(const double* q, const double* dq,
			const double* u, double* g_extra);

	/** Get gradient of additional constraint function */
	virtual void eval_dynamic_bounds_gradient(const double* q, const double* dq,
			const double* u, double* g_extra_diff_q, double* g_extra_diff_dq,
			double* g_extra_diff_u);

	/** Evaluate extra constraints */
	virtual void eval_g_extra(const double** q, const double** dq,
			const double** u, double* g_extra);

	/** Get bounds for extra constraints */
	virtual void get_bounds_info_extra(double* g_l_extra, double* g_u_extra);

	/** Get shape of extra constraints jacobian */
	virtual void eval_jac_g_extra_shape(int* iRow, int* jCol);

	/** Get values of extra constraints jacobian */
	virtual void eval_jac_g_extra_values(const double** q, const double** dq,
			const double** u, double* values);

	/** Get current inverse mass matrix */
	alglib::real_2d_array get_mass_inverse(const double* q);

	/** Turn double array into alglib matrix */
	alglib::real_2d_array double_array_to_matrix(const double* q, int rows,
			int columns);

	/** Turn alglib matrix into double array */
	void matrix_to_double_array(alglib::real_2d_array matrix, double* q,
			int rows, int columns);

	/** This method is called when the algorithm is complete so the TNLP can store/write the solution */
	virtual void finalize_solution(SolverReturn status, Index n,
			const Number* x, const Number* z_L, const Number* z_U, Index m,
			const Number* g, const Number* lambda, Number obj_value,
			const IpoptData* ip_data, IpoptCalculatedQuantities* ip_cq);

private:
	FLBGroundSingleNLP(const FLBGroundSingleNLP&);
	FLBGroundSingleNLP& operator=(const FLBGroundSingleNLP&);

	/** Length of leg segments */
	double L1, L2, L3, L4, L5;
	/** Length of a single step */
	double L_step;
};

#endif /* FiveLinkBipedNLP_HPP_ */
