/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * flb_ground_single_dynamics.h
 *
 * Code generation for function 'flb_ground_single_dynamics'
 *
 */

#ifndef FLB_GROUND_SINGLE_DYNAMICS_H
#define FLB_GROUND_SINGLE_DYNAMICS_H

/* Include files */
#include <cstddef>
#include <cstdlib>
#include "rtwtypes.h"
#include "flb_ground_single_dynamics_types.h"

/* Function Declarations */
extern void dynamics_F(const double in1[5], const double in2[5], const double
  in3[4], double F[5]);
extern void dynamics_F_diff_dq(const double in1[5], const double in2[5], const
  double in3[4], double F_diff_dq[25]);
extern void dynamics_F_diff_u(const double in1[5], const double in2[5], const
  double in3[4], double F_diff_u[20]);
extern void dynamics_M(const double in1[5], double M[25]);
extern void dynamics_MF_diff_q(const double in1[5], const double in2[5], const
  double in3[5], const double in4[4], double MF_diff_q[25]);
extern void flb_ground_single_dynamics_initialize();
extern void flb_ground_single_dynamics_terminate();

#endif

/* End of code generation (flb_ground_single_dynamics.h) */
