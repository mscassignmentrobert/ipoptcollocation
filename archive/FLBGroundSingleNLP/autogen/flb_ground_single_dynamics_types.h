/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * flb_ground_single_dynamics_types.h
 *
 * Code generation for function 'flb_ground_single_dynamics_types'
 *
 */

#ifndef FLB_GROUND_SINGLE_DYNAMICS_TYPES_H
#define FLB_GROUND_SINGLE_DYNAMICS_TYPES_H

/* Include files */
#include "rtwtypes.h"
#endif

/* End of code generation (flb_ground_single_dynamics_types.h) */
