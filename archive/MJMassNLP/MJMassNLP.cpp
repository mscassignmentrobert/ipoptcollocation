#include <iostream>
#include <cmath>
#include <unistd.h>

#include "MJMassNLP.hpp"

using namespace Ipopt;
using namespace std;

/**
 * Constructor
 *
 * Use initialiser list to call non-default constructor of the member class
 */
MJMassNLP::MJMassNLP() :
		model("/home/robert/Mujoco/Models/mass.xml")
{
	num_states = 1;
	num_inputs = 1;

	double t_0 = 0.0;
	double t_f = 2.0;

	N = 20;
	h = (t_f - t_0) / (N - 1);

	create_indices();
}

/**
 * Destructor
 */
MJMassNLP::~MJMassNLP()
{

}

/**
 * Get initial configuration
 *
 * Set value to 1.0e19 to make it unbounded
 */
void MJMassNLP::get_initial_config(double* q_0, double* dq_0, double* u_0)
{
	q_0[0] = 0.0;
	dq_0[0] = 0.0;

	u_0[0] = 1.0e19;
}

/**
 * Get final configuration
 *
 * Set value to 1.0e19 to make it unbounded
 */
void MJMassNLP::get_final_config(double* q_f, double* dq_f, double* u_f)
{
	q_f[0] = 1.0;
	dq_f[0] = 0.0;

	u_f[0] = 1.0e19;
}

/**
 * Get variable bounds
 *
 * Set values to 1.0e19 and -1.0e19 to make it unbounded
 */
void MJMassNLP::get_variable_bound(double* q_u, double* q_l, double* dq_u,
		double* dq_l, double* u_u, double* u_l)
{
	q_l[0] = -1.0e19;
	q_u[0] = 1.0e19;

	dq_l[0] = -1.0e19;
	dq_u[0] = 1.0e19;

	u_l[0] = -1.0e19;
	u_u[0] = 1.0e19;
}

/**
 * Calculate dynamics
 *
 * @return void
 */
void MJMassNLP::eval_dynamics(const double* q, const double* dq,
		const double* u, double* ddq)
{
	model.get_dynamics(q, dq, u, ddq);
}

/**
 * Get dynamics derivative w.r.t. dq
 *
 * Return vector is stacked like:
 *
 * ddq/dq = [
 *    ddq1/dq
 *    ddq2/dq
 *    ...
 * ]
 *
 * @return void
 */
void MJMassNLP::eval_dynamics_diff_dq(const double* q, const double* dq,
		const double* u, double* ddq_diff_dq)
{
	model.get_dynamics_diff_dq(q, dq, u, ddq_diff_dq);
}

/**
 * Get dynamics derivative w.r.t. q
 *
 * @return void
 */
void MJMassNLP::eval_dynamics_diff_q(const double* q, const double* dq,
		const double* u, double* ddq_diff_q)
{
	model.get_dynamics_diff_q(q, dq, u, ddq_diff_q);
}

/**
 * Get dynamics derivative w.r.t. u
 *
 * @return void
 */
void MJMassNLP::eval_dynamics_diff_u(const double* q, const double* dq,
		const double* u, double* ddq_diff_u)
{
	model.get_dynamics_diff_u(q, dq, u, ddq_diff_u);
}

/**
 * Run at the end of the optimisation
 */
void MJMassNLP::finalize_solution(SolverReturn status, Index n, const Number* x,
		const Number* z_L, const Number* z_U, Index m, const Number* g,
		const Number* lambda, Number obj_value, const IpoptData* ip_data,
		IpoptCalculatedQuantities* ip_cq)
{
	cout << endl;
	cout << "N = " << N << ";" << endl;
	cout << "h = " << h << ";" << endl;

	cout << endl << "x = [";
	for (int i = 0; i < n; i++)
	{
		cout << x[i] << ",";
	}
	cout << "];" << endl << endl;

	set_variable_pointers(x); // Update pointers

	viewSolution();
}

/*
 * Use MuJoCoViewer to play the solution
 */
void MJMassNLP::viewSolution()
{
	mjModel* m = model.getModelPtr();
	mjData* d = model.getDataPtr();

	MuJoCoViewer viewer(m);

	bool done = false;

	const double num_interp = 100.0;

	while (!done)
	{
		double tmupdate = glfwGetTime();

		mjvGeom* arrow;

		if (viewer.scn.ngeom >= viewer.scn.maxgeom)
		{
			cout << "Error: reached max scene geoms" << endl;
			return;
		}

		// Loop over collocation points
		for (int k = 0; k < (N - 1); k++)
		{
			// Loop over interpolated data in real time
			for (double interp = 0.0; interp < 1.0; interp += 1.0 / num_interp)
			{
				double tmstart = glfwGetTime();

				for (int j = 0; j < num_states; j++)
				{
					d->qpos[j] = q[k][j] + (q[k + 1][j] - q[k][j]) * interp;
				}
				double force = u[k][0] + (u[k + 1][0] - u[k][0]) * interp;

				mj_forward(m, d);

				if (glfwGetTime() - tmupdate > 1.0 / 60.0)
				{
					mjv_updateScene(m, d, &(viewer.opt), NULL, &(viewer.cam),
							mjCAT_ALL, &(viewer.scn));

					// Add arrow
					arrow = viewer.scn.geoms + viewer.scn.ngeom;
					makeArrow(arrow);
					viewer.scn.ngeom++;

					arrow->size[2] = abs(force) * 0.01;

					mjtNum quat[4], mat[9];
					mjtNum axis[3] = {0.0, 1.0, 0.0};
					mju_axisAngle2Quat(quat, axis, 0.5 * mjPI * ((force > 0) ? 1 : -1));
					mju_quat2Mat(mat, quat);
					mju_n2f(arrow->mat, mat, 9);

					viewer.update(d); // Update at 60 fps
					tmupdate = glfwGetTime();
				}

				if (viewer.windowShouldClose())
				{
					done = true;
					return; // Stop
				}

				double wait_time = h / num_interp;
				while (glfwGetTime() - tmstart < wait_time)
				{
					usleep(100); // Sleep for 0.1 millisecond
				}
			}
		}
	}
}

/**
 * Turn geom pointer into an arrow
 *
 * Set type, color, postion etc.
 * Rotation still has to be set! Without it, it won't appear at all
 */
void MJMassNLP::makeArrow(mjvGeom* arrow)
{
	arrow->type = mjGEOM_ARROW;
	arrow->dataid = -1;
	arrow->objtype = mjOBJ_SITE;
	arrow->objid = -1;
	arrow->category = mjCAT_DECOR;
	arrow->texid = -1;
	arrow->texuniform = 0;
	arrow->texrepeat[0] = 1;
	arrow->texrepeat[1] = 1;
	arrow->emission = 0;
	arrow->specular = 0.5;
	arrow->shininess = 0.5;
	arrow->reflectance = 0;
	arrow->label[0] = 0;
	arrow->size[0] = 0.02f;
	arrow->size[1] = 0.02f;
	arrow->size[2] = 1.0f;
	arrow->rgba[0] = 1.0f;
	arrow->rgba[1] = 0.1f;
	arrow->rgba[2] = 0.1f;
	arrow->rgba[3] = 1.0f;
	arrow->pos[0] = 0.0f;
	arrow->pos[1] = 0.0f;
	arrow->pos[2] = 0.5f;
}
