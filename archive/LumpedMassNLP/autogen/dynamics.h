/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * dynamics.h
 *
 * Code generation for function 'dynamics'
 *
 */

#ifndef DYNAMICS_H
#define DYNAMICS_H

/* Include files */
#include <cstddef>
#include <cstdlib>
#include "rtwtypes.h"
#include "dynamics_types.h"

/* Function Declarations */
extern void dynamics(const double in1[6], const double in2[6], const double in3
                     [12], double ddq[6]);
extern void dynamics_diff_dq(const double in1[6], const double in2[6], const
  double in3[12], double ddq_diff_dq_flat[36]);
extern void dynamics_diff_q(const double in1[6], const double in2[6], const
  double in3[12], double ddq_diff_q_flat[36]);
extern void dynamics_diff_u(const double in1[6], const double in2[6], const
  double in3[12], double ddq_diff_u_flat[72]);
extern void dynamics_initialize();
extern void dynamics_terminate();

#endif

/* End of code generation (dynamics.h) */
