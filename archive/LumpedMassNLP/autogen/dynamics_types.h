/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * dynamics_types.h
 *
 * Code generation for function 'dynamics_types'
 *
 */

#ifndef DYNAMICS_TYPES_H
#define DYNAMICS_TYPES_H

/* Include files */
#include "rtwtypes.h"
#endif

/* End of code generation (dynamics_types.h) */
