#include "FiveLinkBipedNLP.hpp"
#include "autogen/dynamics_F.h"

#include "../alglib/linalg.h"
#include "../alglib/ap.h"

#include <iostream>
#include <cmath>

using namespace Ipopt;
using namespace std;

/**
 * Constructor
 */
FiveLinkBipedNLP::FiveLinkBipedNLP()
{
	num_states = 5;
	num_inputs = 4;

	size_x = num_states * 2 + num_inputs;

	double t_0 = 0.0;
	double t_f = 1.0; // Corresponds with optimal step length 0.4 m

	num_dynamic_bounds = 3; // Additional dynamic bounds per collocation point

	m_extra = size_x; // Symmetry constraints
	nnz_jac_g_extra = size_x * 2; // Compare first and last states

	m_extra += 1; // Hips displacement
	nnz_jac_g_extra += 4; // First and last q1 and q2

	m_extra += 2; // Foot start and end on the ground
	nnz_jac_g_extra += 8; // Foot position is function of four angles

	constraint_config_initial = false; // Do not consider configs as constraints
	constraint_config_final = false; // They still provide a starting point!

	N = 30;
	h = (t_f - t_0) / (N - 1);
	//h = 0.1;

	L5 = L1 = 0.4;
	L4 = L2 = 0.4;
	L3 = 0.625;

	L_step = 0.4;

	create_indices();
}

/**
 * Destructor
 */
FiveLinkBipedNLP::~FiveLinkBipedNLP()
{

}

/**
 * Get initial configuration
 *
 * Set value to 1.0e19 to make it unbounded
 */
void FiveLinkBipedNLP::get_initial_config(double* q_0, double* dq_0,
		double* u_0)
{
	/*q_0[0] = 0.1802;	// Result from previous optimisation
	 q_0[1] = 0.3199;
	 q_0[2] = -0.0082;
	 q_0[3] = -0.2148;
	 q_0[4] = -0.2975;*/

	q_0[0] = -0.0161;
	q_0[1] = 0.5051;
	q_0[2] = -M_PI * 0.05;		// Calculated with IK
	q_0[3] = -0.0161;
	q_0[4] = -0.5051;

	/*q_0[0] = 0.0;
	 q_0[1] = 0.0;
	 q_0[2] = 0.0;
	 q_0[3] = 0.0;
	 q_0[4] = 0.0;*/

	dq_0[0] = 1.0e19;
	dq_0[1] = 1.0e19;
	dq_0[2] = 1.0e19;
	dq_0[3] = 1.0e19;
	dq_0[4] = 1.0e19;

	u_0[0] = 1.0e19;
	u_0[1] = 1.0e19;
	u_0[2] = 1.0e19;
	u_0[3] = 1.0e19;

	return;
}

/**
 * Get final configuration
 *
 * Set value to 1.0e19 to make it unbounded
 */
void FiveLinkBipedNLP::get_final_config(double* q_f, double* dq_f, double* u_f)
{
	/*q_f[0] = -0.2975;	// Result from previous optimisation
	 q_f[1] = -0.2148;
	 q_f[2] = -0.0082;
	 q_f[3] = 0.3199;
	 q_f[4] = 0.1802;*/

	q_f[0] = -0.5051;
	q_f[1] = -0.0161;
	q_f[2] = -M_PI * 0.05;		// Calculated with IK
	q_f[3] = 0.5051;
	q_f[4] = -0.0161;

	/*q_f[0] = -M_PI * 0.3;
	 q_f[1] = -M_PI * 0.1;
	 q_f[2] = -M_PI * 0.1;
	 q_f[3] = M_PI * 0.1;
	 q_f[4] = M_PI * 0.1;*/

	/*q_f[0] = 0.0;
	 q_f[1] = 0.0;
	 q_f[2] = 0.0;
	 q_f[3] = 0.0;
	 q_f[4] = 0.0;*/

	dq_f[0] = 1.0e19;
	dq_f[1] = 1.0e19;
	dq_f[2] = 1.0e19;
	dq_f[3] = 1.0e19;
	dq_f[4] = 1.0e19;

	u_f[0] = 1.0e19;
	u_f[1] = 1.0e19;
	u_f[2] = 1.0e19;
	u_f[3] = 1.0e19;

	return;
}

/**
 * Get variable bounds
 *
 * Set values to 1.0e19 and -1.0e19 to make it unbounded
 */
void FiveLinkBipedNLP::get_variable_bound(double* q_u, double* q_l,
		double* dq_u, double* dq_l, double* u_u, double* u_l)
{
	for (int i = 0; i < num_states; i++)
	{
		q_l[i] = -4.0 * M_PI;
		q_u[i] = 4.0 * M_PI;
		dq_l[i] = -100.0;
		dq_u[i] = 100.0;
	}

	/*q_u[0] = M_PI + M_PI / 2.0;
	 q_l[0] = M_PI - M_PI / 2.0;

	 q_u[1] = M_PI + M_PI / 2.0;
	 q_l[1] = M_PI - M_PI / 2.0;

	 q_u[2] = M_PI + M_PI / 4.0;
	 q_l[2] = M_PI - M_PI / 4.0;

	 q_u[3] = M_PI / 2.0;
	 q_l[3] = -M_PI / 2.0;

	 q_u[4] = M_PI / 2.0;
	 q_l[4] = -M_PI / 2.0;

	 dq_u[2] = 2.0;
	 dq_l[2] = -2.0;*/

	for (int i = 0; i < num_inputs; i++)
	{
		u_l[i] = -1.0e19;
		u_u[i] = 1.0e19;
	}
}

/**
 * Calculate dynamics
 *
 * @return void
 */
void FiveLinkBipedNLP::eval_dynamics(const double* q, const double* dq,
		const double* u, double* ddq)
{
	double F[num_states];

	dynamics_F(q, dq, u, F);

	alglib::real_2d_array M_inverse = get_mass_inverse(q);

	for (int r = 0; r < num_states; r++)
	{
		ddq[r] = 0.0;
		for (int c = 0; c < num_states; c++)
		{
			ddq[r] += M_inverse[r][c] * F[c];
		}
	}

	return;
}

/**
 * Get dynamics derivative w.r.t. dq
 *
 * Return vector is stacked like:
 *
 * ddq/dq = [
 *    ddq1/dq
 *    ddq2/dq
 *    ...
 * ]
 *
 * @return void
 */
void FiveLinkBipedNLP::eval_dynamics_diff_dq(const double* q, const double* dq,
		const double* u, double* ddq_diff_dq)
{
	double F_diff_dq_list[num_states * num_states];

	dynamics_F_diff_dq(q, dq, u, F_diff_dq_list);

	alglib::real_2d_array F_diff_dq = this->double_array_to_matrix(
			F_diff_dq_list, num_states, num_states);

	alglib::real_2d_array M_inverse = get_mass_inverse(q);

	alglib::real_2d_array ddq_diff_dq_matrix;
	ddq_diff_dq_matrix.setlength(num_states, num_states);

	alglib::rmatrixgemm(num_states, num_states, num_states, 1.0, M_inverse, 0,
			0, 0, F_diff_dq, 0, 0, 0, 0, ddq_diff_dq_matrix, 0, 0);

	this->matrix_to_double_array(ddq_diff_dq_matrix, ddq_diff_dq, num_states,
			num_states);

	return;
}

/**
 * Get dynamics derivative w.r.t. q
 *
 * @return void
 */
void FiveLinkBipedNLP::eval_dynamics_diff_q(const double* q, const double* dq,
		const double* u, double* ddq_diff_q)
{
	double MF_diff_q_list[num_states * num_states];
	double ddq[num_states];

	this->eval_dynamics(q, dq, u, ddq);

	dynamics_MF_diff_q(q, dq, ddq, u, MF_diff_q_list);

	alglib::real_2d_array MF_diff_q = this->double_array_to_matrix(
			MF_diff_q_list, num_states, num_states);

	alglib::real_2d_array M_inverse = get_mass_inverse(q);
	alglib::real_2d_array ddq_diff_q_matrix;
	ddq_diff_q_matrix.setlength(num_states, num_states);

	alglib::rmatrixgemm(num_states, num_states, num_states, 1.0, M_inverse, 0,
			0, 0, MF_diff_q, 0, 0, 0, 0, ddq_diff_q_matrix, 0, 0);

	this->matrix_to_double_array(ddq_diff_q_matrix, ddq_diff_q, num_states,
			num_states);

	return;
}

/**
 * Get dynamics derivative w.r.t. u
 *
 * @return void
 */
void FiveLinkBipedNLP::eval_dynamics_diff_u(const double* q, const double* dq,
		const double* u, double* ddq_diff_u)
{
	double F_diff_u_list[num_states * num_inputs];

	dynamics_F_diff_u(q, dq, u, F_diff_u_list);

	alglib::real_2d_array F_diff_u = this->double_array_to_matrix(F_diff_u_list,
			num_states, num_inputs);

	alglib::real_2d_array M_inverse = get_mass_inverse(q);

	alglib::real_2d_array ddq_diff_u_matrix;
	ddq_diff_u_matrix.setlength(num_states, num_inputs);

	alglib::rmatrixgemm(num_states, num_inputs, num_states, 1.0, M_inverse, 0,
			0, 0, F_diff_u, 0, 0, 0, 0, ddq_diff_u_matrix, 0, 0);

	this->matrix_to_double_array(ddq_diff_u_matrix, ddq_diff_u, num_states,
			num_inputs);

	return;
}

/**
 * Get current mass matrix inverse
 *
 * @param double* q
 * @return real_2d_array
 */
alglib::real_2d_array FiveLinkBipedNLP::get_mass_inverse(const double* q)
{
	static bool first = true;
	static alglib::real_2d_array M_inverse;

	// Buffer mass inverse
	if (!this->new_x && !first)
	{
		return M_inverse;
	}

	first = false;

	double M_list[num_states * num_states];

	dynamics_M(q, M_list);

	M_inverse.setlength(num_states, num_states);

	int i = 0;
	for (int c = 0; c < num_states; c++)
	{
		for (int r = 0; r < num_states; r++)
		{
			M_inverse[r][c] = M_list[i++];
		}
	}

	alglib::ae_int_t info;
	alglib::matinvreport rep;
	alglib::rmatrixinverse(M_inverse, info, rep);

	return M_inverse;
}

/**
 * Turn double array into alglib matrix
 *
 * Double array must be built column-by-column!
 *
 * @param double* q Array
 * @param int rows
 * @param int columns
 * @return real_2d_array
 */
alglib::real_2d_array FiveLinkBipedNLP::double_array_to_matrix(const double* q,
		int rows, int columns)
{
	alglib::real_2d_array M;
	M.setlength(rows, columns);

	int i = 0;
	for (int c = 0; c < columns; c++)
	{
		for (int r = 0; r < rows; r++)
		{
			M[r][c] = q[i++];
		}
	}

	return M;
}

/**
 * Turn alglib matrix into double array
 *
 * Array will be built column by column
 *
 * @param real_2d_array M
 * @param double* q
 * @param int rows
 * @param int columns
 * @return void
 */
void FiveLinkBipedNLP::matrix_to_double_array(alglib::real_2d_array M,
		double* q, int rows, int columns)
{
	int i = 0;
	for (int c = 0; c < columns; c++)
	{
		for (int r = 0; r < rows; r++)
		{
			q[i++] = M[r][c];
		}
	}
}

/**
 * Run at the end of the optimisation
 */
void FiveLinkBipedNLP::finalize_solution(SolverReturn status, Index n,
		const Number* x, const Number* z_L, const Number* z_U, Index m,
		const Number* g, const Number* lambda, Number obj_value,
		const IpoptData* ip_data, IpoptCalculatedQuantities* ip_cq)
{
	cout << endl;
	cout << "N = " << N << ";" << endl;
	cout << "h = " << h << ";" << endl;

	cout << endl << "x = [";
	for (int i = 0; i < n; i++)
	{
		cout << x[i] << ",";
	}
	cout << "];" << endl << endl;
}

/**
 * Get bounds for additional constraint function
 */
void FiveLinkBipedNLP::get_dynamic_bounds(double* g_extra_l, double* g_extra_u)
{
	// Minimum height of the hips
	g_extra_l[0] = -1.0e19; // 0.65
	g_extra_u[0] = 1.0e19; // Unbounded

	// Keep both knees bend in the right way
	g_extra_l[1] = 0.05 * M_PI;
	g_extra_u[1] = 0.98 * M_PI; // Never fully straight

	g_extra_l[2] = 0.05 * M_PI;
	g_extra_u[2] = 0.98 * M_PI;
}

/**
 * Get additional constraint function
 */
void FiveLinkBipedNLP::eval_dynamic_bounds(const double* q, const double* dq,
		const double* u, double* g_extra)
{
	// Keep hips high
	g_extra[0] = L1 * cos(q[0]) + L2 * cos(q[1]);

	// Keep both knees bend in the right way
	g_extra[1] = M_PI + q[0] - q[1];
	g_extra[2] = M_PI + q[4] - q[3];
}

/**
 * Get gradient of additional constraint function g(x)
 */
void FiveLinkBipedNLP::eval_dynamic_bounds_gradient(const double* q,
		const double* dq, const double* u, double* g_extra_diff_q,
		double* g_extra_diff_dq, double* g_extra_diff_u)
{

	// Set all to zero first
	for (int i = 0; i < num_states * num_dynamic_bounds; i++)
	{
		g_extra_diff_q[i] = 0.0;
		g_extra_diff_dq[i] = 0.0;

		if (i < num_inputs * num_dynamic_bounds)
		{
			g_extra_diff_u[i] = 0.0;
		}
	}

	// Hips
	g_extra_diff_q[0] = -L1 * sin(q[0]);						// diff g1 / q1
	g_extra_diff_q[0 + num_dynamic_bounds] = -L2 * sin(q[1]);	// diff g1 / q2

	// Knees
	g_extra_diff_q[1] = 1;								// diff g2 / q1
	g_extra_diff_q[1 + num_dynamic_bounds] = -1;		// diff g2 / q2
	g_extra_diff_q[2 + 4 * num_dynamic_bounds] = 1;		// diff g3 / q5
	g_extra_diff_q[2 + 3 * num_dynamic_bounds] = -1;	// diff g3 / q4
}

/**
 * Evaluate extra constraints
 *
 * For entire vector x
 */
void FiveLinkBipedNLP::eval_g_extra(const double** q, const double** dq,
		const double** u, double* g_extra)
{
	// Symmetry constraints
	int i = 0;
	for (int j = 0; j < num_states; j++)
	{
		g_extra[i++] = q[0][j] - q[N - 1][num_states - j - 1]; // e.g. q_1[0] = q_5[end]
	}
	for (int j = 0; j < num_states; j++)
	{
		g_extra[i++] = dq[0][j] - dq[N - 1][num_states - j - 1]; // e.g. dq_1[0] = dq_5[end]
	}
	for (int j = 0; j < num_inputs; j++)
	{
		g_extra[i++] = u[0][j] - u[N - 1][num_inputs - j - 1]; // e.g. u_1[0] = u_4[end]
	}

	// Hips displacement
	double x_N = -L1 * sin(q[N - 1][0]) - L2 * sin(q[N - 1][1]);
	double x_1 = -L1 * sin(q[0][0]) - L2 * sin(q[0][1]);

	g_extra[i++] = x_N - x_1 - L_step; // Ensure movement forward

	// Foot on the ground
	g_extra[i++] = L1 * cos(q[0][0]) + L2 * cos(q[0][1]) - L4 * cos(q[0][3])
			- L5 * cos(q[0][4]);
	g_extra[i++] = L1 * cos(q[N - 1][0]) + L2 * cos(q[N - 1][1])
			- L4 * cos(q[N - 1][3]) - L5 * cos(q[N - 1][4]); // Both equal to zero
}

/**
 * Get shape of extra constraints jacobian
 *
 * `iRow` needs to start at 1
 */
void FiveLinkBipedNLP::eval_jac_g_extra_shape(int* iRow, int* jCol)
{
	// Symmetry constraints
	int i = 0;
	int row = 1;
	for (int l = 0; l < num_states; l++) // q
	{
		iRow[i] = row;
		jCol[i] = l + 1;				// q_1[0]
		i++;
		iRow[i] = row;
		jCol[i] = num_states * N - l; 	// q_5[end]
		i++;
		row++;
	}
	for (int l = 0; l < num_states; l++) // dq
	{
		iRow[i] = row;
		jCol[i] = num_states * N + l + 1;	// dq_1[0]
		i++;
		iRow[i] = row;
		jCol[i] = 2 * num_states * N - l; 	// dq_5[end]
		i++;
		row++;
	}
	for (int l = 0; l < num_inputs; l++) // u
	{
		iRow[i] = row;
		jCol[i] = 2 * num_states * N + l + 1;	// u_1[0]
		i++;
		iRow[i] = row;
		jCol[i] = size_x * N - l; 				// u_5[end]
		i++;
		row++;
	}

	// Hips displacement
	iRow[i] = row;
	jCol[i] = 1;				// q_1[0]
	i++;
	iRow[i] = row;
	jCol[i] = 2;				// q_2[0]
	i++;
	iRow[i] = row;
	jCol[i] = num_states * (N - 1) + 1;	// q_1[N]
	i++;
	iRow[i] = row;
	jCol[i] = num_states * (N - 1) + 2;	// q_1[N]
	i++;
	row++;

	// Foot on the ground
	for (int k = 0; k < num_states * N; k += num_states * (N - 1))
	{
		for (int j = 1; j <= num_states; j++)
		{
			if (j == 3)
			{
				continue;
			}

			iRow[i] = row;
			jCol[i] = k + j;
			i++;
		}
		row++;
	}
}

/**
 * Get values of extra constraints jacobian
 */
void FiveLinkBipedNLP::eval_jac_g_extra_values(const double** q,
		const double** dq, const double** u, double* values)
{
	// Symmetry constraints
	int i = 0;
	for (int j = 0; j < size_x; j++)
	{
		values[i++] = 1.0;	// + q_1[0] - q_5[end]
		values[i++] = -1.0;
	}

	// Hips displacement
	values[i++] = L1 * cos(q[0][0]);		// q_1[0]
	values[i++] = L2 * cos(q[0][1]);		// q_2[0]
	values[i++] = -L1 * cos(q[N - 1][0]);	// q_1[N]
	values[i++] = -L2 * cos(q[N - 1][1]);	// q_2[N]

	// Foot on the ground
	for (int k = 0; k < N; k += N - 1)
	{
		values[i++] = -L1 * sin(q[k][0]);
		values[i++] = -L2 * sin(q[k][1]);
		values[i++] = L4 * sin(q[k][3]);
		values[i++] = L5 * sin(q[k][4]);
	}
}
