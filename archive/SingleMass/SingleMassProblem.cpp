#include "SingleMassProblem.hpp"
#include <iostream>

using namespace Ipopt;
using namespace std;

/**
 * Constructor
 */
SingleMassProblem::SingleMassProblem()
{
	num_states = 1;
	num_inputs = 1;
	N = 10;
	h = 0.1;

	mass = 1.0; // kg
	damping = 0.5; // N/m/s
	spring = 0.3; // N/m

	create_indices();
}

/**
 * Destructor
 */
SingleMassProblem::~SingleMassProblem()
{

}

/**
 * Calculate dynamics
 *
 * @return void
 */
void SingleMassProblem::eval_dynamics(const double *q, const double *dq,
		const double *u, double *ddq)
{
	ddq[0] = (u[0] - damping * dq[0] - spring * q[0]) / mass;
}

/**
 * Get dynamics derivative w.r.t. dq
 *
 * Return vector is stacked like:
 *
 * ddq/dq = [
 *    ddq1/dq
 *    ddq2/dq
 *    ...
 * ]
 *
 * @return void
 */
void SingleMassProblem::eval_dynamics_diff_dq(const double *q, const double *dq,
		const double *u, double *ddq_diff_dq)
{
	ddq_diff_dq[0] = -damping / mass;
}

/**
 * Get dynamics derivative w.r.t. q
 *
 * @return void
 */
void SingleMassProblem::eval_dynamics_diff_q(const double *q, const double *dq,
		const double *u, double *ddq_diff_q)
{
	ddq_diff_q[0] = -spring / mass;
}

/**
 * Get dynamics derivative w.r.t. u
 *
 * @return void
 */
void SingleMassProblem::eval_dynamics_diff_u(const double *q, const double *dq,
		const double *u, double *ddq_diff_u)
{
	ddq_diff_u[0] = 1.0 / mass;
}


/** Get initial configuration */
void SingleMassProblem::get_initial_config(double* q_0, double* dq_0, double* u_0)
{
	q_0[0] = 0.0;
	dq_0[0] = 0.0;
	u_0[0] = 1.0e19;
}

/** Get final configuration */
void SingleMassProblem::get_final_config(double* q_f, double* dq_f, double* u_f)
{
	q_f[0] = 1.0;
	dq_f[0] = 0.0;
	u_f[0] = 1.0e19;
}

/** Get variable bounds */
void SingleMassProblem::get_variable_bound(double* q_u, double* q_l, double* dq_u,
		double* dq_l, double* u_u, double* u_l)
{
	q_l[0] = -1.0e19;
	q_u[0] = 1.0e19;
	dq_l[0] = -1.0e19;
	dq_u[0] = 1.0e19;

	u_l[0] = -1.0e19;
	u_u[0] = 1.0e19;
}

/** Finalise solution */
void SingleMassProblem::finalize_solution(SolverReturn status, Index n,
		const Number* x, const Number* z_L, const Number* z_U, Index m,
		const Number* g, const Number* lambda, Number obj_value,
		const IpoptData* ip_data, IpoptCalculatedQuantities* ip_cq)
{
	cout << "x = [";
	for (int i = 0; i < n; i++)
	{
		cout << x[i] << ", ";
	}
	cout << "];" << endl;
}

